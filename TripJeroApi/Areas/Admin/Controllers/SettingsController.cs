﻿using Buisness.Core.DbContext;
using Buisness.Core.UtitliesSection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TripJeroApi.ExtensionController;

namespace TripJeroApi.Areas.Admin.Controllers
{
    [Authorize(Roles = "Super Admin")]
    public class SettingsController : ExtendedController
    {
        public readonly IUtilities _utilities;

        public SettingsController(IUtilities utilities)
        {
            _utilities = utilities;
        }

        [HttpPost]
        [Authorize(Roles = "Super Admin")]
        public IHttpActionResult AddCategores(TripCategory tripCategory)
        {
            return Ok(_utilities.AddTripCategories(tripCategory));
        }
        [Authorize(Roles = "Super Admin")]
        [HttpPost]
        public IHttpActionResult UpdateCagtegory(TripCategory tripCategory)
        {
            return Ok(_utilities.UpdateTripCategory(tripCategory));
        }
        [Authorize(Roles = "Super Admin")]
        [HttpPost]
        public IHttpActionResult DeletedCateogry(int Id)
        {
            return Ok(_utilities.DeleteCategory(Id));
        }
    }
}
