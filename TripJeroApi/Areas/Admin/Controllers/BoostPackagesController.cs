﻿using Buisness.Core.BoostTrip;
using Buisness.Core.DbContext;
using DataEntites.Model.BoostTrip;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TripJeroApi.ExtensionController;

namespace TripJeroApi.Areas.Admin.Controllers
{
    public class BoostPackagesController : ExtendedController
    {

        private readonly IBoostTrip _boostTrip;
        public BoostPackagesController(IBoostTrip boostTrip)
        {
            _boostTrip = boostTrip;
        }

        #region Boost Package Promo

        [Authorize(Roles = "Super Admin")]
        [HttpPost]
        public IHttpActionResult AddNewBostPromo(BoostPackagesPromo boostPackagePromo)
        {
            boostPackagePromo.ModifyBy = GetProfile().Id;
            boostPackagePromo.CreatedBy = GetProfile().Id;
            return Ok(_boostTrip.AddNewBoostPromo(boostPackagePromo));
        }
        [Authorize(Roles = "Super Admin")]
        [HttpPost]
        public IHttpActionResult DeleteBostPromo(long Id)
        {
            return Ok(_boostTrip.DeletePromo(Id,GetProfile().Id));
        }
        [Authorize(Roles = "Super Admin")]
        [HttpPost]
        public IHttpActionResult UpdateStatus(bool status,long Id)
        {
            return Ok(_boostTrip.UpdateStatus(status,Id, GetProfile().Id));
        }
        [Authorize(Roles = "Super Admin")]
        [HttpGet]
        public IHttpActionResult GetAllBoostPackagePromo()
        {
            return Ok(_boostTrip.GetAllPromo());
        }
        #endregion


        #region Boost pakcatges Addtion 
        [HttpPost]
        [Authorize(Roles = "Super Admin")]
        public IHttpActionResult AddUpdateNewBoostPackage(BoostTripModel boostTripModel)
        {
            boostTripModel.ModifyBy = GetProfile().Id;
            
            return Ok(_boostTrip.AddUpdateNewBoostPackage(boostTripModel));
        }
        [HttpPost]
        [Authorize(Roles = "Super Admin")]
        public IHttpActionResult DeleteBoostPackage(long Id)
        {

            return Ok(_boostTrip.DeleteBoostPackage(Id,GetProfile().Id));
        }
        #endregion

    }
}
