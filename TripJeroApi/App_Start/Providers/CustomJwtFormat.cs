﻿using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using System;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace TripJeroApi.App_Start.Providers
{
    public class CustomJwtFormat : ISecureDataFormat<AuthenticationTicket>
    {
        private static readonly byte[] _secret = TextEncodings.Base64Url.Decode(ConfigurationManager.AppSettings["secret"]);
        private readonly string _issuer;

        public CustomJwtFormat(string issuer)
        {
            _issuer = issuer;
        }

        public string Protect(AuthenticationTicket data)
        {
            // if (data == null)
            // {
            //     throw new ArgumentNullException(nameof(data));
            // }
            // var signingKey = new HmacSigningCredentials(_secret);
            // var issued = data.Properties.IssuedUtc;
            // var expires = data.Properties.ExpiresUtc;
            // JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            // JwtSecurityToken token = handler.CreateJwtSecurityToken(descriptor);
            ////return new JwtSecurityTokenHandler().WriteToken(
            //  //   new JwtSecurityToken(_issuer, "Any", data.Identity.Claims, issued.Value.UtcDateTime, expires.Value.UtcDateTime, signingKey));
            // byte[] key = Convert.FromBase64String(_secret);
            SymmetricSecurityKey securityKey = new SymmetricSecurityKey(_secret);
            SecurityTokenDescriptor descriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(data.Identity.Claims),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(securityKey,
                SecurityAlgorithms.HmacSha256Signature)
            };

            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            JwtSecurityToken token = handler.CreateJwtSecurityToken(descriptor);
            return handler.WriteToken(token);
        }

        //public static string GenerateToken(string username,string Secret)
        //{
        //    byte[] key = Convert.FromBase64String(Secret);
        //    SymmetricSecurityKey securityKey = new SymmetricSecurityKey(key);
        //    var role = username == "wassi" ? "Admin" : "Super Admin";
        //    SecurityTokenDescriptor descriptor = new SecurityTokenDescriptor
        //    {
        //        Subject = new ClaimsIdentity(new[] {
        //              new Claim(ClaimTypes.Name, username),
        //              new Claim(ClaimTypes.Role, role)
        //        }),
        //        Expires = DateTime.UtcNow.AddDays(1),
        //        SigningCredentials = new SigningCredentials(securityKey,
        //        SecurityAlgorithms.HmacSha256Signature)
        //    };

        //    JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
        //    JwtSecurityToken token = handler.CreateJwtSecurityToken(descriptor);
        //    return handler.WriteToken(token);
        //}
        public AuthenticationTicket Unprotect(string protectedText)
        {
            throw new NotImplementedException();
        }
    }
}