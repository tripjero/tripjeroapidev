﻿using Buisness.Core.OwnRepostry;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System.Security.Claims;
using System.Threading.Tasks;

namespace TripJeroApi.App_Start.Providers
{
    public class CustomOAuthProvider : OAuthAuthorizationServerProvider
    {
        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            //context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            using (UserMasterRepository _userRepostrySerivce = new UserMasterRepository())
            {
                var user = _userRepostrySerivce.ValidateUser(context.UserName, context.Password);
                if (user == null)
                {
                    context.SetError("Autorization Error", "Your email or password is incorrect!");
                    context.Response.Headers.Add("AuthorizationResponse", new[] { "Failed" });
                    return Task.FromResult<object>(null);
                }
                else if (!user.IsActive)
                {
                    context.SetError("Disabled Account", "Your account is disable. Please contact customer support for more information.");
                    return Task.FromResult<object>(null);
                }
                var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                identity.AddClaim(new Claim(ClaimTypes.Role, user.Role.Name));
                identity.AddClaim(new Claim("RId", user.RoleId.ToString()));
                identity.AddClaim(new Claim(ClaimTypes.Name, user.FullName));
                identity.AddClaim(new Claim("Email", user.Email));
                identity.AddClaim(new Claim("Id", user.Id.ToString()));
                identity.AddClaim(new Claim("FirebaseToken", user.FirebaseToken != null ? user.FirebaseToken.ToString() : ""));
                var ticket = new AuthenticationTicket(identity, new AuthenticationProperties());
                context.Validated(ticket);
            }
            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
            return Task.FromResult<object>(null);
        }

        private static System.Security.Claims.ClaimsIdentity SetClaimsIdentity(OAuthGrantResourceOwnerCredentialsContext context, string email)
        {
            var identity = new System.Security.Claims.ClaimsIdentity();
            identity.AddClaim(new System.Security.Claims.Claim(System.Security.Claims.ClaimTypes.Name, context.UserName));
            identity.AddClaim(new System.Security.Claims.Claim("sub", context.UserName));

            // var userRoles = context.OwinContext.Get<BookUserManager>().GetRoles(user.Id);
            //foreach (var role in userRoles)
            //{
            identity.AddClaim(new Claim(ClaimTypes.Role, "Admin"));
            //}
            return identity;
        }
    }
}