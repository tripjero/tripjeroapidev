﻿using Buisness.Core.OwnRepostry;
using Microsoft.Owin.Security.OAuth;
using System.Security.Claims;
using System.Threading.Tasks;

namespace TripJeroApi.App_Start
{
    public class MyAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            await Task.Run(() =>
                context.Validated()
                );
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            using (UserMasterRepository _userRepostrySerivce = new UserMasterRepository())
            {
                var user = _userRepostrySerivce.ValidateUser(context.UserName, context.Password);
                if (user == null)
                {
                    context.SetError("Authorization Error", "You email or password is incorrect!");
                    context.Response.Headers.Add("AuthorizationResponse", new[] { "Failed" });

                    return;
                }
                //else if (!user.IsActive)
                //{
                //    context.SetError("Disabled Account", "Your account is disabled please contact your customer support");
                //    return;
                //}
               // var was_deactive = user.IsActive;

                
                var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                identity.AddClaim(new Claim(ClaimTypes.Role, user.Role.Name));
                identity.AddClaim(new Claim("RId", user.RoleId.ToString()));
                identity.AddClaim(new Claim(ClaimTypes.Name, user.FullName));
                identity.AddClaim(new Claim("Email", user.Email));
                identity.AddClaim(new Claim("Id", user.Id.ToString()));
                identity.AddClaim(new Claim("FirebaseToken", user.FirebaseToken != null ? user.FirebaseToken.ToString() : ""));
                await Task.Run(() =>
                context.Validated(identity));
            }
        }
    }
}