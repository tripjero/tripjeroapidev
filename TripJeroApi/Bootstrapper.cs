﻿using Resolver;
using System.Web.Http;
using Unity;

namespace TripJeroApi
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            var container = BuildUnityContainer();
            System.Web.Mvc.DependencyResolver.SetResolver(new Unity.Mvc5.UnityDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        }

        public static void RegisterTypes(IUnityContainer container)
        {
            //Component initialization via MEF
            //ComponentLoader.LoadContainer(container, ".\\bin", "AF.AF.API.dll");
            ComponentLoader.LoadContainer(container, ".\\bin", "Buisness.Core.dll");
        }
    }
}