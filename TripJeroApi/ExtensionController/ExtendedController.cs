﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;

namespace TripJeroApi.ExtensionController
{
    [Authorize]
    public class ExtendedController : ApiController
    {
        [NonAction]
        public LoginModel GetProfile()
        {
            var identity = (ClaimsIdentity)User.Identity;
            LoginModel login = new LoginModel();
            login.Name = identity.Name;
            login.RoleName = identity.Claims.Where(c => c.Type == ClaimTypes.Role).Select(x => x.Value).First();
            login.Id = Convert.ToInt64(identity.Claims.FirstOrDefault(x => x.Type == "Id").Value);
            login.Email = identity.Claims.FirstOrDefault(x => x.Type == "Email").Value;
            login.FirebaseToken = identity.Claims.FirstOrDefault(x => x.Type == "FirebaseToken").Value;
            login.RolId = Convert.ToInt32(identity.Claims.FirstOrDefault(x => x.Type == "RId").Value);
            return login;
        }
    }
}