﻿namespace TripJeroApi.ExtensionController
{
    public class LoginModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string RoleName { get; set; }
        public int RolId { get; set; }
        public string FirebaseToken { get; set; }
    }
}