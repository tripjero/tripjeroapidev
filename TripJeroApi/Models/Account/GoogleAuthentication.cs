﻿using Buisness.Core.Account;
using DataEntites.Model.Account;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using TripJeroApi.App_Start.Providers;

namespace TripJeroApi.Models.Account
{
    public class GoogleAuthentication
    {
        private readonly IAccount account;

        public GoogleAuthentication(IAccount _account)
        {
            account = _account;
        }

        public object GetToken(string token, int role_id)
        {
            string newToken = "";
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            JwtSecurityToken jwtToken = (JwtSecurityToken)tokenHandler.ReadToken(token.Trim());
            if (jwtToken != null)
            {
                if (DateTime.Compare(jwtToken.ValidTo, DateTime.UtcNow) <= 0)
                {
                    return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Token Expried", false);
                }
                ExternalLoginModel externalLoginModel = new ExternalLoginModel();
                externalLoginModel.Email = jwtToken.Claims.FirstOrDefault(x => x.Type == "email").Value;
                externalLoginModel.Name = jwtToken.Claims.FirstOrDefault(x => x.Type == "name").Value;
                externalLoginModel.Picture = jwtToken.Claims.FirstOrDefault(x => x.Type == "picture").Value;
                externalLoginModel.Token = token;
                externalLoginModel.ProviderId = jwtToken.Claims.FirstOrDefault(x => x.Type == "sub").Value;
                externalLoginModel.Issuser = jwtToken.Issuer;
                long UId;
                var result = account.RoleBaseExternalResgistraion(externalLoginModel,role_id, out UId);
                if (result != null)
                {
                    if (!account.isActiveUser(externalLoginModel.Email))
                    {
                        return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Your account is disable. Please contact support for more information. ", false);


                    }
                    else
                    {
                        var identity = new ClaimsIdentity("Bearer");
                        identity.AddClaim(new Claim(ClaimTypes.Role, result.Name));
                        identity.AddClaim(new Claim("RId", result.Id.ToString()));
                        identity.AddClaim(new Claim(ClaimTypes.Name, externalLoginModel.Name));
                        identity.AddClaim(new Claim("Email", externalLoginModel.Email));
                        identity.AddClaim(new Claim("Id", UId.ToString()));
                        identity.AddClaim(new Claim("FirebaseToken", ""));
                        newToken = ExternalProvider.GenrateToken(identity);
                        Thread.CurrentPrincipal = new ClaimsPrincipal(identity);
                        if (HttpContext.Current != null)
                        {
                            HttpContext.Current.User = Thread.CurrentPrincipal;
                        }
                    }
                }
                else
                {
                
                }
            }
            if (!string.IsNullOrEmpty(newToken))
            {
                Authentication authentication = new Authentication();
                authentication.access_token = newToken;
                return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Token Found", true, authentication);
            }
            return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No token found", false);
        }
    }
}