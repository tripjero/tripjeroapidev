﻿using Buisness.Core.Account;
using DataEntites.Model.Account;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using TripJeroApi.App_Start.Providers;

namespace TripJeroApi.Models.Account
{
    public class FacebookAuthentication
    {
        private readonly IAccount account;

        public FacebookAuthentication(IAccount _account)
        {
            account = _account;
        }

        //public async Task<object> GetToken(string token)
        //{
        //    string newToken = "";
        //    if (token != null)
        //    {
        //        ExternalLoginModel externalLoginModel = new ExternalLoginModel();
        //        var facebook = await account.GetFacebookResponseAsync(token);
        //        if (facebook.email == null)
        //        {
        //            return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Email found, Trip jero require email to login", false);
        //        }
        //        externalLoginModel.Name = facebook.name;
        //        externalLoginModel.Picture = null;
        //        externalLoginModel.Token = token;
        //        externalLoginModel.ProviderId = facebook.id;
        //        externalLoginModel.Email = facebook.email;
        //        externalLoginModel.Issuser = "Facebook";
        //        long UId;
        //        var result = account.ExternalResgistraion(externalLoginModel,role_id, out UId);
        //        if (result != null)
        //        {
        //            var identity = new ClaimsIdentity("Bearer");
        //            identity.AddClaim(new Claim(ClaimTypes.Role, result.Name));
        //            identity.AddClaim(new Claim("RId", result.Id.ToString()));
        //            identity.AddClaim(new Claim(ClaimTypes.Name, externalLoginModel.Name));
        //            identity.AddClaim(new Claim("Email", externalLoginModel.Email));
        //            identity.AddClaim(new Claim("Id", UId.ToString()));
        //            identity.AddClaim(new Claim("FirebaseToken", ""));
        //            newToken = ExternalProvider.GenrateToken(identity);
        //            Thread.CurrentPrincipal = new ClaimsPrincipal(identity);
        //            if (HttpContext.Current != null)
        //            {
        //                HttpContext.Current.User = Thread.CurrentPrincipal;
        //            }
        //        }
        //    }
        //    if (!string.IsNullOrEmpty(newToken))
        //    {
        //        Authentication authentication = new Authentication();
        //        authentication.access_token = newToken;
        //        return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Token Found", true, authentication);
        //    }
        //    return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No token found", false);
        //}


        // Created new flow to authenticate role base socila media login . role id will send with each socila request
        public async Task<object> GetRoleBaseToken(string token,int role_id)
        {
            
                string newToken = "";
                if (token != null)
                {
                    ExternalLoginModel externalLoginModel = new ExternalLoginModel();
                    var facebook = await account.GetFacebookResponseAsync(token);
                    if (facebook.email == null)
                    {
                        return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Email found, Trip jero require email to login", false);
                    }
                    

          
                    externalLoginModel.Name = facebook.name;
                    externalLoginModel.Picture = null;
                    externalLoginModel.Token = token;
                    externalLoginModel.ProviderId = facebook.id;
                    externalLoginModel.Email = facebook.email;
                    externalLoginModel.Issuser = "Facebook";
                    long UId;
                    var result = account.RoleBaseExternalResgistraion(externalLoginModel,role_id, out UId);
                    if (result != null)
                    {
                    if (!account.isActiveUser(externalLoginModel.Email))
                    {
                        return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Your account is disable. Please contact support for more information. ", false);


                    }
                    else
                    {
                        var identity = new ClaimsIdentity("Bearer");
                        identity.AddClaim(new Claim(ClaimTypes.Role, result.Name));
                        identity.AddClaim(new Claim("RId", result.Id.ToString()));
                        identity.AddClaim(new Claim(ClaimTypes.Name, externalLoginModel.Name));
                        identity.AddClaim(new Claim("Email", externalLoginModel.Email));
                        identity.AddClaim(new Claim("Id", UId.ToString()));
                        identity.AddClaim(new Claim("FirebaseToken", ""));
                        newToken = ExternalProvider.GenrateToken(identity);
                        Thread.CurrentPrincipal = new ClaimsPrincipal(identity);
                        if (HttpContext.Current != null)
                        {
                            HttpContext.Current.User = Thread.CurrentPrincipal;
                        }
                    }
                    }
                }
                if (!string.IsNullOrEmpty(newToken))
                {
                Authentication authentication = new Authentication();
                authentication.access_token = newToken;
                return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Token Found", true, authentication);

            }
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No token found", false);
            }


        }
}