﻿namespace TripJeroApi.Models.Trip
{
    public class TripImage
    {
        public string ImageString { get; set; }
        public string ImagePath { get; set; }
    }
}