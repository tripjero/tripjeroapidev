﻿namespace TripJeroApi.Models.User
{
    public class UserProfile
    {
        public long Id { get; set; }
        public string ImageProfileString { get; set; }
    }
}