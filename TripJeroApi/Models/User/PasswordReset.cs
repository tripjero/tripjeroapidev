﻿namespace TripJeroApi.Models.User
{
    public class PasswordReset
    {
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
    }
}