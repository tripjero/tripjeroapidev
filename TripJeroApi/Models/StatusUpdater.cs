﻿namespace TripJeroApi.Models
{
    public class StatusUpdater
    {
        public int Id { get; set; }
        public bool Status { get; set; }
    }
}