﻿using Buisness.Core.Trip;
using Buisness.Core.TripBooking;
using DataEntites.Model.Trip;
using DataEntites.Model.Trip.TripSearch;
using DataEntites.Model.Utilities;
using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using TripJeroApi.ExtensionController;
using TripJeroApi.Models;

namespace TripJeroApi.Controllers
{
#pragma warning disable

    [Authorize(Roles = "Super Admin,Admin,Operator,Traveller")]
    public class TripController : ExtendedController
    {
        private readonly ITrip _tripSerivce;
        private readonly ITripBooking _tripBooking;

        public TripController(ITrip trip, ITripBooking tripBooking)
        {
            _tripSerivce = trip;
            _tripBooking = tripBooking;
        }

        [HttpPost]
        public IHttpActionResult AddNewTrip(TripModel tripModel, string callFrom = "")
        {
            if (tripModel != null)
            {
                return Ok(_tripSerivce.Addtrip(tripModel, GetProfile().Id, ""));
            }
            else
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Values not supplied", false));
            }
        }
        [Authorize(Roles = "Super Admin,Admin,Operator")]
        [HttpPost]
        public IHttpActionResult AddTripAsDefult(Buisness.Core.DbContext.TripDefult tripDefult)
        {
            tripDefult.UId = GetProfile().Id;
            return Ok(_tripSerivce.AddTripAsDefult(tripDefult));
        }
        [Authorize(Roles = "Super Admin,Admin,Operator")]
        [HttpPost]
        public IHttpActionResult DeleteDraftTrip(long Id)
        {
            return Ok(_tripSerivce.DeletDraftTrip(Id));
        }

        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetTripByTripId(long Id,string callfrom= "")
        {
            try
            {
                HttpRequestHeaders headers = this.Request.Headers;
                if (User.Identity.IsAuthenticated && GetProfile().RolId == 4)
                {
                    return Ok(_tripSerivce.GetTripById(Id, GetProfile().Id, GetProfile().RolId, callfrom));
                }
                return Ok(_tripSerivce.GetTripById(Id, 0,0, callfrom));
            }
            catch (Exception ex)
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false));
            }
        }

        [Authorize(Roles = "Operator")]
        [HttpGet]
        public IHttpActionResult GetTripByUser()
        {
            try
            {
                return Ok(_tripSerivce.GetTripByUserId(GetProfile().Id));
            }
            catch (Exception ex)
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false));
            }
        }

        [Authorize(Roles = "Operator")]
        [HttpGet]
        public IHttpActionResult GetAllTripByUser()
        {
            try
            {
                return Ok(_tripSerivce.GetAllTripByUserId(GetProfile().Id));
            }
            catch (Exception ex)
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false));
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetAllTrips(int pageNo=1,int pageSize=10)
        {

            try
            {
                HttpRequestHeaders headers = this.Request.Headers;
                if (User.Identity.IsAuthenticated && GetProfile().RolId == 4)
                {
                     return Ok(_tripSerivce.GetAllTrips(GetProfile().Id,pageNo,pageSize));
                }
                return Ok(_tripSerivce.GetAllTrips(0,pageNo,pageSize));
            }
            catch (Exception ex)
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false));
            }


        }


        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetAllStatusTrips(int pageNo = 1, int pageSize = 10)
        {

            try
            {
                HttpRequestHeaders headers = this.Request.Headers;
                if (User.Identity.IsAuthenticated && GetProfile().RolId == 4)
                {
                    return Ok(_tripSerivce.GetAllStatusTrips(GetProfile().Id, pageNo, pageSize));
                }
                return Ok(_tripSerivce.GetAllStatusTrips(0, pageNo, pageSize));
            }
            catch (Exception ex)
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false));
            }


        }

   

        [Authorize(Roles = "Operator")]
        [HttpGet]
        public IHttpActionResult GetScheduleTripViews()
        {
            try
            {
                return Ok(_tripSerivce.GetScheduleTripViews(GetProfile().Id));
            }
            catch (Exception ex)
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false));
            }
        }


        [Authorize(Roles = "Traveller")]
        [HttpGet]
        public IHttpActionResult GetFavtTrips(int pageNo=1,int pageSize=10)
        {

            try
            {
                long user_id = GetProfile().Id;
                return Ok(_tripSerivce.GetFavtTrips(user_id,pageNo,pageSize));
            }
            catch (Exception ex)
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false));
            }
        }



        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetAllBoostTrips()
        {
            try
            {
                HttpRequestHeaders headers = this.Request.Headers;
                if (User.Identity.IsAuthenticated && GetProfile().RolId == 4)
                {
                    return Ok(_tripSerivce.GetAllBoostTrips(GetProfile().Id));
                }
                return Ok(_tripSerivce.GetAllBoostTrips(0));
            }
            catch (Exception ex)
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false));
            }

         
        }

        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetFilterdTrip([FromUri] TripFilter tripFilter)
        {
            


            try
            {
                HttpRequestHeaders headers = this.Request.Headers;
                if (User.Identity.IsAuthenticated && GetProfile().RolId == 4)
                {
                   
                    return Ok(_tripSerivce.TripFiltraion(tripFilter, GetProfile().Id));
                }
                
                return Ok(_tripSerivce.TripFiltraion(tripFilter,0));
            }
            catch (Exception ex)
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false));
            }

        }


        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult GetWebFilterdTrip(TripFilter tripFilter)
        {



            try
            {
                long id = 0;
                HttpRequestHeaders headers = this.Request.Headers;
                if (User.Identity.IsAuthenticated && GetProfile().RolId == 4)
                {
                    id = GetProfile().Id;
                   // return Ok(_tripSerivce.WebTripFiltraion(tripFilter, GetProfile().Id));
                }

                if (tripFilter == null)
                {
                    return Ok(_tripSerivce.GetAllTrips(id,tripFilter.pageNo,tripFilter.pageSize));

                }

                else if ((tripFilter.Title == "" || tripFilter.Title == null) && tripFilter.FromId == 0 && tripFilter.ToId == 0 && tripFilter.cat_Id == 0)
                {
                    return Ok(_tripSerivce.GetAllTrips(id, tripFilter.pageNo, tripFilter.pageSize));

                }
                else if (tripFilter.Title != "" && tripFilter.Title != null && tripFilter.FromId == 0 && tripFilter.ToId == 0 && tripFilter.cat_Id == 0)
                {
                    return Ok(_tripSerivce.SearchTrip(tripFilter.Title, id));
                }
                else if (tripFilter.FromId != 0 && tripFilter.ToId != 0 && tripFilter.cat_Id != 0)
                {
                    return Ok(_tripSerivce.WebTripFiltraion(tripFilter, id,"a"));
                }
                else if (tripFilter.FromId != 0 && tripFilter.ToId == 0 && tripFilter.cat_Id == 0)
                {
                    return Ok(_tripSerivce.WebTripFiltraion(tripFilter, id, "b"));

                }
                else if (tripFilter.FromId == 0 && tripFilter.ToId != 0 && tripFilter.cat_Id == 0)
                {
                    return Ok(_tripSerivce.WebTripFiltraion(tripFilter, id, "c"));
                }
                else if (tripFilter.FromId == 0 && tripFilter.ToId == 0 && tripFilter.cat_Id != 0)
                {
                    return Ok(_tripSerivce.WebTripFiltraion(tripFilter, id, "d"));
                }
                else if (tripFilter.FromId != 0 && tripFilter.ToId != 0 && tripFilter.cat_Id == 0)
                {
                    return Ok(_tripSerivce.WebTripFiltraion(tripFilter, id, "e"));
                }
                else if (tripFilter.FromId == 0 && tripFilter.ToId != 0 && tripFilter.cat_Id != 0)
                {
                    return Ok(_tripSerivce.WebTripFiltraion(tripFilter, id, "f"));
                }
                else if (tripFilter.FromId != 0 && tripFilter.ToId == 0 && tripFilter.cat_Id != 0)
                {
                    return Ok(_tripSerivce.WebTripFiltraion(tripFilter, id, "g"));
                }
                else
                {
                    return Ok(_tripSerivce.GetAllTrips(id, tripFilter.pageNo, tripFilter.pageSize));
                }



               // return Ok(_tripSerivce.WebTripFiltraion(tripFilter, id));
            }
            catch (Exception ex)
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false));
            }

        }


        [HttpGet]
        [Authorize(Roles = "Super Admin,Admin,Operator")]
        public IHttpActionResult GetReviewsByTripId(long id)
        {
            try
            {
                return Ok(_tripSerivce.GetTripReviewByTripId(id));
            }
            catch (Exception ex)
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false));
            }
        }

        [HttpPost]
        public IHttpActionResult AddTripImages(DataEntites.Model.Trip.TripImages tripImages)
        {
            return Ok(_tripSerivce.AddTripImage(tripImages));
        }


        [HttpPost]
        public IHttpActionResult AddTempTripImage(Models.Trip.TripImage image)
        {
            return Ok(_tripSerivce.TempTripImageUpload(image.ImageString,image.ImagePath, GetProfile().Id));
        }

        [HttpPost]
        public IHttpActionResult DeleteTripImage(int? Id)
        {
            return Ok(_tripSerivce.DeleteTripImage((int)Id));
        }

        [HttpGet]
        public IHttpActionResult GetTripImagesByTripId(long Id)
        {
           
            return Ok(_tripSerivce.GetTripImagesByTripId(Id));
        }

        [HttpGet]
        public IHttpActionResult GetTripReviewsByTripId(long Id)
        {
            return Ok(_tripSerivce.GetTripReviewByTripId(Id));
        }

        [HttpPost]
        public IHttpActionResult TripStatusUpdate(StatusUpdater tripStatus)
        {
            return Ok(_tripSerivce.ActiveDActiveTip(tripStatus.Id, tripStatus.Status));
        }

        [HttpPost]
        public IHttpActionResult TripReviewStatusUpdate(StatusUpdater tripStatus)
        {
            return Ok(_tripSerivce.ActiveDActiveTripReviewById(tripStatus.Id, tripStatus.Status));
        }

        [HttpPost]
        public IHttpActionResult TripImageStatusUpdate(StatusUpdater tripStatus)
        {
            return Ok(_tripSerivce.ActiveDeActiveTripImage(tripStatus.Id, tripStatus.Status));
        }

        [HttpPost]
        public IHttpActionResult UpdateTrip(TripModel tripModel)
        {
            if (tripModel != null)
            {
                return Ok(_tripSerivce.UpdateTrip(tripModel));
            }
            else
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Please provied values for update", false));
            }
        }

        [HttpPost]
        public IHttpActionResult UpdateTripReview(DataEntites.Model.Trip.TripReview tripReview)
        {
            if (tripReview != null)
            {
                return Ok(_tripSerivce.UpdateTripReview(tripReview));
            }
            else
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Please provied values for update", false));
            }
        }

        [HttpPost]
        [Authorize(Roles = "Operator,Traveller")]
        public IHttpActionResult UpdateTripReviewComments(DataEntites.Model.Trip.TripReview tripReview)
        {
            if (tripReview != null)
            {
                return Ok(_tripSerivce.UpdateTripComment(tripReview));
            }
            else
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Please provied values for update", false));
            }
        }

        [HttpPost]
        [Authorize(Roles = "Traveller")]
        public IHttpActionResult AddReview(DataEntites.Model.Trip.TripReview tripReview)
        {
            if (tripReview != null)
            {
                tripReview.UId = GetProfile().Id;
                return Ok(_tripSerivce.AddReview(tripReview));
            }
            else
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Please provied values for reivew", false));
            }
        }

       

        [HttpPost]
        public IHttpActionResult DeleteTempTripImage(Models.Trip.TripImage image)
        {
            image.ImagePath = HttpContext.Current.Server.MapPath(image.ImagePath);
            return Ok(_tripSerivce.DeleteTempTripImage(image.ImagePath));
        }

        [HttpPost]
        public IHttpActionResult AddTempTripImageList(List<TripTempImagesList> image)
        {
            return Ok(_tripSerivce.TempTripImageUpload(image, GetProfile().Id));
        }

        [HttpPost]
        public IHttpActionResult UpdateTripStatus(TripStatus tripStatus)
        {
            return Ok(_tripSerivce.UpdateTripStatus(GetProfile().Id, tripStatus));
        }

        [HttpGet]
        [Authorize(Roles = "Super Admin,Admin,Traveller")]
        public IHttpActionResult GetTripCompresion(int FromId, int ToId)
        {
            TripComparison tripComparison = new TripComparison();
            tripComparison.FromId = FromId;
            tripComparison.ToId = ToId;
            return Ok(_tripSerivce.TripCommpersion(tripComparison));
        }

        [HttpPost]
        [Authorize(Roles = "Operator")]
        public IHttpActionResult UpdateNoMoreBookingStatus(Buisness.Core.DbContext.Trip trip)
        {
            return Ok(_tripSerivce.UpdateBookingStatus(trip));
        }

        [HttpGet]
        [Authorize(Roles = "Operator")]
        public IHttpActionResult GetReportByTripId(long TId)
        {
            return Ok(_tripSerivce.SendSingleTripReport(TId, GetProfile().Email));
        }
        [HttpGet]
        [Authorize(Roles = "Operator")]
        public IHttpActionResult GetTripCountByUser()
        {
            return Ok(_tripSerivce.GetTripCountByUser(GetProfile().Id));
        }

        [HttpGet]
        [Authorize(Roles = "Operator")]
        public IHttpActionResult GetOperatorGraphData()
        {
            return Ok(_tripSerivce.GetOperatorGraphData(GetProfile().Id));
        }


        #region Webiste
        [HttpGet]
        [Authorize(Roles = "Operator")]
        public IHttpActionResult GetDraftAllTrips()
        {
            return Ok(_tripSerivce.GetAllDraftTripsByUsers(GetProfile().Id));
        }
        [HttpGet]
        [Authorize(Roles = "Operator")]
        public IHttpActionResult GetDraftTripById(long Id)
        {
            return Ok(_tripSerivce.GetAllDraftTripsByUsers(GetProfile().Id, Id));
        }

        #endregion

        #region Search Trip
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult getTripByPlaceId(long place_id,int pageNo = 1,int pageSize=10)
        {
            try
            {
                HttpRequestHeaders headers = this.Request.Headers;
                if (User.Identity.IsAuthenticated && GetProfile().RolId == 4)
                {
                    return Ok(_tripSerivce.getTripByPlaceId(place_id, GetProfile().Id,pageNo,pageSize));
                   
                }
                return Ok(_tripSerivce.getTripByPlaceId(place_id,0, pageNo, pageSize));
            }
            catch (Exception ex)
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false));
            }
           
        }

        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult getTripByOptId(long optId)
        {
            
            try
            {
                HttpRequestHeaders headers = this.Request.Headers;
                if (User.Identity.IsAuthenticated && GetProfile().RolId == 4)
                {
                    return Ok(_tripSerivce.getTripByOptId(optId, GetProfile().Id));

                }
                return Ok(_tripSerivce.getTripByOptId(optId,0));
            }
            catch (Exception ex)
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false));
            }
        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult SearchTrip(string keyword)
        {

            try
            {
                HttpRequestHeaders headers = this.Request.Headers;
                if (User.Identity.IsAuthenticated && GetProfile().RolId == 4)
                {
                    return Ok(_tripSerivce.SearchTrip(keyword,GetProfile().Id));
                }
                return Ok(_tripSerivce.SearchTrip(keyword,0));
            }
            catch (Exception ex)
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false));
            }
          
        }
        #endregion

        #region Admin Section

        [Authorize(Roles = "Super Admin,Admin")]
        public IHttpActionResult GetAllTripCount()
        {
            return Ok(_tripSerivce.GetTripStatusCount());
        }

        [Authorize(Roles = "Super Admin,Admin")]
        public IHttpActionResult GetAllTripsForAdmin()
        {
            return Ok(_tripSerivce.GetAllTripForAdmin());
        }

        [Authorize(Roles = "Super Admin,Admin")]
        public IHttpActionResult GetAllTripsOrBookedTrips(int roleId, long Uid,int pageNo=1,int pageSize=10)
        {
            if (roleId == 3)
            {
                return Ok(_tripSerivce.GetTripByUserId(Uid));
            }
            else
            {
                return Ok(_tripBooking.GetScheduledTripsByUserId(Uid, pageNo, pageSize));
            }
        }

        //[HttpGet]
        //[Authorize(Roles = "Super Admin,Admin")]
        //public IHttpActionResult GetTotalToBeReviwedCommentsCount()
        //{
        //}
        [AllowAnonymous]
        public IHttpActionResult GetTripByOptIdAndStatusId(long Id, int statusId, int pageNo=1, int pageSize=10)
        {
            return Ok(_tripSerivce.GetTripByUserIdAndStatusId(Id,statusId,pageNo,pageSize));
        }
        [AllowAnonymous]
        public IHttpActionResult GetTripByIdForShare(long Id)
        {
            return Ok(_tripSerivce.GetTripByIdForShare(Id));
        }
        #endregion Admin Section
    }
}