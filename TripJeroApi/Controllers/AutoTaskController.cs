﻿using Buisness.Core.AutoTask;
using System.Web.Http;

namespace TripJeroApi.Controllers
{
    public class AutoTaskController : ApiController
    {
        private readonly IAutoTask _autoService;

        public AutoTaskController(IAutoTask autoService)
        {
            _autoService = autoService;
        }

        [HttpGet]
        public IHttpActionResult UpdateStatus()
        {
            return Ok(_autoService.UpdateStatus());
        }

        [HttpGet]
        public IHttpActionResult SendWeeklyNotification()
        {
            _autoService.SendWeeklyTripsNotifications();
            return Ok("Notification Sent");
        }

        [HttpGet]
        public IHttpActionResult SendTwoDaysBeforNotification()
        {
            _autoService.SendTwoDayBeforNotfication();
            return Ok("Notification Sent");
        }
    }
}