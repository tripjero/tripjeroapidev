﻿using Buisness.Core.TripComments;
using DataEntites.Model.Comments;
using System.Web.Http;
using TripJeroApi.ExtensionController;

namespace TripJeroApi.Controllers
{
    public class TripCommentsController : ExtendedController
    {
        private readonly ITripComments _commentsService;

        public TripCommentsController(ITripComments tripComments)
        {
            _commentsService = tripComments;
        }

        [HttpGet]
        [Authorize(Roles = "Operator,Traveller")]
        public IHttpActionResult GetCommentsByTrip(long TId)
        {
            return Ok(_commentsService.GetTripCommentsByTripIdAndOpId(GetProfile().Id, TId));
        }

        [HttpGet]
        [AllowAnonymous]
        //[Authorize(Roles = "Operator,Traveller")]
        public IHttpActionResult GetTripComments(long TId,int pageNo=1,int pageSize=10)
        {
            return Ok(_commentsService.GetTripCommentsByTripId(TId,pageNo,pageSize));
        }

        [HttpPost]
        [Authorize(Roles = "Traveller,Operator")]
        public IHttpActionResult AddCommentsByTripId(TravlerComments travlerComments)
        {
            return Ok(_commentsService.AddComment(GetProfile().Id, travlerComments));
        }

        [HttpPost]
        [Authorize(Roles = "Operator")]
        public IHttpActionResult AddReplyCommentsByTrip(TripReplyComments tripReplyComments)
        {
            return Ok(_commentsService.AddReply(GetProfile().Id, tripReplyComments));
        }

        [HttpGet]
        [Authorize(Roles = "Admin,Super Admin")]
        public IHttpActionResult GetTripCommentsByTId(long Id,int pageNo=1,int pageSize=10)
        {
            return Ok(_commentsService.GetTripCommentsByTripId(Id,pageNo,pageSize));
        }

        [HttpPost]
        [Authorize(Roles = "Operator")]
        public IHttpActionResult TripCommentsSpamStatusUpdate(DataEntites.Model.Comments.TripComments tripComments)
        {
            return Ok(_commentsService.UpdateCommentIsSpam(tripComments));
        }

        #region Admin

        [HttpPost]
        [Authorize(Roles = "Admin,Super Admin")]
        public IHttpActionResult UpdateTripComments(DataEntites.Model.Comments.TripComments tripComment)
        {
            return Ok(_commentsService.UpdateTripComments(tripComment));
        }

        #endregion Admin
    }
}