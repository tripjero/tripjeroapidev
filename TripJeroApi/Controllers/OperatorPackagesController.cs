﻿using Buisness.Core.OperatorPackages;
using DataEntites.Model.OperatorPackages;
using System.Web.Http;
using TripJeroApi.ExtensionController;

namespace TripJeroApi.Controllers
{
    public class OperatorPackagesController : ExtendedController
    {
        private readonly IOperatorPackage _operatorSerice;

        public OperatorPackagesController(IOperatorPackage operatorSerice)
        {
            _operatorSerice = operatorSerice;
        }

        [HttpPost]
        public IHttpActionResult AddNewPackage(OperatorPackagesModel operatorPackages)
        {
            if (operatorPackages != null)
            {
                return Ok(_operatorSerice.AddNewPackage(operatorPackages));
            }
            else
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Values must be supplied", false));
            }
        }
        [Authorize(Roles = "Super Admin")]
        [HttpPost]
        public IHttpActionResult DeletePackage(long Id)
        {
            return Ok(_operatorSerice.DeleteOperatorPackage(Id, GetProfile().Id));
        }
        [HttpPost]
        public IHttpActionResult UpdatePackages(OperatorPackagesModel operatorPackages)
        {
            if (operatorPackages != null)
            {
                return Ok(_operatorSerice.UpdatePackage(operatorPackages));
            }
            else
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Values must be supplied", false));
            }
        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult GetAllPackages()
        {
            return Ok(_operatorSerice.GetAllPackages());
        }
        [Authorize(Roles = "Operator")]
        [HttpPost]
        public IHttpActionResult SubPackage(long pid)
        {
            return Ok(_operatorSerice.PackageSubscription(pid,GetProfile().Id));
        }
        [Authorize(Roles = "Operator")]
        [HttpGet]
        public IHttpActionResult GetUsersBoostPackage(int pageNo=1, int pageSize=10)
        {
            return Ok(_operatorSerice.GetBoostPackageByUser(GetProfile().Id,pageNo,pageSize));
        }

        [Authorize(Roles = "Operator")]
        [HttpGet]
        public IHttpActionResult GetOperatorPackageById(long Id)
        {
            return Ok(_operatorSerice.GetOperatorPackageById(Id));
        }
    }
}