﻿using Buisness.Core.TripBooking;
using DataEntites.Model.TripBooking;
using System.Web.Http;
using TripJeroApi.ExtensionController;

namespace TripJeroApi.Controllers
{
    public class TravelerController : ExtendedController
    {
        private readonly ITripBooking _tripBooking;

        public TravelerController(ITripBooking tripBooking)
        {
            _tripBooking = tripBooking;
        }

        [HttpPost]
        [Authorize(Roles = "Traveller")]
        public IHttpActionResult BookTrip(TripBooking tripBooking)
        {
            if (tripBooking != null)
            {
                tripBooking.UId = GetProfile().Id;
                var result = _tripBooking.BookTrip(tripBooking, GetProfile().FirebaseToken);
                return Ok(result);
            }
            else
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Values must be supplied", false));
            }
        }

        [HttpGet]
        [Authorize(Roles = "Traveller")]
        public IHttpActionResult GetUsersBookedTrip(int pageNo=1,int pageSize=10)
        {
            return Ok(_tripBooking.GetScheduledTripsByUserId(GetProfile().Id, pageNo,pageSize));
        }

        [HttpPost]
        [Authorize(Roles = "Traveller,Operator")]
        public IHttpActionResult AddComplaint(Buisness.Core.DbContext.Complaint complaint)
        {
            complaint.UId = GetProfile().Id;
            return Ok(_tripBooking.AddComplaint(complaint));
        }

        [HttpPost]
        [Authorize(Roles = "Traveller")]
        public IHttpActionResult TripCancelation(CanccledModel canccledModel)
        {
            return Ok(_tripBooking.CanclledTrip(GetProfile().Id, canccledModel.Bid));
        }

    }

    public class CanccledModel
    {
        public long Bid { get; set; }
    }
}