﻿using Buisness.Core.Operator;
using System.Web.Http;
using TripJeroApi.ExtensionController;

namespace TripJeroApi.Controllers
{
    public class OperatorController : ExtendedController
    {
        private readonly IOperator _operatorService;

        public OperatorController(IOperator operatorService)
        {
            _operatorService = operatorService;
        }

        [HttpGet]
        [Authorize(Roles = "Operator")]
        public IHttpActionResult GetOperatorTripsWithStatus(int statusId)
        {
            return Ok(_operatorService.GetOperatorTripsWithStatus(GetProfile().Id, statusId));
        }

        [HttpGet]
        [Authorize(Roles = "Operator")]
        public IHttpActionResult GetOperatorPackages()
        {
            return Ok(_operatorService.GetOperatorPackageInfo(GetProfile().Id));
        }
    }
}