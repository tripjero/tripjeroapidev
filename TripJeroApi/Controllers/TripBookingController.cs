﻿using Buisness.Core.Trip;
using Buisness.Core.TripBooking;
using DataEntites.Model.TripBooking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TripJeroApi.ExtensionController;

namespace TripJeroApi.Controllers
{

    [Authorize(Roles = "Super Admin,Admin,Operator,Traveller")]
    public class TripBookingController  : ExtendedController
    {

        private readonly ITripBooking _tripBooking;

        public TripBookingController(ITrip trip, ITripBooking tripBooking)
        {
           
            _tripBooking = tripBooking;
        }


        [Authorize(Roles = "Operator")]
       
        [HttpGet]
        public IHttpActionResult GetOperatorScheduleBookingTrips(int pageNo=1,int pageSize=10)
        {
            try
            {
                return Ok(_tripBooking.GetOperatorScheduleBookingTrips(GetProfile().Id, pageNo,pageSize));
            }
            catch (Exception ex)
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false));
            }
        }

        [Authorize(Roles = "Operator")]
        [HttpGet]
        public IHttpActionResult GetOperatorOldBookingTrips(int pageNo = 1, int pageSize = 10)
        {
            try
            {
                return Ok(_tripBooking.GetOperatorOldBookingTrips(GetProfile().Id,pageNo,pageSize));
            }
            catch (Exception ex)
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false));
            }
        }


        [Authorize(Roles = "Operator")]
        [HttpPost]
        public IHttpActionResult UpdateBookingStatus(OperatorBookings req)
        {
            try
            {
                return Ok(_tripBooking.UpdateBookingStatus(req));
            }
            catch (Exception ex)
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false));
            }
        }

    }
}
