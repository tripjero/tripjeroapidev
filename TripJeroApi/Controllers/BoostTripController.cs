﻿using Buisness.Core.BoostTrip;
using DataEntites.Model.BoostTrip;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TripJeroApi.ExtensionController;

namespace TripJeroApi.Controllers
{
    public class BoostTripController : ExtendedController
    {
        private readonly IBoostTrip _boostTrip;
        public BoostTripController(IBoostTrip boostTrip)
        {
            _boostTrip = boostTrip;
        }

        [HttpGet]
        [Authorize(Roles = "Operator,Super Admin,Admin")]
        public IHttpActionResult GetTripBoostPackages(string callFrom = "")
        {
            return Ok(_boostTrip.GetAllBoostPackages(callFrom));
        }
        [HttpGet]
        [Authorize(Roles = "Operator")]
        public IHttpActionResult BoostTrip(long pincode)
        {
            return Ok(_boostTrip.VarifyBoostPin(pincode, GetProfile().Id));
        }
        [HttpPost]
        [Authorize(Roles = "Operator")]
        public IHttpActionResult BoostPackageSubscription(BoostPackageSubModel boostPackageSubscription)
        {
            boostPackageSubscription.ModifyById = GetProfile().Id;
            boostPackageSubscription.UId = GetProfile().Id;
            return Ok(_boostTrip.BoostPackageSubscription(boostPackageSubscription));
        }
        [HttpGet]
        [Authorize(Roles = "Operator")]
        public IHttpActionResult GetBoostPromoDetails(string prmoCode,long bid)
        {
            return Ok(_boostTrip.GetBoostPinDetails(prmoCode, bid));
        }
        [HttpGet]
        [Authorize(Roles = "Operator")]
        public IHttpActionResult GetBoostVarificationByUser(long prmoCode)
        {
            return Ok(_boostTrip.BoostPakageVarificationDetails(prmoCode, GetProfile().Id));
        }
       
    }
}
