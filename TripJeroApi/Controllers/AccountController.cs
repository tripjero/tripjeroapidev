﻿using Buisness.Core.Account;
using DataEntites.Model.Account;
using DataEntites.Model.Utilities;
using System.Threading.Tasks;
using System.Web.Http;
using TripJeroApi.Models.Account;

namespace TripJeroApi.Controllers
{
    public class AccountController : ApiController
    {
        private readonly IAccount account;
        private readonly FacebookAuthentication facebookAuthentication;
        private readonly GoogleAuthentication googleAuthentication;

        public AccountController(IAccount _account)
        {
            account = _account;
            facebookAuthentication = new FacebookAuthentication(_account);
            googleAuthentication = new GoogleAuthentication(_account);
        }

        [HttpGet]
        public IHttpActionResult ForgetPassword(string email)
        {
            return Ok(account.ForegetPassword(email));
        }

        [HttpPost]
        public IHttpActionResult ResetPassword(ResetPassword resetPassword)
        {
            return Ok(account.ResetPassword(resetPassword));
        }

        //[HttpGet]
        //public async Task<IHttpActionResult> ExternalLogin(string token, string provider)
        //{
        //    if (provider == "fb")
        //    {
        //        //newToken = await facebookAuthentication.GetToken(token);
        //        return Ok(await facebookAuthentication.GetToken(token));
        //    }
        //    else if (provider == "google")
        //    {
        //        return Ok(googleAuthentication.GetToken(token));
        //        //newToken = googleAuthentication.GetToken(token);
        //    }
        //    return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Token found", false));
        //}

        [HttpPost]
        public async Task<IHttpActionResult> WebSocialLogin(SocialMediLoginReq req)
        {
            if (req.provider == "fb")
            {
                //newToken = await facebookAuthentication.GetToken(token);
                return Ok(await facebookAuthentication.GetRoleBaseToken(req.token, req.role_id));
            }
            else if (req.provider == "google")
            {
                return Ok(googleAuthentication.GetToken(req.token, req.role_id));
                //newToken = googleAuthentication.GetToken(token);
            }
            return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Token found", false));
        }

    }
}