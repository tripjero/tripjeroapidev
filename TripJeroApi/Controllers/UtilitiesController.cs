﻿using Buisness.Core.Address;
using Buisness.Core.DbContext;
using Buisness.Core.UtitliesSection;
using DataEntites.Model.Utilities;
using System.Web.Http;
using TripJero.Notification.Services;
using TripJeroApi.ExtensionController;
using Utilties.Genral;

namespace TripJeroApi.Controllers
{
    [Authorize(Roles = "Super Admin,Admin,Operator,Traveller")]
    public class UtilitiesController : ExtendedController
    {
        private readonly AddressService _AddressService;
        private readonly IUtilities _utilities;

        public UtilitiesController(AddressService AddressService, IUtilities utilities)
        {
            _AddressService = AddressService;
            _utilities = utilities;
        }

        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetCities()
        {
            return Ok(ObjectSerilization.GetSerilizedObject("Cities Found", true, _AddressService.GetCity()));
        }

        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult sendtestemail()
        {
            //string EmailString = Utilties.Genral.EmailTemplates.WelcomeEmailString("Mehdi","himehdi2012@gmail.com");
            //EmailSender.SendEmail("ghulammehdi178@gmail.com", "Welcome to TripJero", EmailString);

            // EmailString = Utilties.Genral.EmailTemplates.resetpass("Mehdi", "himehdi2012@gmail.com","9089");
            //EmailSender.SendEmail("ghulammehdi178@gmail.com", "Welcome to TripJero", EmailString);



            string EmailString = Utilties.Genral.EmailTemplates.boostpkgsubscription("Mehdi","Temp Package","545","24","538", "9089","10");
            //EmailSender.SendEmail("sherullahbaig@gmail.com", "Test Tripjero Email", EmailString);

            EmailSender.SendEmail("wassizafar786@gmail.com", "Test Tripjero Email", EmailString);

            return Ok(ObjectSerilization.GetSerilizedObject("Email sending...", true, ""));

        }

        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult sendtestPushNotification(string token = "")
        {
            NotificationSender.SendNotification(token, "Test Notification" , "TripJero");


            return Ok(ObjectSerilization.GetSerilizedObject("Noti sending", true, ""));

        }

        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult getFcmTokenByEmail(string email = "")
        {
            return Ok(_utilities.getFcmTokenByEmail(email));

      
        }


        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetCityByProvinceId(int id)
        {
            return Ok(ObjectSerilization.GetSerilizedObject("Cities Found", true, _AddressService.GetCityByProvinceId(id)));
        }

        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetProvinces()
        {
            return Ok(ObjectSerilization.GetSerilizedObject("Cities Found", true, _AddressService.GetProvinces()));
        }

        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetProvincesByCountryId(int id)
        {
            return Ok(ObjectSerilization.GetSerilizedObject("Provinces Found", true, _AddressService.GetProvincesbyCountryId(id)));
        }

        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetCountry()
        {
            return Ok(ObjectSerilization.GetSerilizedObject("Cities Found", true, _AddressService.GetCountries()));
        }

        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetVistPlaces()
        {
            return Ok(_utilities.GetVisitPlaces());
        }


        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetVistPlacesWithCount()
        {
            return Ok(_utilities.GetVistPlacesWithCount());
        }

        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetTripStatus()
        {
            return Ok(_utilities.GetTripStatus());
        }



        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetCatagories()
        {
            return Ok(_utilities.GetTripCategories());
        }

        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetTravCounters()
        {
            return Ok(_utilities.GetTravCounters());
        }


        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult SendPushAlert(NotificationHistory noti)
        {
            return Ok(_utilities.AllTravPushNotification(noti));
        }

       
        [AllowAnonymous]
        public IHttpActionResult getCurrency()
        {
            return Ok(_utilities.getCurrency());
        }

        #region Admin
        [HttpPost]
        [Authorize(Roles = "Super Admin,Admin")]
        public IHttpActionResult UpdateVisitPlaces(VisitplaceModal visitplaceModal)
        {
            return Ok(_utilities.AddNewVisitPlace(visitplaceModal));
        }

        [HttpPost]
        [Authorize(Roles = "Super Admin,Admin")]
        public IHttpActionResult UpdateCity(Buisness.Core.DbContext.City city)
        {
            return Ok(_utilities.UpdateCity(city));
        }
        [HttpGet]
        [Authorize(Roles = "Super Admin,Admin,Operator,Traveller")]
        public IHttpActionResult GetNotficationListByUser(int pageNo=1, int pageSize=10)
        {

            return Ok(_utilities.GetNotificationHistory(GetProfile().Id,pageNo,pageSize));
        }
        [HttpGet]
        [Authorize(Roles = "Super Admin,Admin,Operator,Traveller")]
        public IHttpActionResult DeleteNotificationById(long Id)
        {

            return Ok(_utilities.DeletNotificationById(Id));
        }
        [HttpGet]
        [Authorize(Roles = "Super Admin,Admin,Operator,Traveller")]
        public IHttpActionResult DeleteAllNotificationByUser()
        {
            
            return Ok(_utilities.DeleteAllNotification(GetProfile().Id));
        }
        [HttpPost]
        [Authorize(Roles ="Super Admin")]
        public IHttpActionResult AddNewTripCateogires(TripCategory tripCategory)
        {
            return Ok(_utilities.AddTripCategories(tripCategory));
        }
        [HttpPost]
        [Authorize(Roles = "Super Admin")]
        public IHttpActionResult UpdateTripCateogires(TripCategory tripCategory)
        {
            return Ok(_utilities.UpdateTripCategory(tripCategory));
        }
        [HttpPost]
        [Authorize(Roles = "Super Admin")]
        public IHttpActionResult DeleteTripCateogires(int Id)
        {
            return Ok(_utilities.DeleteCategory(Id));
        }
        #endregion


    }
}