﻿using Buisness.Core.DbContext;
using Buisness.Core.Promo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TripJeroApi.ExtensionController;

namespace TripJeroApi.Controllers
{
        
    public class PromoController : ExtendedController
    {
        private readonly IPromo _promo;
        public PromoController(IPromo promo)
        {
            _promo = promo;
        }
        [HttpPost]
        [Authorize(Roles = "Operator")]
        public IHttpActionResult Add(PromoTbl promoTbl)
        {
            promoTbl.UId = GetProfile().Id;
            promoTbl.ModifyId = GetProfile().Id;
            return Ok(_promo.AddNewPromo(promoTbl));
        }
        [HttpPost]
        [Authorize(Roles = "Operator")]
        public IHttpActionResult UpdateStatus([FromBody] long Id, [FromBody] bool Status)
        {
            return Ok(_promo.ChnageStatus(Id,Status, GetProfile().Id));
        }
        [HttpGet]
        [Authorize(Roles = "Operator")]
        public IHttpActionResult GetPromosByUser(int pageNo=1, int pageSize=10)
        {
            return Ok(_promo.GetPromoByUserId(GetProfile().Id,pageNo,pageSize));
        }
        [HttpGet]
        [Authorize(Roles = "Operator,Traveller")]
        public IHttpActionResult VarifyPromo(string PromoCode,long TId=0)
        {
            if (GetProfile().RolId==3)
            {
                return Ok(_promo.VarifyPromo(PromoCode, GetProfile().Id));
            }
            return Ok(_promo.VerifyPromoTravller(PromoCode,TId));

        }
    }
}
