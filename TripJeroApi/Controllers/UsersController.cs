﻿using Buisness.Core.User;
using DataEntites.Model.User;
using DataEntites.Model.User.Firebase;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Http;
using TripJeroApi.App_Start.Providers;
using TripJeroApi.ExtensionController;
using TripJeroApi.Models.User;
using Utilties.Genral;

namespace TripJeroApi.Controllers
{
    public class UsersController : ExtendedController
    {
        private readonly IUser _user;

        public UsersController(IUser user)
        {
            _user = user;
        }

        [HttpGet]
        [Authorize(Roles = "Super Admin,Admin,Operator,Traveller,UnVerified")]
        public IHttpActionResult GetUser()
        {
            return Ok(_user.GetUserById(GetProfile().Id));
        }

        public IHttpActionResult GetUserById(long? id)
        {
            var user = _user.GetUserById(GetProfile().Id);
            if (user != null)
            {
                return Ok(ObjectSerilization.GetSerilizedObject("Result Found", true, user));
            }
            else
            {
                return Ok(ObjectSerilization.GetSerilizedWithoutObject("Not Found", false));
            }
        }

        [HttpGet]
        [Authorize(Roles = "Super Admin,Admin")]
        public IHttpActionResult GetUserByEmailId(string email)
        {
            var user = _user.GetUserByEmail(email);
            if (user != null)
            {
                return Ok(ObjectSerilization.GetSerilizedObject("Result Found", true, user));
            }
            else
            {
                return Ok(ObjectSerilization.GetSerilizedWithoutObject("Not Found", false));
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult UserRegistraion(UserModel user)
        {
            if (user != null)
            {
                return Ok(_user.UserRegistraion(user));
            }
            else
            {
                return Ok(ObjectSerilization.GetSerilizedWithoutObject("Values must be supplied", false));
            }
        }


        [HttpGet]
        public IHttpActionResult AddTripToFavt(long trip_id)
        {
            long   user_id = GetProfile().Id;
            return Ok(_user.AddTripToFavt(user_id,trip_id));
        }

        [HttpGet]
        public IHttpActionResult DelFavtTrip(long trip_id)
        {
            long userid = GetProfile().Id;
            return Ok(_user.DelFavtTrip(userid, trip_id));
        }



        [Authorize(Roles = "Super Admin,Admin,Operator,Traveller")]
        public IHttpActionResult UpdateUser(UserModel user)
        {
            user.Id = GetProfile().Id;
            return Ok(_user.UserUpdate(user));
        }

        [Authorize(Roles = "Operator")]
        public IHttpActionResult UpdateOptLiecence( DataEntites.Model.User.LicenceNo userlicence)
        {
            return Ok(_user.UpdateOptLiecence(userlicence, GetProfile().Id));

        }



        [Authorize(Roles = "Super Admin,Admin,Operator,Traveller")]
        public IHttpActionResult UpdateUserProfileImage(UserProfile userProfile)
        {
            return Ok(_user.UpdateUserProfileImage(userProfile.ImageProfileString, GetProfile().Id));
        }

        [Authorize(Roles = "Super Admin,Admin,Operator,Traveller")]
        public IHttpActionResult ResetPassword(PasswordReset passwordReset)
        {
            return Ok(_user.ChangePassword(passwordReset.CurrentPassword, passwordReset.NewPassword, GetProfile().Id));
        }

        [Authorize(Roles = "Super Admin,Admin,Operator,Traveller")]
        public IHttpActionResult DeactiveAccount()
        {
            var data = _user.DeActiveAccount(GetProfile().Id);
            //HttpContext.Current.GetOwinContext().Authentication.SignOut();
            return Ok(data);
        }

        [AllowAnonymous]
        public IHttpActionResult GetTripOperatorById(long Id)
        {
            return Ok(_user.GetTripOperatorInfoById(Id));
        }

        [Authorize(Roles = "Operator")]
        [HttpPost]
        public IHttpActionResult UpdateSocialLinks(DataEntites.Model.User.OperatorInfo.UsersSocialLinks operatorSocialLinks)
        {
            operatorSocialLinks.UId = GetProfile().Id;
            return Ok(_user.AddSocialLinks(operatorSocialLinks));
        }

        [Authorize(Roles = "Super Admin,Admin,Traveller,Operator")]
        [HttpGet]
        public IHttpActionResult Get_Users_Complaint_ById(int pageNo=1,int pageSize=10)
        {
            return Ok(_user.GetComplaintByUserId(GetProfile().Id,pageNo,pageSize));
        }

        [Authorize(Roles = "Super Admin,Admin,Operator,Traveller")]
        [HttpPost]
        public IHttpActionResult UpdateFirebaseToken(FirebaseToken firebaseToken)
        {
            return Ok(_user.UpdateToken(firebaseToken, GetProfile().Id));
        }

        [Authorize(Roles = "UnVerified")]
        [HttpPost]
        public IHttpActionResult UpdateUserType(updateUserTypeStatus updateUserTypeStatus)
        {
            string newToken = "";
            bool isOk = _user.UpdateUserType(GetProfile().Id, updateUserTypeStatus.roleId);
            if (isOk)
            {
                var identityexist = (ClaimsIdentity)User.Identity;
                var roleDb = _user.GetRoleById(updateUserTypeStatus.roleId);
                var identity = new ClaimsIdentity("Bearer");
                identity.AddClaim(new Claim(ClaimTypes.Role, roleDb.Name));
                identity.AddClaim(new Claim("RId", roleDb.Id.ToString()));
                identity.AddClaim(new Claim(ClaimTypes.Name, identityexist.Name));
                identity.AddClaim(new Claim("Email", identityexist.Claims.FirstOrDefault(x => x.Type == "Email").Value));
                identity.AddClaim(new Claim("Id", GetProfile().Id.ToString()));
                identity.AddClaim(new Claim("FirebaseToken", ""));
                newToken = ExternalProvider.GenrateToken(identity);
                Thread.CurrentPrincipal = new ClaimsPrincipal(identity);
                if (HttpContext.Current != null)
                {
                    HttpContext.Current.User = Thread.CurrentPrincipal;
                }
            }
            return Ok(new { access_token = newToken });
        }

        #region Admin Section

        [HttpGet]
        [Authorize(Roles = "Super Admin,Admin")]
        public IHttpActionResult GetAllUsers()
        {
            return Ok(_user.GetAllUsers());
        }

        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetAllOperators(int pageNo=1,int pageSize=10)
        {
            return Ok(_user.GetAllOperators(pageNo,pageSize));

        }
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetAllOperatorsReviews(int pageNo = 1, int pageSize = 10)
        {
            return Ok(_user.GetAllOperatorsReviews(pageNo, pageSize));

        }
        [HttpGet]
        [Authorize(Roles = "Super Admin,Admin")]
        public IHttpActionResult GetTotalUsersRegisterd()
        {
            return Ok(_user.GetAllUsers());
        }

        [HttpGet]
        [Authorize(Roles = "Super Admin,Admin")]
        public IHttpActionResult GetUserByIdForAdmin(long id)
        {
            return Ok(_user.GetUserById(id));
        }

        [HttpPost]
        [Authorize(Roles = "Super Admin,Admin")]
        public IHttpActionResult UpdateAdminUsers(UserModel user)
        {
            return Ok(_user.UpdateAdminUser(user));
        }

        [HttpGet]
        [Authorize(Roles = "Super Admin,Admin")]
        public IHttpActionResult GetComplaint()
        {
            return Ok(_user.GetComplaint());
        }

        [HttpGet]
        [Authorize(Roles = "Super Admin,Admin")]
        public IHttpActionResult UpdateComplaintStatus(long Id, bool status, string type)
        {
            return Ok(_user.UpdateComplaintStatus(Id, status, type,GetProfile().Id));
        }

        [HttpPost]
        [Authorize(Roles = "Super Admin,Admin")]
        public IHttpActionResult UpdateTripReviews(DataEntites.Model.Trip.TripReview tripReview)
        {
            return Ok(_user.UpdateTripReviewsAdmin(tripReview));
        }

        [HttpGet]
        [Authorize(Roles = "Super Admin,Admin")]
        public IHttpActionResult GetAllCounter()
        {
            return Ok(_user.GetAllCounts());
        }

        [HttpPost]
        [Authorize(Roles = "Super Admin")]
        public IHttpActionResult UpdateStatus(long Id, bool status)
        {
            return Ok(_user.UpdateStatus(Id, status));
        }

        [HttpPost]
        [Authorize(Roles = "Super Admin")]
        public IHttpActionResult UpdateLicnsesStatus(long Id, bool status)
        {
            return Ok(_user.UpdateLicenseStatus(GetProfile().Id, Id, status));
        }

        [HttpPost]
        [Authorize(Roles = "Super Admin")]
        public IHttpActionResult UserSubPackages(bool status)
        {
            return Ok(_user.GetUsersPackageSubscription(status));
        }
        [HttpPost]
        [Authorize(Roles = "Super Admin")]
        public IHttpActionResult ChangePaymentStatusTripPackage(long Id,bool status)
        {
            return Ok(_user.ChangePaymentStatus(Id,status));
        }
        [HttpPost]
        [Authorize(Roles = "Super Admin")]
        public IHttpActionResult UserSubBoostPackages(bool status)
        {
            return Ok(_user.GetUsersBoostPackageSubscription(status));
        }
        [HttpPost]
        [Authorize(Roles = "Super Admin")]
        public IHttpActionResult ChangePaymentStatusTripBoostPackage(long Id, bool status)
        {
            return Ok(_user.ChangeBoostPackagePaymentStatus(Id, status));
        }
        #endregion Admin Section
    }

    public class updateUserTypeStatus
    {
        public int roleId { get; set; }
    }
}