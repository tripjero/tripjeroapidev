﻿using Buisness.Core.PackageSub;
using DataEntites.Model.PackageSub;
using System.Web.Http;
using TripJeroApi.ExtensionController;

namespace TripJeroApi.Controllers
{
    public class PackageSubscriptionController : ExtendedController
    {
        private readonly IPackageSubsciption _packageSubsciption;

        public PackageSubscriptionController(IPackageSubsciption packageSubsciption)
        {
            _packageSubsciption = packageSubsciption;
        }

        [HttpPost]
        public IHttpActionResult NewSubscription(PackageSubModel packageSubModel)
        {
            if (packageSubModel != null)
            {
                return Ok(_packageSubsciption.NewSubscription(packageSubModel));
            }
            else
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Values Requred", false));
            }
        }

        [HttpPost]
        public IHttpActionResult UpdateSubscription(PackageSubModel packageSubModel)
        {
            if (packageSubModel != null)
            {
                return Ok(_packageSubsciption.UpdatePackageSubscription(packageSubModel));
            }
            else
            {
                return Ok(Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Values Requred", false));
            }
        }

        [HttpPost]
        public IHttpActionResult RemoveSubscription(long id)
        {
            return Ok(_packageSubsciption.RemovePacakgeSubscription(id));
        }

        [Authorize(Roles = "Operator")]
        [HttpGet]
        public IHttpActionResult GetSubscribedPackagesbyUserId()
        {
            return Ok(_packageSubsciption.GetSubPackagesByUser(GetProfile().Id));
        }
    }
}