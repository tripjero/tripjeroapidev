﻿using Buisness.Core.DbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TripJero.Notification.Model;

namespace TripJero.Log.NotificationLog
{
    public class LogInsertion
    {
        public delegate void InsertNotificationLogEvent(NotificationHistory notificationHistory, TripJeroEntities db);

        public static void insetNotifiationToDb(NotificationHistory notificationHistory, TripJeroEntities db)
        {
            try
            {
                notificationHistory.CreatedDate = DateTime.Now;
                db.NotificationHistories.Add(notificationHistory);
                db.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static void NotificationInDb(NotificationHistory notificationHistory, TripJeroEntities db)
        {
            InsertNotificationLogEvent insertNotificationLog = new InsertNotificationLogEvent(insetNotifiationToDb);
            insertNotificationLog.BeginInvoke(notificationHistory, db, null, null);
        }
    }
}
