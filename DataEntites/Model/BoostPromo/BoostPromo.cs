﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataEntites.Model.BoostPromo
{
    public class BoostPromo
    {
        public long Id { get; set; }
        public string PromoCode { get; set; }
        public int Discount { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
        public bool IsActive { get; set; }
        public bool Is_Delete { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifyDate { get; set; }
        public Nullable<long> ModifyBy { get; set; }

    }
}
