﻿namespace DataEntites.Model.VistPlace
{
    public class VistPlaces
    {
        public long Id { get; set; }
        public string VistPlace { get; set; }
        public string PartialVisitPlace { get; set; }
        public int CityId { get; set; }
        public int ProviceId { get; set; }
        public string ImgPath { get; set; }

        public int trips { get; set; }
    }
}