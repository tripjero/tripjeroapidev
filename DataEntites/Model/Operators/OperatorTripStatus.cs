﻿namespace DataEntites.Model
{
    public class OperatorTripStatus
    {
        public long TripId { get; set; }
        public string Title { get; set; }
        public System.DateTime StartDateTime { get; set; }
        public System.DateTime EndDateTime { get; set; }
        public string Duration { get; set; }
        public double AmountPerPerson { get; set; }
        public string TripStatus { get; set; }
    }
}