﻿using System.Collections.Generic;

namespace DataEntites.Model.Operators
{
    public class OperatorPackagesInfo
    {
        public List<object> PackageDetaisl;

        public OperatorPackagesInfo()
        {
            PackageDetaisl = new List<object>();
        }

        public string PackgeName { get; set; }
        public int LimitedTrip { get; set; }
    }

    public class OperatorPackages
    {
        public string PackageName { get; set; }
        public int TripLimits { get; set; }
        public string TripLimitation { get; set; }
        public string RulsContent { get; set; }
    }
}