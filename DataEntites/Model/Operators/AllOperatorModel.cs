﻿using DataEntites.Model.Admin.User;
using DataEntites.Model.Trip;
using System;
using System.Collections.Generic;
using System.Text;
namespace DataEntites.Model.Operators
{
   public class AllOperatorModel
    {
        public Users BasicInfo { get; set; }
        public long TotalTrips { get; set; }
        public int AvgStars { get; set; }
        public int TotalRevews { get; set; }

       public List<TripModel> ScheduleTrips { get; set; }
    }
}
