﻿using System;

namespace DataEntites.Model.Admin.Trip
{
    public class Trip
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string TripOperator { get; set; }
        public string Duration { get; set; }
        public string FromPlaceName { get; set; }
        public string ToPlaceName { get; set; }
        public double Amount { get; set; }
        public string StatusName { get; set; }
    }
}