﻿namespace DataEntites.Model.Admin.Trip
{
    public class TripStatusCount
    {
        public int TotalComplete { get; set; }
        public int TotalInProgress { get; set; }
        public int TotalScheduled { get; set; }
        public int TotalHold { get; set; }
        public int TotalCancel { get; set; }
    }
}