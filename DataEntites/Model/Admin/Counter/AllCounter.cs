﻿namespace DataEntites.Model.Admin.Counter
{
    public class AllCounter
    {
        public int TotalRegistredUser { get; set; }
        public int TotalTrips { get; set; }
        public int TotalComplaint { get; set; }
        public int TotalScheduledTrips { get; set; }
        public int TotalCompleteTrips { get; set; }
        public int TotalBookedTrips { get; set; }
    }
}