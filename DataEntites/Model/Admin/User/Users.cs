﻿using DataEntites.Model.Utilities;
using System;

namespace DataEntites.Model.Admin.User
{
    public class Users:BasePaginations
    {
        public long Id { get; set; }
        public string FullName { get; set; }
        public string BuisnessName { get; set; }
        public string Gender { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public string Email { get; set; }
        public int RoleId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public long? ModifyBy { get; set; }
        public string ModifyByName { get; set; }
        public string Remarks { get; set; }
        public string ProfileImagePath { get; set; }
        public string ContactNumber { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public bool IsVarified { get; set; }
        public bool IsProfileCompleted { get; set; }
        public string ResidentialAddress { get; set; }
        public string PermentAddress { get; set; }
        public string RoleName { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }

        public bool OperatorLicActive { get; set; }
    }
}