﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataEntites.Model.Admin.Operator
{
    public class OperatorSubPackage
    {
        public string PackageName { get; set; }
        public int LimitaionTrips { get; set; }
        public string UserName { get; set; }
        public long UserId { get; set; }
        public bool IS_Payment { get; set; }
        public long SubscriptionId { get; set; }
        public long PackageID { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool? IS_Discount { get; set; }
        public double? CurrentAmmount { get; set; }
        public bool? IS_Promo { get; set; }

    }
}
