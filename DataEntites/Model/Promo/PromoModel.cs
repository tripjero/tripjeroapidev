﻿using DataEntites.Model.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataEntites.Model.Promo
{
    public class PromoModel:BasePaginations
    {
        public long Id { get; set; }
        public string PromoCode { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
        public double DiscountPercentage { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifyDate { get; set; }
        public long ModifyId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public long UId { get; set; }
        public string Promo_Key { get; set; }
        public long? TripId { get; set; }
        public string Title { get; set; }
        public int ISExpire { get; set; }
    }

}
