﻿using System;

namespace DataEntites.Model.User
{
    public class LicenceNo
    {
        public int Id { get; set; }
        public string LicenceNumber { get; set; }
        public double UId { get; set; }
        public string LicenceImage { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public double ValidBy { get; set; }
        public string ValidByName { get; set; }
        public double ModifyBy { get; set; }
        public string Remakas { get; set; }
        public bool Status { get; set; }
    }
}