﻿using System.Collections.Generic;

namespace DataEntites.Model.User.OperatorInfo
{
    public class TripOperatorInfo
    {
        public List<SocailLinks> SocailLinks;
        public List<Model.Trip.TripReviewModel.OperatorsReviews> OperatorReviews;
        public List<OperatorTrips> OperatorRecentTrips;

        public TripOperatorInfo()
        {
            SocailLinks = new List<SocailLinks>();
            OperatorReviews = new List<Model.Trip.TripReviewModel.OperatorsReviews>();
            OperatorRecentTrips = new List<OperatorTrips>();
        }

        public long OperatorId { get; set; }
        public string ProfileImage { get; set; }

        public long CompletedTrips { get; set; }

        public string BuisnessName { get; set; }
        public bool? IsVarified { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string Address { get; set; }
        public long TotalReviews { get; set; }
        public string Email { get; set; }
        public int AvgReviews { get; set; }
    }
}