﻿namespace DataEntites.Model.User.OperatorInfo
{
    public class OperatorTrips
    {
        public long Id { get; set; }
        public string Title { get; set; }
    }
}