﻿using System;

namespace DataEntites.Model.User.OperatorInfo
{
    public class OperatorReviews
    {
        public long Id { get; set; }
        public long UId { get; set; }
        public long TId { get; set; }
        public string UserName { get; set; }
        public string Review { get; set; }
        public int? Starts { get; set; }
        public DateTime ReviewDateTime { get; set; }
    }
}