﻿namespace DataEntites.Model.User.OperatorInfo
{
    public class UsersSocialLinksToShow
    {
        public int TypeId { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
    }
}