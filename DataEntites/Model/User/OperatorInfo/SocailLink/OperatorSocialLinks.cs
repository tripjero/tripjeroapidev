﻿namespace DataEntites.Model.User.OperatorInfo
{
    public class UsersSocialLinks
    {
        public SocailLink.SocailLinksType SocailLinksType;

        public UsersSocialLinks()
        {
            SocailLinksType = new SocailLink.SocailLinksType();
        }

        public long Id { get; set; }
        public string Url { get; set; }
        public long UId { get; set; }
        public int TypeId { get; set; }
    }
}