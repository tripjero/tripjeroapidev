﻿namespace DataEntites.Model.User.Firebase
{
    public class FirebaseToken
    {
        public string Token { get; set; }
    }
}