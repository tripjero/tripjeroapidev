﻿using DataEntites.Model.User.OperatorInfo;
using System;
using System.Collections.Generic;

namespace DataEntites.Model.User
{
    public class UserModel
    {
        public LicenceNo Licence;
        public List<UsersSocialLinksToShow> OperatorSocialLinks;

        public UserModel()
        {
            Licence = new LicenceNo();
            OperatorSocialLinks = new List<UsersSocialLinksToShow>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        public bool IsLicenceVarified { get; set; }
        public string Email { get; set; }

        public bool NeedToShowWelcomeBack { get; set; }
        public int RoleId { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifyDate { get; set; }
        public Nullable<DateTime> DateOfBirth { get; set; }
        public int ModifyBy { get; set; }
        public string Remarks { get; set; }
        public string Password { get; set; }
        public string ContactNumber { get; set; }
        public string CityName { get; set; }
        public int? CityId { get; set; }
        public string ProvinceName { get; set; }
        public string CountryName { get; set; }
        public string FullAddress { get; set; }
        public string RoleName { get; set; }
        public long PackageId { get; set; }
        public string PackageName { get; set; }
        public string BuisnessName { get; set; }
        public string PhoneNumber { get; set; }
        public string ProfileImage { get; set; }
        public string FirebaseToken { get; set; }
        public bool? IsVarified { get; set; }
        public bool? IsUserTypeVarified { get; set; }

        public bool? isDeactivated { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
    }
}