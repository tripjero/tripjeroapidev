﻿using DataEntites.Model.Utilities;
using System;

namespace DataEntites.Model.User.Complaint
{
    public class UsersComplaint:BasePaginations
    {
        public long ComplaintId { get; set; }
        public string ComplaintSubject { get; set; }
        public string ComplaintDetails { get; set; }
        public bool ComplaintStatus { get; set; }
        public bool ResolvedStatus { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}