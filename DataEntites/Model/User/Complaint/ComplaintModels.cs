﻿using System;

namespace DataEntites.Model.User.Complaint
{
    public class ComplaintModels
    {
        public long Id { get; set; }
        public string Subject { get; set; }
        public string ComplaintDetails { get; set; }
        public bool IsActive { get; set; }
        public bool IsResolved { get; set; }
        public DateTime CreatedDate { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public bool IsVisited { get; set; }
        public string ResolvedBy { get; set; }
    }
}