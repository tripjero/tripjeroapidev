﻿namespace DataEntites.Model.User
{
    public class ContactNumbers
    {
        public int Id { get; set; }
        public string ContactNumber { get; set; }
        public int TypeId { get; set; }
        public double UId { get; set; }
        public string ContactTypeName { get; set; }
    }
}