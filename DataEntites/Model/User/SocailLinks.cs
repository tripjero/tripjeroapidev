﻿namespace DataEntites.Model.User
{
    public class SocailLinks
    {
        public long Id { get; set; }
        public string UrlType { get; set; }
        public string Url { get; set; }
    }
}