﻿namespace DataEntites.Model.Address
{
    public class Provinces
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }
    }
}