﻿namespace DataEntites.Model.Address
{
    public class Address
    {
        public double Id { get; set; }
        public string StreetAddress { get; set; }
        public string PermentAddress { get; set; }
        public int? CityId { get; set; }
    }
}