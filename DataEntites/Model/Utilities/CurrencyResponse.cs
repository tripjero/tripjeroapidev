﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataEntites.Model.Utilities
{
    public class CurrencyResponse
    {
        public bool success { get; set; }
        public int timestamp { get; set; }
        public string @base { get; set; }
        public string date { get; set; }
        public Dictionary<string, double> rates { get; set; }

    }
}
