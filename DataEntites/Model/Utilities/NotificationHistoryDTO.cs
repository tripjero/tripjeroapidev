﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataEntites.Model.Utilities
{
    public class NotificationHistoryDTO
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public long UId { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int PageSize { get; set; }
        public int TotalPage { get; set; }
        public int PageNo { get; set; }
    }
}
