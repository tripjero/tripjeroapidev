﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataEntites.Model.Utilities
{
    public class TripCategoresModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
