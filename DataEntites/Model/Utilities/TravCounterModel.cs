﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataEntites.Model.Utilities
{
   public class TravCounterModel
    {
        public int TotalOperators { get; set; }
        public int TotalTrips { get; set; }
        public int TotalTravellers { get; set; }
        public int TotalReviews { get; set; }
    }
}
