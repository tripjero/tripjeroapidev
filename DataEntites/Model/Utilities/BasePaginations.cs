﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataEntites.Model.Utilities
{
    public class BasePaginations
    {
        public int PageSize { get; set; }
        public int TotalPage { get; set; }
        public int PageNo { get; set; }
    }
}
