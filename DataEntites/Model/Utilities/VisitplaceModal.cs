﻿namespace DataEntites.Model.Utilities
{
    public class VisitplaceModal
    {
        public long Id { get; set; }
        public string VisitPlace { get; set; }
        public int CityId { get; set; }

        public string ImgPath { get; set; }
    }
}