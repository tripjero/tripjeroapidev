﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataEntites.Model.Utilities
{
   public class SocialMediLoginReq
    {
        public string provider { get; set; }
        public int role_id { get; set; }


        public string token { get; set; }
    }
}
