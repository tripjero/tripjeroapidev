﻿namespace DataEntites.Model.Utilities
{
    public class TripStatusDropDown
    {
        public int Id { get; set; }
        public string StatusName { get; set; }
    }
}