﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataEntites.Model.BoostTrip
{
    public class BoostPackageSubModel
    {
        public long Id { get; set; }
        [Required]
        public long BoostPackageId { get; set; }
        public long UId { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifyDate { get; set; }
        public int RemningBoostTrips { get; set; }
        public Nullable<bool> IS_Payment { get; set; }
        public Nullable<bool> IS_Active { get; set; }
        public Nullable<bool> IS_Delted { get; set; }
        public Nullable<long> ModifyById { get; set; }
        [Required]
        public bool IS_Promo { get; set; }
        public string PromoCode { get; set; }

    }
}
