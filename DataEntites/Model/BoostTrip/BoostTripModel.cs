﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataEntites.Model.BoostTrip
{
   public class BoostTripModel
    {
        public long Id { get; set; }
        public int NoTrips { get; set; }
        public string BoostCode { get; set; }
        public double Price { get; set; }
        public int? Discount { get; set; }
        public bool? Is_Promo { get; set; }
        public long? PromoId { get; set; }
        public string Title { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public long ModifyBy { get; set; }
        public string PromoCode { get; set; }
    }
}
