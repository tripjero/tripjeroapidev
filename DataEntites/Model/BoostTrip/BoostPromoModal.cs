﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataEntites.Model.BoostTrip
{
    public class BoostPromoModal
    {
        public long BoostPcakgeId { get; set; }
        public long PromoId { get; set; }
        public int NoTrips { get; set; }
        public double CurrentPrice { get; set; }
        public double Price { get; set; }
        public int Discount { get; set; }
        public string Title { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
       
    }
}
