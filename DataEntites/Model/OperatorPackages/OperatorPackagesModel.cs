﻿using System;

namespace DataEntites.Model.OperatorPackages
{
    public class OperatorPackagesModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public int? Price { get; set; }
        public int LimitaionTrips { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifyDate { get; set; }
        public long ModifyById { get; set; }
        public Nullable<bool> IS_Discount { get; set; }
        public Nullable<int> Discount { get; set; }
        public double CurrentPrice { get; set; }
        public string Details { get; set; }

    }
}