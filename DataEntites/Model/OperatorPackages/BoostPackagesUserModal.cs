﻿using DataEntites.Model.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataEntites.Model.OperatorPackages
{
    public class BoostPackagesUserModal:BasePaginations
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public int TotalBoostTrips { get; set; }
        public long UId { get; set; }
        public int RemningBoostTrips { get; set; }
        public bool IS_Payment { get; set; }
        public DateTime CreatedDate { get; set; }
        public string BoostCode { get; set; }
        public bool IS_Active { get; set; }

    }
}
