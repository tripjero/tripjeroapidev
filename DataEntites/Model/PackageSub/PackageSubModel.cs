﻿using System;

namespace DataEntites.Model.PackageSub
{
    public class PackageSubModel
    {
        public long Id { get; set; }
        public long TId { get; set; }
        public long UId { get; set; }
        public long OId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public bool IsActive { get; set; }
        public string TripTitle { get; set; }
        public string UserName { get; set; }
        public string PackageName { get; set; }
    }
}