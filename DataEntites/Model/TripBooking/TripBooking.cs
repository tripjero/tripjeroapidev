﻿using DataEntites.Model.Trip;
using System;

namespace DataEntites.Model.TripBooking
{
    public class TripBooking
    {
        public TripBasicInfo TripBasicInfo;

        public TripBooking()
        {
            TripBasicInfo = new TripBasicInfo();
        }

        public long Id { get; set; }
        public long TId { get; set; }
        public long UId { get; set; }
        public bool IsComplete { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public long ModifyById { get; set; }
        public System.DateTime ModifyDate { get; set; }
        public bool IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public string Remarks { get; set; }
        public Nullable<bool> IsCanclled { get; set; }
        public int NoPersons { get; set; }
        public string PaymentStatus { get; set; }
        public double TotalPrice { get; set; }

        public int PromoId { get; set; }
        public string PromoCode { get; set; }
        public bool Is_Promo { get; set; }

        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string EmergencyContact { get; set; }
        public bool IsBookForMySelf { get; set; }
        public int Childrens { get; set; }

    }
}