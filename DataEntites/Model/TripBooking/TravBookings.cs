﻿using DataEntites.Model.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataEntites.Model.TripBooking
{
   public class TravBookings:BasePaginations
    {
        public long BId { get; set; }
        public long TripId { get; set; }
        public long OperatorId { get; set; }
        public System.DateTime BookedDate { get; set; }
        public Nullable<int> NoPersons { get; set; }
        public string PaymentStatus { get; set; }
        public Nullable<double> TotalPrice { get; set; }

        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string EmergencyContact { get; set; }
        public Nullable<int> Childrens { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public string TripTitle { get; set; }

        public string TripStatus { get; set; }


        public bool IsCanclled { get; set; }




    }
}
