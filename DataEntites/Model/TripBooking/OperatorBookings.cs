﻿using DataEntites.Model.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataEntites.Model.TripBooking
{
  public  class OperatorBookings
    {

        public long Id { get; set; }
        public long UId { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<double> TotalPrice { get; set; }
       
        public Nullable<bool> IsCanclled { get; set; }
        public Nullable<int>  NoPersons { get; set; }
        public string PaymentStatus { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string EmergencyContact { get; set; }
        public Nullable<int> Childrens { get; set; }


    }
}
