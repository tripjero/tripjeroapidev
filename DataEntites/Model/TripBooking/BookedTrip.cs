﻿namespace DataEntites.Model.TripBooking
{
    public class BookedTrip
    {
        public int Id { get; set; }
        public string TripTitle { get; set; }
        public string TripStatusName { get; set; }
    }
}