﻿namespace DataEntites.Model.Account
{
    public class FacebookModel
    {
        public string id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
    }
}