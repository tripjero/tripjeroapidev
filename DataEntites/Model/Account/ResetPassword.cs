﻿namespace DataEntites.Model.Account
{
    public class ResetPassword
    {
        public string Email { get; set; }
        public int Opt { get; set; }
        public string Password { get; set; }
    }
}