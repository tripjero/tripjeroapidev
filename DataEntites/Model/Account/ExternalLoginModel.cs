﻿namespace DataEntites.Model.Account
{
    public class ExternalLoginModel
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string Picture { get; set; }
        public string Issuser { get; set; }
        public string Token { get; set; }
        public string ProviderId { get; set; }
    }
}