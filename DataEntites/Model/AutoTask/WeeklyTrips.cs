﻿using System;

namespace DataEntites.Model.AutoTask
{
    public class WeeklyTrips
    {
        public int Weeks { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string Title { get; set; }
    }
}