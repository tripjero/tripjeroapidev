﻿using System;

namespace DataEntites.Model.AutoTask
{
    public class CurrentTripStatus
    {
        public long TripId { get; set; }
        public string StatusName { get; set; }
        public int TripStatusId { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
    }
}