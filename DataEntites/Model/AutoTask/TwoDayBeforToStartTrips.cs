﻿using System;

namespace DataEntites.Model.AutoTask
{
    public class TwoDayBeforToStartTrips
    {
        public int Days { get; set; }
        public string Title { get; set; }
        public long TripId { get; set; }
        public string FirebaseToken { get; set; }
        public long UserId { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
    }
}