﻿namespace DataEntites.Model.Comments
{
    public class TripReplyComments
    {
        public long CommentId { get; set; }
        public string ReplyComments { get; set; }
    }
}