﻿using DataEntites.Model.Utilities;
using System;

namespace DataEntites.Model.Comments
{
    public class TripComments: BasePaginations
    {
        public long TravlerId { get; set; }
        public string TravlerName { get; set; }
        public string TravlerProfileImageUrl { get; set; }
        public long OperatorId { get; set; }
        public string OperatorName { get; set; }
        public string OpreatorProfileImageUrl { get; set; }
        public long CommentId { get; set; }
        public string TravlerComment { get; set; }
        public DateTime? TravlerPostedDate { get; set; }
        public long TripId { get; set; }
        public long? CommentReplyId { get; set; }
        public string OperatorComments { get; set; }
        public DateTime? OperatorReplyDateTime { get; set; }
        public bool IsSpam { get; set; }
        public bool IsAdminReviwed { get; set; }
        public string ReviewComments { get; set; }
    }
}