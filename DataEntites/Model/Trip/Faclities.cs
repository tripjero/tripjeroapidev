﻿namespace DataEntites.Model.Trip
{
    public class Faclities
    {
        public long Id { get; set; }
        public long TripId { get; set; }
        public string Accommodiation { get; set; }
        public string Facalities { get; set; }
        public string Transportation { get; set; }


        public bool HaveTransport { get; set; }
        public bool HaveAccomodation { get; set; }
        public bool HaveMeals { get; set; }
        public bool HaveFirstAid { get; set; }
        public string Icon_Transport { get; set; }
        public string Icon_Accomodation { get; set; }
        public string Icon_Meals { get; set; }
        public string Icon_FirstAid { get; set; }

    }
}