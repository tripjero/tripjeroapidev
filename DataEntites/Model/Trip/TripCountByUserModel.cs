﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataEntites.Model.Trip
{
   public class TripCountByUserModel
    {
        public int TotalTrips { get; set; }
        public int TotalHoldTrips { get; set; }
        public int TotalScheduledTrips { get; set; }
        public int TotalInprogressTrips { get; set; }
        public int TotalCanceldTrips { get; set; }
        public int TotalCompleteTrips { get; set; }

        public int TotalBookings { get; set; }

        public int TotalTripViews { get; set; }
    }
}
