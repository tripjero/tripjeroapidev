﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataEntites.Model.Trip
{
   public class DraftModel
    {
        public int Id { get; set; }
        public string TripString { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public bool IS_FacilitiesDone { get; set; }
        public bool IS_ItnaryPlaneDone { get; set; }
        public bool IS_TripImagesDone { get; set; }
        public System.DateTime ModifyDate { get; set; }
        public Nullable<long> UId { get; set; }
    }
}
