﻿using System;

namespace DataEntites.Model.Trip.TripSearch
{
    public class TripFilter
    {
        public int FromId { get; set; }
        public int ToId { get; set; }

        public int cat_Id { get; set; }
        public DateTime StartDate { get; set; }
        public double MinAmount { get; set; }
        public double MaxAmount { get; set; }
        public string Accommodation { get; set; }
        public string Transportation { get; set; }
        public string Title { get; set; }
        public int pageNo { get; set; } = 1;
        public int pageSize { get; set; } = 10;
    }
}