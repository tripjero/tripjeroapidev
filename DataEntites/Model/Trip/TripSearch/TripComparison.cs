﻿namespace DataEntites.Model.Trip.TripSearch
{
    public class TripComparison
    {
        public long TripId { get; set; }
        public string Title { get; set; }
        public string Details { get; set; }
        public double Price { get; set; }
        public string Duration { get; set; }
        public string Accommodiation { get; set; }
        public string Facalities { get; set; }
        public string Transportation { get; set; }
        public int FromId { get; set; }
        public int ToId { get; set; }
    }
}