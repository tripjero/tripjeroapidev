﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataEntites.Model.Trip
{
    public class TotalTripsModal
    {
        public List<TripModel> CompleteTrips;
        public List<TripModel> InProgressTrips;
        public List<TripModel> Schedule;
        public List<TripModel> Hold;
        public List<TripModel> Cancel;
        public List<TripModel> TopReviews;
        public TotalTripsModal()
        {
            CompleteTrips = new List<TripModel>();
            InProgressTrips = new List<TripModel>();
            Schedule = new List<TripModel>();
            Hold = new List<TripModel>();
            Cancel = new List<TripModel>();
            TopReviews = new List<TripModel>();
        }
    }
}
