﻿using System;

namespace DataEntites.Model.Trip.TripReviewModel
{
    public class OperatorsReviews
    {
        public int ReviewId { get; set; }
        public long TripId { get; set; }
        public string Comment { get; set; }
        public string Reviews { get; set; }
        public DateTime ReviewCreatedDateTime { get; set; }
        public int TripStar { get; set; }
        public bool ReviewStatus { get; set; }
        public string UserName { get; set; }
    }
}