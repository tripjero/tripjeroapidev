﻿using System;

namespace DataEntites.Model.Trip.TripReviewModel
{
    public class OperatorTripReviews
    {
        public int Id { get; set; }

        public string Reviews { get; set; }
        public bool Status { get; set; }
        public string Comment { get; set; }
        public long TId { get; set; }
        public long UId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UserName { get; set; }
        public string TripTitle { get; set; }
        public int? TripStars { get; set; }
        public bool IsAdminReviewd { get; set; }
    }
}