﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataEntites.Model.Trip.TripReviewModel
{
    public class ReviewsCount
    {
        public int TotalReviews { get; set; }
        public int AvgStars { get; set; }
    }
}
