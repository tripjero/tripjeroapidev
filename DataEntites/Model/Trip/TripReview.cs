﻿using System;

namespace DataEntites.Model.Trip
{
    public class TripReview
    {
        public int? Id { get; set; }
        public string Reviews { get; set; }
        public DateTime CreatedDate { get; set; }
        public long UId { get; set; }
        public long OperatorId { get; set; }
        public long TId { get; set; }
        public bool Status { get; set; }
        public bool isActive { get; set; }
        public string UserName { get; set; }
        public int? Stars { get; set; }
        public string Comment { get; set; }
    }
}