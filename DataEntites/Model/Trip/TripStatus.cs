﻿namespace DataEntites.Model.Trip
{
    public class TripStatus
    {
        public long TripId { get; set; }
        public int StatusId { get; set; }
    }
}