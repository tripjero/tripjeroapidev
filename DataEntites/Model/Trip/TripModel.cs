﻿using DataEntites.Model.TripBooking;
using DataEntites.Model.Utilities;
using System;
using System.Collections.Generic;

namespace DataEntites.Model.Trip
{
    public class TripModel
    {
        public List<Model.Trip.TripReviewModel.OperatorsReviews> TripReviews;
        public List<TripImages> TripImages;
        public List<OperatorBookings> Bookings;
        public Faclities Faclities;
        public List<IternaryPlan> IternaryPlan;
        public TripBasicInfo TripBasicInfo;
        public Trip.TripPrice.TripPrice tripPrices;
        public TripModel()
        {
            TripImages = new List<TripImages>();
            TripReviews = new List<Model.Trip.TripReviewModel.OperatorsReviews>();
            Faclities = new Faclities();
            IternaryPlan = new List<IternaryPlan>();
            TripBasicInfo = new TripBasicInfo();
            tripPrices = new TripPrice.TripPrice();
            
        }
        public bool? IsBoost { get; set; }
        public int Boost_Code { get; set; }
        public string TripStatus { get; set; }
        public int AvgStars { get; set; }
        public int TotalRevews { get; set; }
        public bool IsMyFavt { get; set; }
    }

    public class TripBasicInfo: BasePaginations
    {
        public long? Id { get; set; }
        public string Title { get; set; }
        public string Details { get; set; }
        public int FromVistId { get; set; }
        public int ToVistId { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string Duration { get; set; }
        public double Amount { get; set; }
        public long UId { get; set; }
        public string Services { get; set; }

   
        public DateTime CreatedDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public long ModifyById { get; set; }
        public long? ActiveBy { get; set; }
        public string FeaturesImage { get; set; }
        public string BaseStringFeaturesImages { get; set; }
        public string FromPlaceName { get; set; }
        public string ToPlaceName { get; set; }
        public string ModifybyName { get; set; }
        public string ActiveByName { get; set; }
        public string TripOperator { get; set; }
        public string OperatorLicNumer { get; set; }
        public bool? OperatorLicActive { get; set; }
        public string TripBuisnessName { get; set; }
        public string TripStatus { get; set; }
        public int TripStatusId { get; set; }
        public bool NO_MORE_BOOKING { get; set; }
        public long? PromoId { get; set; }
        public bool Is_Promo { get; set; }
        public double? DiscountPercentage { get; set; }
        public int? CategoriesId { get; set; }
        public string CategoriesName { get; set; }
        public int? TotalNumberOfViews { get; set; }
        public bool IsBoost { get; set; }
        public string PromoCode { get; set; }

    }
}