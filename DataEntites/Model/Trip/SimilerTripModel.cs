﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataEntites.Model.Trip
{
 public class SimilerTripModel
    {
        public SimilerTripModel()
        {
            SimilartripsList = new List<SimilarTripList>();
            TripReviews = new List<TripReviewModel.OperatorsReviews>();
        }
        public TripModel SingleTrip { get; set; }
        public  List<SimilarTripList> SimilartripsList;
        public List<Model.Trip.TripReviewModel.OperatorsReviews> TripReviews;
    }
}
public class SimilarTripList
{
    public long Id { get; set; }
    public string Title { get; set; }
    public string FeaturesImage { get; set; }

    public string FromPlaceName { get; set; }
    public string ToPlaceName { get; set; }
}
