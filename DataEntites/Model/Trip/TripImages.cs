﻿using System;

namespace DataEntites.Model.Trip
{
    public class TripImages
    {
        public int? Id { get; set; }
        public string ImageUrl { get; set; }
        public long TripId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public long ModifybyId { get; set; }
        public string ModifybyName { get; set; }
        public bool IsActive { get; set; }
    }
}