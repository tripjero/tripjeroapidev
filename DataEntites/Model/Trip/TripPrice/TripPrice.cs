﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataEntites.Model.Trip.TripPrice
{
    public class TripPrice
    {
        public double ActualAmount { get; set; }
        public double CurrentAmount { get; set; }
        public double DiscountPercentage { get; set; }
        public bool HavePromo { get; set; }
        public string PromoCode { get; set; }// Key
        public long trip_id { get; set; }
        public long user_id { get; set; }
    }
}
