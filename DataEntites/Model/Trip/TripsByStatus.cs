﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataEntites.Model.Trip
{
    public class TripsByStatus
    {
        public string Title { get; set; }
        public long Id { get; set; }
        public Nullable<bool> IsAcive { get; set; }
        public string StatusName { get; set; }
        public int PageSize { get; set; }
        public int TotalPage { get; set; }
        public int PageNo { get; set; }
    }
}
