﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataEntites.Model.Trip
{
   public class TripTempImagesList
    {

        public string ImageString { get; set; }
        public string ImagePath { get; set; }

    }
}
