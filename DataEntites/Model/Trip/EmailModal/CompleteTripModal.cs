﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataEntites.Model.Trip.EmailModal
{
    public class CompleteTripModal
    {
        public string Title { get; set; }
        public string Duration { get; set; }
        public double PerPersonCost { get; set; }
        public int NumberOfViews { get; set; }
        public int NumberOfBookings { get; set; }
        public int Ratings { get; set; }
        
    }
}
