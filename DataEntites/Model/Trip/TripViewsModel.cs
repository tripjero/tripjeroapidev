﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataEntites.Model.Trip
{
   public class TripViewsModel
    {

        public long TId { get; set; }
        public long UId { get; set; }

        public string TravName { get; set; }

        public DateTime Dtm { get; set; }

        public string TripTitle { get; set; }
        public string TripStatus { get; set; }
    }
}
