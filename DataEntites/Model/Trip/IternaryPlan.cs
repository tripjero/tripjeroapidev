﻿using System.Collections.Generic;

namespace DataEntites.Model.Trip
{
    public class IternaryPlan
    {
        public List<DayPlan> DayPlan;

        public IternaryPlan()
        {
            DayPlan = new List<DayPlan>();
        }

        public long Id { get; set; }
        public string DayName { get; set; }
        public long TripId { get; set; }
    }
}