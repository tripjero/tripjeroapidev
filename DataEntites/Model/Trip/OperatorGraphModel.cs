﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataEntites.Model.Trip
{
    public class OperatorGraphModel
    {
        public long Id { get; set; }
        public long UId { get; set; }
        public DateTime CreatedDate { get; set; }

        public int TotalBookings { get; set; }


    }

    public class MonthlyTripData {
        public int TotalBookings { get; set; }
        public int TotalTripPosted { get; set; }
    }


    public class MonthList {
        public int year { get; set; }
        public MonthlyTripData monthlyTripData { get; set; }

        public int month { get; set; }
    }
}
