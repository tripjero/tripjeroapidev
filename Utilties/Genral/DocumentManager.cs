﻿using System;
using System.IO;
using System.Web;

namespace Utilties.Genral
{
    public static class DocumentManager
    {
        public static string SaveFile(string baseString, string fullDirectory, string extentions)
        {
            String path = HttpContext.Current.Server.MapPath(fullDirectory); //Path
            string fileNameForSaving = Guid.NewGuid().ToString();
            //Check if directory exist
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path); //Create directory if it doesn't exist
            }
            string fileName = fileNameForSaving + extentions;

            //set the image path
            string imgPath = Path.Combine(path, fileName);

            byte[] imageBytes = Convert.FromBase64String(baseString);

            File.WriteAllBytes(imgPath, imageBytes);

            return fullDirectory + "/" + fileName;
        }

        public static bool DeleteFile(string fullDirectory)
        {
            if (File.Exists(fullDirectory))
            {
                try
                {
                    File.Delete(fullDirectory);
                    return true;
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return false;
        }
    }
}