﻿using System;

namespace Utilties.Genral
{
    public static class ObjectSerilization
    {
        public static object GetSerilizedWithoutObject(string Message, bool StatusValue)
        {
            return new { Status = StatusValue, Message = Message };
        }

        public static object GetSerilizedTripPostObject(string Message, bool StatusValue, long refId)
        {
            return new { Status = StatusValue, Message = Message, RefId = refId };
        }
        public static object GetSerilizedObject(string Message, bool StatusValue, object obj)
        {
            return new { Status = StatusValue, Message = Message, Data = obj };
        }
        public static object GetSerilizedObject(string Message, bool StatusValue, object obj,int pageSize=10,int pageNo=1,int TotalPages=0)
        {
            return new { Status = StatusValue, Message = Message, Data = obj,PageSize=pageSize,PageNo= pageNo, TotalPages= TotalPages};
        }

        public static DateTime TimeSpanToDateTime(string date)
        {
            var d = DateTimeOffset.FromUnixTimeMilliseconds(long.Parse(date));
            return Convert.ToDateTime(d.ToString());
        }

        public static string GetVistString(string vist, string city, string country)
        {
            return vist + " , " + city + " , " + country;
        }
    }
}