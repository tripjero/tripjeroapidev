﻿using DataEntites.Model.Trip;
using DataEntites.Model.Trip.EmailModal;
using System.IO;
using System.Web;

namespace Utilties.Genral
{
    public class EmailTemplates
    {
        public static string bank_one = "Easypaisa Account:";
        public static string acc_one = "0347 0000000";
        public static string acc_title_one = "JHON DOE";

        public static string bank_two = "Faysal Bank lmt:";
        public static string acc_two = "PAKFAY010010000545";
        public static string acc_title_two = "TRIP JERO";

        public static string WelcomeEmailString(string name,string username , string password)
        {
            string path = HttpContext.Current.Server.MapPath(Path.Combine("~/ReportsHTML/operator", "sign_up.html"));
            string htmlCompleteTripWithModal = ReadHTMLasString(path);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#name", name);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#username", username);
            if (password == null || password == "")
            {
                htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#password", "");
            }
            else
            {
                htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#password", password);

            }
            return htmlCompleteTripWithModal;
        }

        public static string resetpass(string name,string username,string pin)
        {
            string path = HttpContext.Current.Server.MapPath(Path.Combine("~/ReportsHTML/operator", "reset_password.html"));
            string htmlCompleteTripWithModal = ReadHTMLasString(path);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#name", name);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#username", username);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#pin", pin);
            return htmlCompleteTripWithModal;
        }

        public static string profilePkgSubscription(string name,string pkg,string amount,string limit)
        {
            string path = HttpContext.Current.Server.MapPath(Path.Combine("~/ReportsHTML/operator", "profilepkgsubscription.html"));
            string htmlCompleteTripWithModal = ReadHTMLasString(path);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#name", name);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#pkg", pkg);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#rs", amount);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#limit", limit);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#bank_one", bank_one);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#acc_one", acc_one);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#acc_title_one", acc_title_one);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#bank_two", bank_two);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#acc_two", acc_two);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#acc_title_two", acc_title_two);


            return htmlCompleteTripWithModal;
        }
        public static string profilePkgSubscriptionpayment(string name, string pkg, string amount, string limit)
        {
            string path = HttpContext.Current.Server.MapPath(Path.Combine("~/ReportsHTML/operator", "sub_pkg_paid_recipt.html"));
            string htmlCompleteTripWithModal = ReadHTMLasString(path);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#name", name);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#pkg", pkg);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#rs", amount);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#limit", limit);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#bank_one", bank_one);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#acc_one", acc_one);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#acc_title_one", acc_title_one);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#bank_two", bank_two);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#acc_two", acc_two);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#acc_title_two", acc_title_two);


            return htmlCompleteTripWithModal;
        }

        public static string boostpkgsubscription(string name, string pkg, string amount,string discount ,string p_discount,string total,string limit)
        {
            string path = HttpContext.Current.Server.MapPath(Path.Combine("~/ReportsHTML/operator", "boost_pkg_invoice.html"));
            string htmlCompleteTripWithModal = ReadHTMLasString(path);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#name", name);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#pkg", pkg);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#rs", amount);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#discount", discount);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#p_discount", p_discount);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#total", total);

            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#limit", limit);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#bank_one", bank_one);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#acc_one", acc_one);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#acc_title_one", acc_title_one);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#bank_two", bank_two);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#acc_two", acc_two);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#acc_title_two", acc_title_two);


            return htmlCompleteTripWithModal;
        }
        

        public static string boostpkgsubrecipt(string name, string pkg, string amount, string discount, string p_discount, string total, string limit)
        {
            string path = HttpContext.Current.Server.MapPath(Path.Combine("~/ReportsHTML/operator", "boost_pkg_recipt.html"));
            string htmlCompleteTripWithModal = ReadHTMLasString(path);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#name", name);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#pkg", pkg);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#rs", amount);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#discount", discount);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#p_discount", p_discount);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#total", total);

            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#limit", limit);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#bank_one", bank_one);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#acc_one", acc_one);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#acc_title_one", acc_title_one);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#bank_two", bank_two);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#acc_two", acc_two);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#acc_title_two", acc_title_two);


            return htmlCompleteTripWithModal;
        }
        public static string CompleteTripEmailString(CompleteTripModal completeTripModal)
        {
            string path = HttpContext.Current.Server.MapPath(Path.Combine("~/ReportsHTML/operator", "completed_trip.html"));
            string htmlCompleteTripWithModal = ReadHTMLasString(path);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#Title", completeTripModal.Title);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#Duration", completeTripModal.Duration.ToString());
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#PerPersonCost", completeTripModal.PerPersonCost.ToString());
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#NumberOfViews", completeTripModal.NumberOfViews.ToString());
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#NumberOfBookings", completeTripModal.NumberOfBookings.ToString());
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#Ratings", completeTripModal.Ratings.ToString());
            return htmlCompleteTripWithModal;
        }

        public static string TripPostEmail(TripModel trip, string username)
        {
            string path = HttpContext.Current.Server.MapPath(Path.Combine("~/ReportsHTML/operator", "trip_posted.html"));
            string htmlCompleteTripWithModal = ReadHTMLasString(path);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#Operator", username);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#trip", trip.TripBasicInfo.Title.ToString());
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#tripname", trip.TripBasicInfo.Title.ToString());
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#id", trip.TripBasicInfo.Id.ToString());
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#desc", trip.TripBasicInfo.Details.ToString());
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#from", trip.TripBasicInfo.FromPlaceName.ToString());
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#to", trip.TripBasicInfo.ToPlaceName.ToString());

            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#start", trip.TripBasicInfo.StartDateTime.ToShortDateString());
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#enddate", trip.TripBasicInfo.EndDateTime.ToShortDateString());
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#actualp", trip.tripPrices.ActualAmount.ToString());
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#discountp ", trip.tripPrices.DiscountPercentage.ToString());
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#currentp", trip.tripPrices.CurrentAmount.ToString());
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#boost", trip.IsBoost.ToString());
            string promo = "No";
            if (trip.tripPrices.HavePromo)
            {
                promo = "Yes";
            }
           
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#promo", promo);


            return htmlCompleteTripWithModal;
        }


        public static string BookingEmailToOpt(string username,string trip,string U5K4RI,string travName,string no,string start, string end, string actualp, string discountp,
            string currentp, string noOfPerson, string promo, string bookdate, string payment)
        {
            string path = HttpContext.Current.Server.MapPath(Path.Combine("~/ReportsHTML/operator", "booking_confirmation.html"));
            string htmlCompleteTripWithModal = ReadHTMLasString(path);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#Operator", username);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#trip ", trip);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#U5K4RI", U5K4RI);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#name", travName);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#no", no);

            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#start", start);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#end ", end);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#actualp", actualp);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#discountp ", discountp);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#boost", noOfPerson);

            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#currentp", currentp);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#promo", promo);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#bookdate", bookdate);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#payment", payment);




            return htmlCompleteTripWithModal;
        }



        public static string PaymentStatusToTrav(string username, string reff, string status)
        {
            string path = HttpContext.Current.Server.MapPath(Path.Combine("~/ReportsHTML/trav", "booking_payment_update.html"));
            string htmlCompleteTripWithModal = ReadHTMLasString(path);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#user", username);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#ref ", reff);
            htmlCompleteTripWithModal = htmlCompleteTripWithModal.Replace("#status", status);
      




            return htmlCompleteTripWithModal;
        }
        public static string ReadHTMLasString(string path)
        {

            string html = File.ReadAllText(path);
            return html;
        }
    }
}