﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilties.Genral
{
    public class DiscountCalculator
    {
        public static double Discount(int Percentage, double TotalAmount)
        {
            return (double)(TotalAmount * (100.0 - Percentage) / 100.0);
        }
    }
}
