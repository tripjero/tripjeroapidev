﻿using DataEntites.Model.PackageSub;

namespace Buisness.Core.PackageSub
{
    public interface IPackageSubsciption
    {
        object NewSubscription(PackageSubModel packageSubModel);

        object UpdatePackageSubscription(PackageSubModel packageSubModel);

        object RemovePacakgeSubscription(long id);

        object GetSubPackagesByUser(long id);
    }
}