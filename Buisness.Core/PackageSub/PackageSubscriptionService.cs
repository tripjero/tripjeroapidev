﻿using Buisness.Core.DbContext;
using DataEntites.Model.PackageSub;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Buisness.Core.PackageSub
{
    public class PackageSubscriptionService : IPackageSubsciption
    {
        private TripJeroEntities db = new TripJeroEntities();

        public object GetSubPackagesByUser(long id)
        {
            List<DataEntites.Model.PackageSub.PackageSubModel> operatorPackageList = new List<DataEntites.Model.PackageSub.PackageSubModel>();
            var dbPackages = db.SubscriptionPackages.Where(x => x.UId == id).ToList();
            if (dbPackages != null)
            {
                foreach (var item in dbPackages)
                {
                    DataEntites.Model.PackageSub.PackageSubModel opt = new DataEntites.Model.PackageSub.PackageSubModel();
                    opt.Id = item.Id;
                    opt.OId = item.Id;
                    opt.UserName = item.User.FullName;
                    opt.PackageName = item.OperatorPackage.Name;
                    operatorPackageList.Add(opt);
                }
            }
            return Utilties.Genral.ObjectSerilization.GetSerilizedObject("User Packages", true, operatorPackageList);
        }

        public object NewSubscription(PackageSubModel packageSubModel)
        {
            DbContext.SubscriptionPackage subscription = new DbContext.SubscriptionPackage();
            try
            {
                subscription.CreatedDate = DateTime.Now;
                subscription.IsActive = false;
                subscription.OId = packageSubModel.OId;
                subscription.UId = packageSubModel.UId;
                subscription.ModifyDate = DateTime.Now;
                db.SubscriptionPackages.Add(subscription);
                db.SaveChanges();
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("New Subscription has been Created", true);
            }
            catch (Exception ex)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false);
            }
        }

        public object RemovePacakgeSubscription(long id)
        {
            var subscrioption = db.SubscriptionPackages.Find(id);
            if (subscrioption != null)
            {
                db.SubscriptionPackages.Remove(subscrioption);
                db.SaveChanges();
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Package has been remvoed", true);
            }
            else
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Package not Found", false);
            }
        }

        public object UpdatePackageSubscription(PackageSubModel packageSubModel)
        {
            var subscrioption = db.SubscriptionPackages.Find(packageSubModel.Id);
            if (subscrioption != null)
            {
                subscrioption.IsActive = false;
                subscrioption.OId = packageSubModel.OId;
                subscrioption.UId = packageSubModel.UId;
                subscrioption.ModifyDate = DateTime.Now;
                db.SaveChanges();
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Package has been updated", true);
            }
            else
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Package not Found", false);
            }
        }
    }
}