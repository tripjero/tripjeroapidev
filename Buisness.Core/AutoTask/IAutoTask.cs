﻿namespace Buisness.Core.AutoTask
{
    public interface IAutoTask
    {
        object UpdateStatus();

        void SendWeeklyTripsNotifications();

        void SendTwoDayBeforNotfication();
    }
}