﻿using Buisness.Core.DbContext;
using Buisness.Core.Trip;
using DataEntites.GeneraicRepositroy;
using DataEntites.Model.AutoTask;
using DataEntites.Model.Trip.EmailModal;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TripJero.Notification.Services;

namespace Buisness.Core.AutoTask
{
    public class AutoTaskService : IAutoTask
    {
        private TripJeroEntities db = new TripJeroEntities();
        private TripService TripService = new TripService();

        public object UpdateStatus()
        {
            var tripList = db.TripCurrentStatus.Where(x => x.IsAcive == true && x.TripStatusId != 5 && x.TripStatusId != 4 && x.TripStatusId != 1).ToList();
            if (tripList.Any())
            {
                foreach (var item in tripList)
                {
                    if (item.Trip.IsActive)
                    {
                        if (item.Trip.StartDateTime.Date == DateTime.Now.Date )
                        {
                            DataEntites.Model.Trip.TripStatus tripStatus = new DataEntites.Model.Trip.TripStatus();
                            tripStatus.StatusId = 2;
                            tripStatus.TripId = item.TripId;
                            TripService.UpdateTripStatus(item.Trip.UId, tripStatus, "Auto");
                        }
                        else if (item.Trip.EndDateTime.Date < DateTime.Now.Date)
                        {
                            DataEntites.Model.Trip.TripStatus tripStatus = new DataEntites.Model.Trip.TripStatus();
                            tripStatus.StatusId = 1;
                            tripStatus.TripId = item.TripId;
                            TripService.UpdateTripStatus(item.Trip.UId, tripStatus, "Auto");
                            var tripId = new SqlParameter("TripId", SqlDbType.Int) { Value = tripStatus.TripId };
                            var tripcurrentStatus = SpRepository<CompleteTripModal>.GetSingleObjectWithStoreProcedure(@"exec Get_Complete_Trip_Report @TripId", tripId);
                            if (tripcurrentStatus != null)
                            {
                                string EmailString = Utilties.Genral.EmailTemplates.CompleteTripEmailString(tripcurrentStatus);
                                EmailSender.SendEmail("ghulammehdi178@gmail.com", "Complete trip Details",EmailString);
                            }


                        }
                    }
                }
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Updated", true);
            }
            return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Trip Updated", true);
        }

        public void SendWeeklyTripsNotifications()
        {
            var totalTripCount = SpRepository<WeeklyTrips>.GetListWithStoreProcedure(@"exec Get_Weekly_Trips_Count");
            if (totalTripCount.Any())
            {
                var users = db.Users.Where(x => x.RoleId == 4 && x.FirebaseToken != null).ToList();
                if (users.Any())
                {
                    foreach (var item in users)
                    {
                        if (item.FirebaseToken != null && !string.IsNullOrEmpty(item.FirebaseToken))
                        {
                            try
                            {
                                NotificationSender.SendNotification(item.FirebaseToken, "There is " + totalTripCount.Count + " upcoming trips waiting you.", "Upcoming Trips");
                                var t = Task.Run(() => NotificationLog(item.Id, "There is " + totalTripCount.Count + " upcoming trips waiting you.", "Upcoming Trips"));
                                t.Wait();
                            }
                            catch (Exception)
                            {

                                throw;
                            }
                           
                        }
                    }
                }
            }
        }

        public void SendTwoDayBeforNotfication()
        {
            var totalTripCount = SpRepository<TwoDayBeforToStartTrips>.GetListWithStoreProcedure(@"exec Get_Two_Day_Befor_Trips_Count");
            if (totalTripCount.Any())
            {
                foreach (var item in totalTripCount)
                {
                    if (item.FirebaseToken != null && !string.IsNullOrEmpty(item.FirebaseToken))
                    {
                        NotificationSender.SendNotification(item.FirebaseToken, "Two days left to start trip " + item.Title, "Be ready to go");
                        var t = Task.Run(() => NotificationLog(item.UserId, "Two days left to start trip " + item.Title, "Be ready to go"));
                        t.Wait();
                    }
                }
            }
        }
        public async Task NotificationLog(long Uid,string Title,string Body)
        {
            NotificationHistory notificationHistory = new NotificationHistory();
            notificationHistory.UId = Uid;
            notificationHistory.Title = Title;
            notificationHistory.Body = Body;
            notificationHistory.CreatedDate = DateTime.Now;
            db.NotificationHistories.Add(notificationHistory);
            await db.SaveChangesAsync();
        }
    }
}