﻿using Buisness.Core.DbContext;
using DataEntites.GeneraicRepositroy;
using DataEntites.Model.BoostPromo;
using DataEntites.Model.BoostTrip;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TripJero.Notification.Services;
using Utilties.Genral;

namespace Buisness.Core.BoostTrip
{
    public class BoostTripService: IBoostTrip
    {
        TripJeroEntities db = new TripJeroEntities();

        public object BoostPackageSub(long packageId, long userId)
        {
            throw new NotImplementedException();
        }

        public object GetAllBoostPackages(string callFrom = "")
        {
            List<BoostTripModel> boostTrips = new List<BoostTripModel>(); 
            
            var boostTrip = callFrom =="web" ? db.BoostPackages.Where(x=> x.IsDeleted == false).ToList() : db.BoostPackages.Where(x => x.IsActive == true && x.IsDeleted == false).ToList();
            if (boostTrip.Any())
            {
                foreach (var item in boostTrip)
                {
                    var boost = new BoostTripModel();
                    boost.Title = item.Title;
                    boost.Price = item.Price;
                    boost.NoTrips = item.NoTrips;
                    boost.Is_Promo = item.Is_Promo.HasValue ? item.Is_Promo : false;
                    if (boost.Is_Promo.HasValue && (bool)boost.Is_Promo)
                    {
                        boost.PromoCode = db.BoostPackagesPromoes.Find((long)item.PromoId).PromoCode;
                    }
                    boost.Discount = item.Discount;
                    boost.PromoId = item.PromoId !=null ? item.PromoId: 0;
                    boost.BoostCode = "BOOST-" + item.Id;
                    boost.Id = item.Id;
                    boost.IsActive = item.IsActive;
                    boostTrips.Add(boost);
                }
                return ObjectSerilization.GetSerilizedObject("Packages Found", true, boostTrips);
            }
            else
            {
                return ObjectSerilization.GetSerilizedWithoutObject("No Packages Found", false);
            }
           
        }

        public object VarifyBoostPin(long pinCode, long userId)
        {
           
               
                var boostSubscription = db.BoostPackageSubscriptions.FirstOrDefault(x => x.UId == userId && x.Id == pinCode);
                if (boostSubscription!=null)
                {
                    if (boostSubscription.RemningBoostTrips == 0 || boostSubscription.RemningBoostTrips < 0)
                    {
                        return ObjectSerilization.GetSerilizedWithoutObject("You cannot boost anymore by using given pin.Limit Reached",false);
                    }else
                    {
                        return ObjectSerilization.GetSerilizedWithoutObject("Your pin is valid",true);
                    }
                }
                else
                {
                    return ObjectSerilization.GetSerilizedWithoutObject("Your pin is not valid", false);
                }
            
        }

        public object BoostPackageSubscription(BoostPackageSubModel boostPackageSubscription)
        {
            double currentPackageAmout = 0;
            var alreadySubs = db.BoostPackageSubscriptions.FirstOrDefault(x => x.BoostPackageId == boostPackageSubscription.BoostPackageId && x.IS_Active == true && x.UId == boostPackageSubscription.UId);
            if (alreadySubs!=null)
            {
                return ObjectSerilization.GetSerilizedWithoutObject("You already have subscription of given package", false);
            }
            var boostPackage = db.BoostPackages.Find(boostPackageSubscription.BoostPackageId);
            if (boostPackage == null)
            {
                return ObjectSerilization.GetSerilizedWithoutObject("Invalid Boost Package Id", false);
            }
            currentPackageAmout = boostPackage.Price; 
            DbContext.BoostPackageSubscription packageSubscription = new BoostPackageSubscription();
            if (boostPackage!=null)
            {
                #region Discount 
                if (boostPackage.Discount != null && boostPackage.Discount > 0)
                {
                    currentPackageAmout = DiscountCalculator.Discount((int)boostPackage.Discount,currentPackageAmout);
                    packageSubscription.IS_Discount = true;
                }
                else
                {
                    packageSubscription.IS_Discount = false;
                }
                #endregion

                var p_dis = 0;

                #region PromoDiscount
                if (boostPackageSubscription.IS_Promo)
                {
                    if (boostPackage.Is_Promo.HasValue && (bool)boostPackage.Is_Promo)
                    {
                        if (boostPackage.BoostPackagesPromo!=null && boostPackage.BoostPackagesPromo.PromoCode.Trim() == boostPackageSubscription.PromoCode.Trim()
                            && boostPackage.IsActive && !boostPackage.IsDeleted)
                        {
                            p_dis = boostPackage.BoostPackagesPromo.Discount;
                            currentPackageAmout = DiscountCalculator.Discount(boostPackage.BoostPackagesPromo.Discount, currentPackageAmout);
                            packageSubscription.IS_Promo = true;
                        }else
                        {
                            return ObjectSerilization.GetSerilizedWithoutObject("Promo Code Not Active or Invalid",false);
                        }
                      
                    }else
                    {
                        packageSubscription.IS_Promo = false;
                    }
                }
                #endregion

                packageSubscription.UId = boostPackageSubscription.UId;
                packageSubscription.IS_Active = true;
                packageSubscription.IS_Payment = false;
                packageSubscription.ModifyDate = DateTime.Now;
                packageSubscription.BoostPackageId = boostPackageSubscription.BoostPackageId;
                packageSubscription.CreatedDate = DateTime.Now;
                packageSubscription.CurrentAmmount = currentPackageAmout;
                packageSubscription.RemningBoostTrips = boostPackage.NoTrips;
                packageSubscription.TotalAmmount = boostPackage.Price;
                packageSubscription.IS_Delted  = false;
                db.BoostPackageSubscriptions.Add(packageSubscription);
                db.SaveChanges();

                SubscirptionResponse subscirptionResponse = new SubscirptionResponse();
                subscirptionResponse.Id = packageSubscription.Id;
                subscirptionResponse.Is_payment = false;

                var user = db.Users.Find(boostPackageSubscription.UId);
                string EmailString = Utilties.Genral.EmailTemplates.boostpkgsubscription(user.FullName,packageSubscription.BoostPackage.Title,packageSubscription.BoostPackage.Price.ToString(), packageSubscription.BoostPackage.Discount.ToString(), p_dis.ToString(), packageSubscription.CurrentAmmount.ToString(),packageSubscription.RemningBoostTrips.ToString());
                EmailSender.SendEmail(user.Email, "Boost Package subscription", EmailString);

                return ObjectSerilization.GetSerilizedObject("Boost Package Subscribed! A detail invoice with payment option is sent to your email.", true, subscirptionResponse);
            }
            return ObjectSerilization.GetSerilizedWithoutObject("Values requried", false);
        }

        public object GetBoostPinDetails(string pinCode, long BoostPackageID)
        {
            var boostId = new SqlParameter("BoostId", SqlDbType.BigInt) { Value = BoostPackageID };
            var _promoCode = new SqlParameter("PromoCode", SqlDbType.VarChar) { Value = pinCode };
            var boostPcakgeWithPromo = SpRepository<BoostPromoModal>.GetSingleObjectWithStoreProcedure(@"exec GetBoostPromo @BoostId,@PromoCode", boostId, _promoCode);
            if (boostPcakgeWithPromo!=null)
            {
                boostPcakgeWithPromo.CurrentPrice = Utilties.Genral.DiscountCalculator.Discount(boostPcakgeWithPromo.Discount, boostPcakgeWithPromo.Price);
                return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Boost Promo Found", true, boostPcakgeWithPromo);
            }
            else
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Your boost promo is invalid", false);
            }
        }

        public object BoostPakageVarificationDetails(long boostId, long UId)
        {
            #region Boost Validation 
            
                if (boostId < 0)
                {
                    return ObjectSerilization.GetSerilizedWithoutObject("Boost Code Required", false);
                }
               
                var boostAvaible = db.BoostPackageSubscriptions.FirstOrDefault(x => x.Id == boostId && x.UId == UId && x.IS_Payment==true && x.IS_Active!=false && x.IS_Delted!=true);
                if (boostAvaible == null)
                {
                    return ObjectSerilization.GetSerilizedWithoutObject("Boost Code is not vaild", false);
                }
                if (boostAvaible.RemningBoostTrips == 0 || boostAvaible.RemningBoostTrips < 0)
                {
                    return ObjectSerilization.GetSerilizedWithoutObject("You cannot use this boost code any more.", false);
                }

             return ObjectSerilization.GetSerilizedWithoutObject("Boost Package is valid", true);


            //return ObjectSerilization.GetSerilizedObject("Boost Package is valid", true, boostAvaible);
            #endregion
        }



        #region Admin
        public object AddNewBoostPromo(DbContext.BoostPackagesPromo boostPackagesPromo)
        {
            if (boostPackagesPromo.Id > 0)
            {
                var exsitngPromo = db.BoostPackagesPromoes.Find(boostPackagesPromo.Id);
                if (exsitngPromo !=null)
                {
                    exsitngPromo.PromoCode = boostPackagesPromo.PromoCode;
                    exsitngPromo.StartDate = boostPackagesPromo.StartDate;
                    exsitngPromo.EndDate = boostPackagesPromo.EndDate;
                    exsitngPromo.Discount = boostPackagesPromo.Discount;
                    exsitngPromo.ModifyDate = DateTime.Now;
                    exsitngPromo.ModifyBy = boostPackagesPromo.ModifyBy;
                    exsitngPromo.CreatedDate = DateTime.Now;
                    exsitngPromo.CreatedBy = boostPackagesPromo.CreatedBy;
                    db.SaveChanges();
                    return ObjectSerilization.GetSerilizedWithoutObject("Boost Promo has been updated", true);
                }
                return ObjectSerilization.GetSerilizedWithoutObject("Promo not exist, no any update commit", false);
            }
            else
            {
                boostPackagesPromo.ModifyDate = DateTime.Now;
                boostPackagesPromo.Is_Delete = false;
                boostPackagesPromo.IsActive = true;
                db.BoostPackagesPromoes.Add(boostPackagesPromo);
                db.SaveChanges();
                return ObjectSerilization.GetSerilizedWithoutObject("Boost Promo has been updated", true);
            }

            
        }

        public object UpdateStatus(bool status, long promoId, long modifierId)
        {
            var exsitngPromo = db.BoostPackagesPromoes.Find(promoId);
            if (exsitngPromo!=null)
            {
                exsitngPromo.ModifyDate = DateTime.Now;
                exsitngPromo.ModifyBy = modifierId;
                exsitngPromo.IsActive = status;
                db.SaveChanges();
                return ObjectSerilization.GetSerilizedWithoutObject("Promo status updated", true);
            }
            return ObjectSerilization.GetSerilizedWithoutObject("No Promo exist of given Id", false);
        }

        public object DeletePromo(long Id, long modifierId)
        {
            var exsitngPromo = db.BoostPackagesPromoes.Find(Id);
            if (exsitngPromo != null)
            {
                exsitngPromo.ModifyDate = DateTime.Now;
                exsitngPromo.ModifyBy = modifierId;
                exsitngPromo.Is_Delete = true;
                db.SaveChanges();
                return ObjectSerilization.GetSerilizedWithoutObject("Promo has been deleted", true);
            }
            return ObjectSerilization.GetSerilizedWithoutObject("No Promo exist of given Id", false);
        }

        public object GetAllPromo()
        {
            List<BoostPromo> boostPromosList = new List<BoostPromo>();
            var allBoostPromo = db.BoostPackagesPromoes.Where(x=>x.Is_Delete!=true).OrderByDescending(x => x.CreatedDate);
            if (allBoostPromo.Any())
            {
                foreach (var item in allBoostPromo)
                {
                    BoostPromo boostPromo = new BoostPromo();
                    boostPromo.Id = item.Id;
                    boostPromo.IsActive = item.IsActive;
                    boostPromo.PromoCode = item.PromoCode;
                    boostPromo.StartDate = item.StartDate;
                    boostPromo.EndDate = item.EndDate;
                    boostPromo.Discount = item.Discount;
                    boostPromosList.Add(boostPromo);
                }
                
            }
            return ObjectSerilization.GetSerilizedObject("Boost Promo has been updated", true, boostPromosList);
        }

        public object AddUpdateNewBoostPackage(BoostTripModel boost)
        {
            if (boost!=null)
            {
                if (boost.Id > 0)
                {
                    var exsitingPackage = db.BoostPackages.Find(boost.Id);
                    if (exsitingPackage!=null)
                    {
                        exsitingPackage.IsActive = boost.IsActive;
                        exsitingPackage.IsDeleted = boost.IsDeleted;
                        exsitingPackage.Is_Promo = boost.PromoId > 0 ? true : false;
                        exsitingPackage.PromoId = boost.PromoId > 0 ? boost.PromoId :null;
                        exsitingPackage.Title = boost.Title;
                        exsitingPackage.ModifyBy = boost.ModifyBy;
                        exsitingPackage.ModifyDate = DateTime.Now;
                        exsitingPackage.NoTrips = boost.NoTrips;
                        exsitingPackage.Price = boost.Price;
                        exsitingPackage.Discount = boost.Discount;
                        db.SaveChanges();
                        return ObjectSerilization.GetSerilizedWithoutObject("Boost Package has been updated", true);
                    }
                } 
                else
                {
                    DbContext.BoostPackage boostPackage = new BoostPackage();
                    boostPackage.Title = boost.Title;
                    boostPackage.Discount = boost.Discount;
                    boostPackage.NoTrips = boost.NoTrips;
                    boostPackage.Price = boost.Price;
                    boostPackage.Is_Promo = boost.PromoId > 0 ? true : false;
                    
                    if ((bool)boostPackage.Is_Promo)
                    {
                        boostPackage.PromoId = boost.PromoId;
                    }
                    boostPackage.IsDeleted = false;
                    boostPackage.IsActive = true;
                    boostPackage.ModifyBy = boost.ModifyBy;
                    boostPackage.ModifyDate = DateTime.Now;
                    boostPackage.CreatedDate = DateTime.Now;
                    db.BoostPackages.Add(boostPackage);
                    db.SaveChanges();
                    return ObjectSerilization.GetSerilizedWithoutObject("Boost Package has been updated", true);
                }
                
            }
            return ObjectSerilization.GetSerilizedWithoutObject("Boost Package Value required", false);
        }

        public object DeleteBoostPackage(long Id, long userId)
        {
            var boostPackage = db.BoostPackages.Find(Id);
            boostPackage.IsDeleted = true;
            boostPackage.ModifyBy = userId;
            boostPackage.ModifyDate = DateTime.Now;
            db.SaveChanges();
            return ObjectSerilization.GetSerilizedWithoutObject("Boost Package Deleted", true);
        }

        public object UpdateBoostPackageStatus(bool status, long Id, long userId)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}


public class SubscirptionResponse
{
    public long Id { get; set; }
    public bool Is_payment { get; set; }
}
