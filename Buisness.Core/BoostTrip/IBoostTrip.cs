﻿using DataEntites.Model.BoostTrip;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buisness.Core.BoostTrip
{
    public interface IBoostTrip
    {
        object BoostPackageSub(long packageId, long userId);
        object GetAllBoostPackages(string callFrom="");
        object VarifyBoostPin(long pinCode ,long userId);
        object GetBoostPinDetails(string pinCode, long BoostPackageID);
        object BoostPackageSubscription(BoostPackageSubModel boostPackageSubscription);
        object BoostPakageVarificationDetails(long boostId, long UId);


        #region Admin Section
        object AddNewBoostPromo(DbContext.BoostPackagesPromo boostPackagesPromo);
        object UpdateStatus(bool status, long promoId, long modifierId);
        object DeletePromo(long Id, long modifierId);
        object GetAllPromo();

        //Boost Packages
        object AddUpdateNewBoostPackage(BoostTripModel boost);
        object DeleteBoostPackage(long Id,long userId);
        object UpdateBoostPackageStatus(bool status,long Id, long userId);
        #endregion


    }
}
