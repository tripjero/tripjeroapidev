﻿using Buisness.Core.Account;
using Buisness.Core.Address;
using Buisness.Core.AutoTask;
using Buisness.Core.BoostTrip;
using Buisness.Core.OperatorPackages;
using Buisness.Core.PackageSub;
using Buisness.Core.Promo;
using Buisness.Core.TripComments;
using Resolver;
using System.ComponentModel.Composition;

namespace Buisness.Core
{
    [Export(typeof(IComponent))]
    public class DependencyResolver : IComponent
    {
        public void SetUp(IRegisterComponent registerComponent)
        {
            registerComponent.RegisterType<User.IUser, User.UserSerivce>();
            registerComponent.RegisterType<Trip.ITrip, Trip.TripService>();
            registerComponent.RegisterType<UtitliesSection.IUtilities, UtitliesSection.UtilitiesService>();
            registerComponent.RegisterType<TripBooking.ITripBooking, TripBooking.TripBookingService>();
            registerComponent.RegisterType<Operator.IOperator, Operator.OperatorService>();
            registerComponent.RegisterType<ITripComments, TripCommentsService>();
            registerComponent.RegisterType<IAutoTask, AutoTaskService>();
            registerComponent.RegisterType<IAccount, AccountService>();
            registerComponent.RegisterType<IAddress, AddressService>();
            registerComponent.RegisterType<IPromo, PromoService>();
            registerComponent.RegisterType<IBoostTrip, BoostTripService>();
            registerComponent.RegisterType<IOperatorPackage, OperatorPackageService>();
            registerComponent.RegisterType<IPackageSubsciption, PackageSubscriptionService>();
        }
    }
}