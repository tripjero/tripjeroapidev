﻿using Buisness.Core.DbContext;
using DataEntites.GeneraicRepositroy;
using DataEntites.Model.Comments;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Utilties.Genral;

namespace Buisness.Core.TripComments
{
    public class TripCommentsService : ITripComments
    {
        private TripJeroEntities db = new TripJeroEntities();

        public object AddComment(long Uid, TravlerComments travlerComments)
        {
            try
            {
                DbContext.TripComment tripComment = new DbContext.TripComment();
                tripComment.IsDeleted = false;
                tripComment.ModifyDate = DateTime.Now;
                tripComment.CreatedDate = DateTime.Now;
                tripComment.Comments = travlerComments.Comment;
                tripComment.TId = travlerComments.TId;
                tripComment.UId = Uid;
                db.TripComments.Add(tripComment);
                db.SaveChanges();
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Comments save successufuly posted", true);
            }
            catch (Exception ex)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false);
            }
        }

        public object AddReply(long UId, TripReplyComments tripReplyComments)
        {
            try
            {
                DbContext.TripCommentsReply commentsReply = new TripCommentsReply();
                if (db.TripCommentsReplies.FirstOrDefault(x => x.CommentId == tripReplyComments.CommentId) != null)
                    return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("You already replyed to this comment", false);
                commentsReply.CommentId = tripReplyComments.CommentId;
                commentsReply.Comments = tripReplyComments.ReplyComments;
                commentsReply.CreatedDate = DateTime.Now;
                commentsReply.ModifyDate = DateTime.Now;
                commentsReply.UId = UId;
                db.TripCommentsReplies.Add(commentsReply);
                db.SaveChanges();
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Comments save successufuly posted", true);
            }
            catch (Exception ex)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false);
            }
        }

        public object GetTripCommentsByTripId(long Id,int pageNo,int pageSize)
        {
            var _id = new SqlParameter("TripId", SqlDbType.BigInt) { Value = Id }; // by tips Id
            var _pageNo = new SqlParameter("PageNumber", SqlDbType.Int) { Value = pageNo };
            var _pageSize = new SqlParameter("RowOfPage", SqlDbType.Int) { Value = pageSize };
            var tripComments = SpRepository<DataEntites.Model.Comments.TripComments>.GetListWithStoreProcedure(@"exec Get_Comments_By_TripId @TripId,@PageNumber,@RowOfPage", _id,_pageNo,_pageSize);
            if (tripComments != null && tripComments.Any())
            {
                return ObjectSerilization.GetSerilizedObject("Comments Found", true, tripComments, tripComments[0].PageSize,tripComments[0].PageNo,tripComments[0].TotalPage);
            }
            else
            {
                return ObjectSerilization.GetSerilizedWithoutObject("No Comments Found", false);
            }
        }

        public object GetTripCommentsByTripIdAndOpId(long OperatorId, long TripId)
        {
            var OId = new SqlParameter("OperatorId", SqlDbType.BigInt) { Value = OperatorId };
            var TId = new SqlParameter("TripId", SqlDbType.BigInt) { Value = TripId };
            var tripCommentsList = SpRepository<DataEntites.Model.Comments.TripComments>.GetListWithStoreProcedure(@"exec Get_Comments_By_TripId_And_Operator_Id @TripId,@OperatorId", TId, OId);
            if (tripCommentsList.Count > 0)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Comments Founds", true, tripCommentsList);
            }
            else
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Comments Not Founds", false);
            }
        }

        public object UpdateCommentIsSpam(DataEntites.Model.Comments.TripComments tripComments)
        {
            var commentStatusUpdate = db.TripComments.Find(tripComments.CommentId);
            if (commentStatusUpdate != null)
            {
                commentStatusUpdate.IsSpam = tripComments.IsSpam;
                commentStatusUpdate.ReviewComments = tripComments.ReviewComments;
                commentStatusUpdate.ModifyDate = DateTime.Now;
                db.SaveChanges();
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Comment Status Updated", true);
            }
            return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Comment not found", false);
        }

        public object UpdateTripComments(DataEntites.Model.Comments.TripComments tripComment)
        {
            var tripCommentUpdate = db.TripComments.Find(tripComment.CommentId);
            if (tripComment != null)
            {
                tripCommentUpdate.Comments = tripComment.TravlerComment;
                tripCommentUpdate.ModifyDate = DateTime.Now;
                tripCommentUpdate.IsAdminReviwed = true;
                db.SaveChanges();
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Comments Modified", true);
            }
            return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Comments Found", false);
        }
    }
}