﻿using DataEntites.Model.Comments;

namespace Buisness.Core.TripComments
{
    public interface ITripComments
    {
        object GetTripCommentsByTripIdAndOpId(long OperatorId, long TripId);

        object AddComment(long Uid, TravlerComments travlerComments);

        object AddReply(long UId, TripReplyComments tripReplyComments);

        object GetTripCommentsByTripId(long Id, int pageNo, int pageSize);

        object UpdateTripComments(DataEntites.Model.Comments.TripComments tripComments);

        object UpdateCommentIsSpam(DataEntites.Model.Comments.TripComments tripComments);
    }
}