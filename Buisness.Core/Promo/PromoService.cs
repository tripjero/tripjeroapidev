﻿using Buisness.Core.DbContext;
using DataEntites.GeneraicRepositroy;
using DataEntites.Model.Promo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilties.Genral;

namespace Buisness.Core.Promo
{
    public class PromoService: IPromo
    {
        TripJeroEntities db = new TripJeroEntities();

        public object AddNewPromo(PromoTbl promo)
        {
            if (promo!=null)
            {
                if (promo.PromoCode.Length < 4)
                {
                    return ObjectSerilization.GetSerilizedWithoutObject("Promo should be more than 4 chractors  ", false);
                }
                if (promo.PromoCode.Length > 8 )
                {
                    return ObjectSerilization.GetSerilizedWithoutObject("Promo should not be more than 8 chractors  ", false);
                }
                if (promo.StartDate > promo.EndDate)
                {
                    return ObjectSerilization.GetSerilizedWithoutObject("Start date should be greater then End date", false);
                }
                promo.CreatedDate = DateTime.Now;
                promo.ModifyDate = DateTime.Now;
                promo.IsActive = true;
                promo.IsDeleted = false;
                db.PromoTbls.Add(promo);
                db.SaveChanges();
                promo.Promo_Key = promo.PromoCode.Substring(0,3).ToString()+promo.Id;
                db.SaveChanges();
                return ObjectSerilization.GetSerilizedWithoutObject("New Promo Code is created",true);
            }
            return ObjectSerilization.GetSerilizedWithoutObject("Values required", false);
        }

        public object ChnageStatus(long Id, bool Status, long UId)
        {
            var pormoExist = db.PromoTbls.Find(Id);
            if (pormoExist!=null)
            {
                pormoExist.IsActive = Status;
                pormoExist.ModifyId = UId;
                db.SaveChanges();
                return ObjectSerilization.GetSerilizedWithoutObject("Promo status updated", true);
            }
            return ObjectSerilization.GetSerilizedWithoutObject("Promo not exist", false);
        }

        public object DeletePromo(long Id)
        {
            throw new NotImplementedException();
        }

        public object GetPromoByUserId(long Id,int pageNo,int pageSize)
        {
            var userId = new SqlParameter("UserId", SqlDbType.BigInt) { Value = Id };
            var _pageNo = new SqlParameter("PageNumber", SqlDbType.Int) { Value = pageNo };
            var _pageSize = new SqlParameter("RowOfPage", SqlDbType.Int) { Value = pageSize };
            var prmoList = SpRepository<PromoModel>.GetListWithStoreProcedure(@"exec Get_PromoCode_By_User @UserId,@PageNumber,@RowOfPage", userId, _pageNo,_pageSize);
            if (prmoList.Count > 0)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Promo Code Founds", true, prmoList, prmoList[0].PageSize, prmoList[0].PageNo, prmoList[0].TotalPage);
            }
            else
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Promo Code Not Founds", false);
            }
        }

        public object VarifyPromo(string PromoKey, long UId)
        {
            var userId = new SqlParameter("UserId", SqlDbType.BigInt) { Value = UId };
            var pKey = new SqlParameter("PromoKey", SqlDbType.VarChar) { Value = PromoKey };
            var prmoList = SpRepository<PromoModel>.GetListWithStoreProcedure(@"exec Get_PromoCode_By_User_And_Promo_Key @UserId,@PromoKey", userId, pKey);
            if (prmoList.Count > 0)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Promo Code Founds", true, prmoList);
            }
            else
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Promo code not valid or expire", false);
            }
        }

        public object VerifyPromoTravller(string PromoKy,long TId=0)
        {
            var pKey = new SqlParameter("PromoCode", SqlDbType.VarChar) { Value = PromoKy };
            var tripId = new SqlParameter("TripId", SqlDbType.VarChar) { Value = TId };
            var prmoList = SpRepository<PromoModel>.GetSingleObjectWithStoreProcedure(@"exec Get_Trip_Promo_Verification_On_Booking @PromoCode,@TripId", pKey, tripId);
            if (prmoList!=null)
            {
                if (DateTime.Now >=  prmoList.StartDate)
                {
                    return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Promo Code Founds", true, prmoList);
                }
                else
                {
                    return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Promo will be applicable on "+ prmoList.StartDate.ToString("MMMM dd, yyyy"), false);
                }
            }
            else
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Promo code not valid or expire", false);
            }
        }
    }
}
