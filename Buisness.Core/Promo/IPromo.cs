﻿using Buisness.Core.DbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buisness.Core.Promo
{
    public interface IPromo
    {
        object AddNewPromo(PromoTbl promo);
        object DeletePromo(long Id);
        object ChnageStatus(long Id, bool Status,long UId);
        object GetPromoByUserId(long Id, int pageNo, int pageSize);
        object VarifyPromo(string PromoKey, long UId);
        object VerifyPromoTravller(string PromoKy, long TId = 0);
    }
}
