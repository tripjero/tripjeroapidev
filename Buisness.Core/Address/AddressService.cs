﻿using DataEntites.Model.Address;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Buisness.Core.Address
{
    public class AddressService : IAddress
    {
        private DbContext.TripJeroEntities db = new DbContext.TripJeroEntities();

        public List<City> GetCity()
        {
            List<City> _citiesList = new List<City>();
            var citylist = db.Cities.ToList();
            if (citylist != null)
            {
                foreach (var item in citylist)
                {
                    City city = new City();
                    city.Id = item.Id;
                    city.Name = item.Name;
                    city.ProvinceId = item.ProvincesId;
                    _citiesList.Add(city);
                }
            }

            return _citiesList;
        }

        public List<City> GetCityByProvinceId(int id)
        {
            List<City> _citiesList = new List<City>();
            var citylist = db.Cities.Where(x => x.ProvincesId == id).ToList();
            if (citylist != null)
            {
                foreach (var item in citylist)
                {
                    City city = new City();
                    city.Id = item.Id;
                    city.Name = item.Name;
                    city.ProvinceId = item.ProvincesId;
                    _citiesList.Add(city);
                }
            }
            return _citiesList;
        }

        public List<Country> GetCountries()
        {
            List<Country> _countryList = new List<Country>();
            var citylist = db.Countries.ToList();
            if (citylist != null)
            {
                foreach (var item in _countryList)
                {
                    Country country = new Country();
                    country.Id = item.Id;
                    country.Name = item.Name;
                    _countryList.Add(country);
                }
            }
            return _countryList;
        }

        public List<Provinces> GetProvinces()
        {
            throw new NotImplementedException();
        }

        public List<Provinces> GetProvincesbyCountryId(int id)
        {
            List<Provinces> _provincesList = new List<Provinces>();
            var provincesList = db.Provinces.Where(x => x.CountryId == id).ToList();
            foreach (var item in provincesList)
            {
                Provinces provinces = new Provinces();
                provinces.Id = item.Id;
                provinces.Name = item.Name;
                provinces.CountryId = item.CountryId;
                _provincesList.Add(provinces);
            }
            return _provincesList;
        }

        public DataEntites.Model.Address.Address GetAddressbyUserId(double id)
        {
            DataEntites.Model.Address.Address address = new DataEntites.Model.Address.Address();
            var addressdb = db.Addresses.Find(id);
            if (addressdb != null)
            {
                address.Id = id;
                address.PermentAddress = addressdb.PermentAddress;
                address.StreetAddress = addressdb.ResidentialAddress;
            }
            return address;
        }

        
    }
}