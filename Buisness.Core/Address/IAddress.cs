﻿using DataEntites.Model.Address;
using System.Collections.Generic;

namespace Buisness.Core.Address
{
    internal interface IAddress
    {
        List<City> GetCity();

        List<City> GetCityByProvinceId(int id);

        List<Provinces> GetProvinces();

        List<Provinces> GetProvincesbyCountryId(int id);

        List<Country> GetCountries();


    }
}