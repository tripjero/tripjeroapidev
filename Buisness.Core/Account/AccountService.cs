﻿using Buisness.Core.DbContext;
using DataEntites.Model.Account;
using System;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using TripJero.Notification.Services;
using Utilties.Genral;

namespace Buisness.Core.Account
{
    public class AccountService : IAccount
    {
        private TripJeroEntities db = new TripJeroEntities();
        private readonly string fbfileds = "name,birthday,email";
        private static HttpClient client = new HttpClient();

        //public Role ExternalResgistraion(ExternalLoginModel exlink,int role_id, out long UId)
        //{
        //    Role role = new Role();
        //    var userExist = db.Users.FirstOrDefault(x => x.Email == exlink.Email);
        //    if (userExist != null)
        //    {
        //        var externalUser = db.ExternalLogins.FirstOrDefault(x => x.UId == userExist.Id);
        //        if (externalUser != null)
        //        {
        //            externalUser.ModifyDate = DateTime.Now;
        //            externalUser.Provider = exlink.Issuser;
        //            externalUser.Token = exlink.Token;
        //            db.SaveChanges();
        //            role.Id = userExist.RoleId;
        //            role.Name = userExist.Role.Name;
        //            UId = userExist.Id;
        //            return role;
        //        }
        //        else
        //        {
        //            var externalUserToInsert = new DbContext.ExternalLogin();
        //            externalUserToInsert.CreatedDate = DateTime.Now;
        //            externalUserToInsert.ModifyDate = DateTime.Now;
        //            externalUserToInsert.Token = exlink.Token;
        //            externalUserToInsert.ProviderId = exlink.ProviderId;
        //            externalUserToInsert.Provider = exlink.Issuser;
        //            externalUserToInsert.UId = userExist.Id;
        //            db.ExternalLogins.Add(externalUserToInsert);
        //            db.SaveChanges();
        //            role.Id = userExist.RoleId;
        //            role.Name = userExist.Role.Name;
        //            UId = userExist.Id;
        //            return role;
        //        }
        //    }
        //    using (var transaction = db.Database.BeginTransaction())
        //    {
        //        try
        //        {
        //            var _user = new DbContext.User();
        //            _user.Email = exlink.Email;
        //            _user.FullName = exlink.Name;
        //            _user.ProfileImagePath = exlink.Picture;
        //            _user.RoleId = role_id;
        //            _user.IsUserTypeVarified = true;
        //            _user.Gender = "null";
        //            _user.IsActive = true;
        //            _user.IsDeleted = false;
        //            _user.CreatedDate = DateTime.Now;
        //            _user.ModifyDate = DateTime.Now;
        //            db.Users.Add(_user);
        //            db.SaveChanges();
        //            var externalUserToInsert = new DbContext.ExternalLogin();
        //            externalUserToInsert.CreatedDate = DateTime.Now;
        //            externalUserToInsert.ModifyDate = DateTime.Now;
        //            externalUserToInsert.Token = exlink.Token;
        //            externalUserToInsert.ProviderId = exlink.ProviderId;
        //            externalUserToInsert.Provider = exlink.Issuser;
        //            externalUserToInsert.UId = _user.Id;
        //            db.ExternalLogins.Add(externalUserToInsert);
        //            db.SaveChanges();
        //            transaction.Commit();
        //            role.Id = _user.RoleId;
        //            role.Name = _user.Role.Name;
        //            UId = _user.Id;
                   

        //            string EmailString = Utilties.Genral.EmailTemplates.WelcomeEmailString(_user.FullName, _user.Email);
        //            EmailSender.SendEmail(_user.Email, "Welcome to TripJero", EmailString);
        //            return role;
        //        }
        //        catch (Exception ex)
        //        {
        //            transaction.Rollback();
        //            UId = 0;
        //            return null;
        //        }
        //    }
        //}




        //Role Base Socila Media Login/Signup
        public Role RoleBaseExternalResgistraion(ExternalLoginModel exlink, int role_id, out long UId)
        {
            Role role = new Role();
            var userExist = db.Users.FirstOrDefault(x => x.Email == exlink.Email);
            if (userExist != null)
            {

             
                var externalUser = db.ExternalLogins.FirstOrDefault(x => x.UId == userExist.Id);
                if (externalUser != null)
                {
                    externalUser.ModifyDate = DateTime.Now;
                    externalUser.Provider = exlink.Issuser;
                    externalUser.Token = exlink.Token;
                    db.SaveChanges();
                    role.Id = userExist.RoleId;
                    role.Name = userExist.Role.Name;
                    UId = userExist.Id;
                    return role;
                }
                else
                {
                    var externalUserToInsert = new DbContext.ExternalLogin();
                    externalUserToInsert.CreatedDate = DateTime.Now;
                    externalUserToInsert.ModifyDate = DateTime.Now;
                    externalUserToInsert.Token = exlink.Token;
                    externalUserToInsert.ProviderId = exlink.ProviderId;
                    externalUserToInsert.Provider = exlink.Issuser;
                    externalUserToInsert.UId = userExist.Id;
                    db.ExternalLogins.Add(externalUserToInsert);
                    db.SaveChanges();
                    role.Id = userExist.RoleId;
                    role.Name = userExist.Role.Name;
                    UId = userExist.Id;
                    return role;
                }
            }
            else
            {

                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {

                        var temprole = db.Roles.Find(role_id);

                        role.Id = temprole.Id;
                        role.Name = temprole.Name;


                        var _user = new DbContext.User();
                        _user.Email = exlink.Email;
                        _user.FullName = exlink.Name;
                        _user.ProfileImagePath = exlink.Picture;
                        _user.RoleId = role_id;
                        _user.IsUserTypeVarified = true;
                        _user.Gender = "null";
                        _user.IsActive = true;
                        _user.Password = Guid.NewGuid().ToString("d").Substring(1, 8);
                        _user.IsDeleted = false;
                        _user.CreatedDate = DateTime.Now;
                        _user.ModifyDate = DateTime.Now;
                        db.Users.Add(_user);
                        db.SaveChanges();
                        var externalUserToInsert = new DbContext.ExternalLogin();
                        externalUserToInsert.CreatedDate = DateTime.Now;
                        externalUserToInsert.ModifyDate = DateTime.Now;
                        externalUserToInsert.Token = exlink.Token;
                        externalUserToInsert.ProviderId = exlink.ProviderId;
                        externalUserToInsert.Provider = exlink.Issuser;
                        externalUserToInsert.UId = _user.Id;


                        DbContext.SubscriptionPackage subscription = new SubscriptionPackage();
                        subscription.CreatedDate = DateTime.Now;
                        subscription.ModifyDate = DateTime.Now;
                        subscription.UId = _user.Id;
                        subscription.IsActive = true;
                        subscription.OId = 1;

                        db.SubscriptionPackages.Add(subscription);
                        db.SaveChanges();


                        db.ExternalLogins.Add(externalUserToInsert);
                        db.SaveChanges();
                        transaction.Commit();
                    
                        UId = _user.Id;


                        string EmailString = Utilties.Genral.EmailTemplates.WelcomeEmailString(_user.FullName, _user.Email,_user.Password);
                        EmailSender.SendEmail(_user.Email, "Welcome to TripJero", EmailString);
                        return role;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        UId = 0;
                        return null;
                    }
                }

            }
        }

        public bool isActiveUser(string Email)
        {
            return db.Users.Where(x => x.Email == Email).FirstOrDefault().IsActive;
        }

        public object ForegetPassword(string email)
        {
            var existingemail = db.Users.FirstOrDefault(x => x.Email == email);
            if (existingemail != null)
            {
                var random = new Random();
                int randomnumber = random.Next();

               

                string EmailString = Utilties.Genral.EmailTemplates.resetpass(existingemail.FullName, existingemail.Email, randomnumber.ToString());
                EmailSender.SendEmail(existingemail.Email, "Reset Password", EmailString);

                existingemail.OPT = randomnumber;
                existingemail.ResetPasswordRequestDate = DateTime.Now;
                db.SaveChanges();
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Password reset instructions sent to your email", true);
            }
            else
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Email is not exist", false);
            }
        }

        public async Task<FacebookModel> GetFacebookResponseAsync(string token)
        {
            if (token != null)
            {
                string url = ConfigurationManager.AppSettings.Get("facebookaccessprofile").ToString() + "access_token=" + token + "&fields=" + fbfileds;
                FacebookModel facebookModel = new FacebookModel();
                HttpResponseMessage response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    facebookModel = new JavaScriptSerializer().Deserialize<FacebookModel>(result);
                    return facebookModel;
                }
                return null;
            }
            return null;
        }

        public object ResetPassword(ResetPassword resetPassword)
        {
            var optVerfication = db.Users.FirstOrDefault(x => x.Email == resetPassword.Email && x.OPT == resetPassword.Opt);
            if (optVerfication != null && resetPassword.Opt > 0)
            {
                var resetDate = (DateTime)optVerfication.ResetPasswordRequestDate;

                TimeSpan ts = DateTime.Now - resetDate;
                Console.WriteLine("No. of Hours (Difference) = {0}", ts.TotalHours);

                if (ts.TotalHours > 1)
                {
                    return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Your password reset request is timeout.Please resend request for opt", false);

                }
                else
                {
                    optVerfication.Password = resetPassword.Password;
                    optVerfication.OPT = null;
                    db.SaveChanges();
                    return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Your password has been reset", true);
                }
            }
            else
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Your opt is not valid", false);
            }
        }

        private string ForgetPasswordEmailString(string username, int opt)
        {
            string emailHTML = "<div align='center'><h3>Dear " + username + " ,</h3><p align='center'>Your one time password is : <b>" + opt + "</b></p><p>Use given one time password for rest your password.</p>"
      + "<div><b>Disclaimer: </b> TripJero, does not handles or responsible for financial losses of either party, TripJero is facility provider to query trips and operators.Our contact channels are open for disputes and complains.</div><br><br>"
                                + @"<div align='center'>
                                © <span style='color:blue'>Trip</span><span style='color:orange'>Jero</span>, 2020. All rights reserved.
                                TripJero.com | Islamabad, Pakistan
                                </div>";
            return emailHTML;
        }
    }
}