﻿using Buisness.Core.DbContext;
using DataEntites.Model.Account;
using System.Threading.Tasks;

namespace Buisness.Core.Account
{
    public interface IAccount
    {
        object ForegetPassword(string email);

        object ResetPassword(ResetPassword resetPassword);

        //Role ExternalResgistraion(ExternalLoginModel resetPassword, int role_id, out long UId);

        Role RoleBaseExternalResgistraion(ExternalLoginModel resetPassword,int role_id, out long UId);

        bool isActiveUser(string Email);
        Task<FacebookModel> GetFacebookResponseAsync(string token);
    }
}