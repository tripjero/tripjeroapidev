﻿using Buisness.Core.DbContext;
using DataEntites.GeneraicRepositroy;
using DataEntites.Model;
using DataEntites.Model.Admin.Counter;
using DataEntites.Model.Admin.Operator;
using DataEntites.Model.Trip;
using DataEntites.Model.Trip.TripReviewModel;
using DataEntites.Model.User;
using DataEntites.Model.User.Complaint;
using DataEntites.Model.User.Firebase;
using DataEntites.Model.User.OperatorInfo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using TripJero.Notification.Services;
using Utilties.Genral;

namespace Buisness.Core.User
{
    public class UserSerivce : IUser
    {
        private TripJeroEntities db = new TripJeroEntities();

        public object ChangePassword(string CurrentPassword, string NewPassword, long id)
        {
            var users = db.Users.FirstOrDefault(x => x.Password == CurrentPassword && x.Id == id);
            if (users != null)
            {
                users.Password = NewPassword;
                db.SaveChanges();
                return ObjectSerilization.GetSerilizedWithoutObject("Password Reseted", true);
            }
            else
            {
                return ObjectSerilization.GetSerilizedWithoutObject("Your current password not matched", false);
            }
        }

        public object DeActiveAccount(long Id)
        {
            var users = db.Users.Find(Id);
            if (users != null)
            {
                users.isDeactivated = true;
                db.SaveChanges();
                return ObjectSerilization.GetSerilizedWithoutObject("Account Deactive successfully", true);
            }
            else
            {
                return ObjectSerilization.GetSerilizedWithoutObject("Your current password not matched", false);
            }
        }

        public UserModel GetUserByEmail(string Email)
        {
            DbContext.User dbUser = db.Users.FirstOrDefault(x => x.Email == Email && x.IsActive == true && x.IsDeleted == false);
            if (dbUser != null)
            {
                return UserHelper(dbUser);
            }
            return UserHelper(null);
        }

        public UserModel GetUserByEmailAndPassword(string Email, string Password)
        {
            var user = db.Users.FirstOrDefault(x => x.Email == Email && x.Password == Password && x.IsActive == true && x.IsDeleted == true);
            return UserHelper(user);
        }

        public object GetUserById(double Id)
        {
            var user = db.Users.FirstOrDefault(x => x.Id == Id && x.IsDeleted == false);
            if (user.IsUserTypeVarified == null || (bool)user.IsUserTypeVarified)
            {
                return ObjectSerilization.GetSerilizedObject("User Founds", true, UserHelper(user));
            }
            else
            {
                NonVarifiedUsers nonVarifiedUsers = new NonVarifiedUsers();
                nonVarifiedUsers.IsUserTypeVarified = false;
                nonVarifiedUsers.UserId = user.Id;
                return ObjectSerilization.GetSerilizedObject("User Founds With Unvarified User Type", true, nonVarifiedUsers);
            }
        }

        public object AddSocialLinks(UsersSocialLinks operatorSocialLinks)
        {
            try
            {
                var socialMedia = db.UsersSocialLinks.FirstOrDefault(x => x.TypeId == operatorSocialLinks.TypeId && x.UId == operatorSocialLinks.UId);
                if (socialMedia != null)
                {
                    socialMedia.Url = operatorSocialLinks.Url;
                    db.SaveChanges();
                    return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Social Url Updated", true);
                }

                DbContext.UsersSocialLink usersSocialLink = new UsersSocialLink();
                usersSocialLink.UId = operatorSocialLinks.UId;
                usersSocialLink.TypeId = operatorSocialLinks.TypeId;
                usersSocialLink.Url = operatorSocialLinks.Url;
                db.UsersSocialLinks.Add(usersSocialLink);
                db.SaveChanges();
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Users Social Links Inserted", true);
            }
            catch (Exception ex)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false);
            }
        }

        public object AddTripToFavt(long user_id, long trip_id)
        {

            if (db.FavtTrips.Where(x => x.user_id == user_id && x.trip_id == trip_id && x.is_delete == false).FirstOrDefault() != null)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("This trip is already exist in your favourite list.", false);
            }
            else
            {

                try
                {
                    FavtTrip trip = new FavtTrip();
                    trip.user_id = user_id;
                    trip.trip_id = trip_id;
                    trip.is_delete = false;
                    db.FavtTrips.Add(trip);
                    db.SaveChanges();
                    return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("This trip has been added to your favourite list.", true);

                }
                catch (Exception ex)
                {
                    return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false);

                }

            }
        }

        public object DelFavtTrip(long user_id, long trip_id)
        {
            var trip = db.FavtTrips.Where(x => x.user_id == user_id && x.trip_id == trip_id && x.is_delete == false).FirstOrDefault();

            if (trip == null)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Trip found", false);
            }
            else
            {

                try
                {

                    db.FavtTrips.Remove(trip);
                    db.SaveChanges();
                    return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Trip has been removed from your favourite list.", true);

                }
                catch (Exception ex)
                {
                    return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false);

                }

            }
        }


        public object UpdateUserProfileImage(string ImageUrl, long Id)
        {
            var userUpdate = db.Users.Find(Id);
            if (ImageUrl != null)
            {
                userUpdate.ProfileImagePath = DocumentManager.SaveFile(ImageUrl, "/UserImages/" + Id + "/", ".jpg");
                var profileUrl = new { ProfileUrl = userUpdate.ProfileImagePath.Replace("~", "") };
                db.SaveChanges();
                return Utilties.Genral.ObjectSerilization.GetSerilizedObject("User Profile Updated", true, profileUrl);
            }
            return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Image string required", false);
        }

        public object UserRegistraion(UserModel users)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (db.Users.FirstOrDefault(x => x.Email == users.Email) != null)
                    {
                        transaction.Rollback();
                        return ObjectSerilization.GetSerilizedWithoutObject("Email Aready exist", false);
                    }
                    if (users.ContactNumber != null && !string.IsNullOrEmpty(users.ContactNumber))
                    {
                        if (db.Users.First(x => x.ContactNumber == users.ContactNumber) != null)
                        {
                            transaction.Rollback();
                            return ObjectSerilization.GetSerilizedWithoutObject("This number already exist associated with with another account", false);
                        }
                    }

                    DbContext.User user = new DbContext.User();
                    user.IsActive = true;
                    user.IsVarified = false;
                    user.IsDeleted = false;
                    user.FullName = users.Name;
                    user.Gender = users.Gender;
                    user.CreatedDate = DateTime.Now;
                    user.ModifyBy = 1;
                    user.Password = users.Password;
                    user.Remarks = users.Remarks;
                    user.ModifyDate = DateTime.Now;
                    user.Email = users.Email;
                    user.RoleId = users.RoleId;
                    user.PhoneNumber = users.PhoneNumber;
                    user.DateOfBirth = users.DateOfBirth;
                    user.FirebaseToken = users.FirebaseToken;
                    user.PhoneNumber = users.PhoneNumber;
                    user.ContactNumber = users.ContactNumber;
                    db.Users.Add(user);
                    db.SaveChanges();
                    if (users.ProfileImage != null)
                    {
                        user.ProfileImagePath = DocumentManager.SaveFile(users.ProfileImage, "~/UserImages/" + user.Id + "/", ".jpg");
                    }
                    db.SaveChanges();
                    if (users.FullAddress != null)
                    {
                        DbContext.Address address = new DbContext.Address();
                        address.Id = user.Id;
                        address.PermentAddress = users.FullAddress;
                        address.CityId = users.CityId;
                        db.Addresses.Add(address);
                        db.SaveChanges();
                    }
                    if (users.Licence != null && users.Licence.LicenceNumber!=null && users.RoleId == 3)
                    {
                        DbContext.LicenceNo licenceNo = new DbContext.LicenceNo();
                        if (users.Licence.LicenceImage != null)
                        {
                            licenceNo.FileUrl = DocumentManager.SaveFile(users.Licence.LicenceImage, "~/LicenceNo/" + user.Id + "/", ".jpg");
                            users.Licence.LicenceImage = licenceNo.FileUrl;
                        }
                        licenceNo.LicenceNo1 = users.Licence.LicenceNumber != null ? users.Licence.LicenceNumber : "";
                        licenceNo.ModifyBy = 1;
                        licenceNo.ModifyDate = DateTime.Now;
                        licenceNo.Remakrs = users.Licence.Remakas != null ? users.Licence.Remakas : "No Remarks";
                        licenceNo.Status = false;
                        licenceNo.UId = user.Id;
                        licenceNo.CreatrdDate = DateTime.Now;
                        db.LicenceNoes.Add(licenceNo);
                        db.SaveChanges();
                        DbContext.SubscriptionPackage subscription = new SubscriptionPackage();
                        subscription.CreatedDate = DateTime.Now;
                        subscription.ModifyDate = DateTime.Now;
                        subscription.UId = user.Id;
                        subscription.IsActive = true;
                        subscription.OId = users.PackageId;

                        db.SubscriptionPackages.Add(subscription);
                        db.SaveChanges();
                        if (users.BuisnessName != null)
                        {
                            DbContext.OperatorInfo operatorInfo = new OperatorInfo();
                            operatorInfo.BuisnessName = users.BuisnessName;
                            operatorInfo.Id = user.Id;
                            db.OperatorInfoes.Add(operatorInfo);
                            db.SaveChanges();
                        }
                    }
                    transaction.Commit();
                    //var notifcationList = db.NotificationTrayTbls.FirstOrDefault(x => x.Title == "NewUserNotifi");
                    //if (notifcationList!=null && users.FirebaseToken!=null)
                    //{
                    //    NotificationSender.SendNotification(users.FirebaseToken, notifcationList.NotificationMessage, notifcationList.Title);
                    //}

                    string EmailString = Utilties.Genral.EmailTemplates.WelcomeEmailString(users.Name, users.Email,users.Password);
                    EmailSender.SendEmail(users.Email, "Welcome to TripJero", EmailString);
                    return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Users Registred", true);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false);
                }
            }
        }

        public object UserUpdate(UserModel users)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (db.Users.FirstOrDefault(x => x.Email == users.Email && x.Id != users.Id) != null)
                    {
                        transaction.Rollback();
                        return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Email already exist", false);
                    }
                    if (users.ContactNumber != null && !string.IsNullOrEmpty(users.ContactNumber.Trim()))
                    {
                        if (db.Users.FirstOrDefault(x => x.ContactNumber == users.ContactNumber && x.Id != users.Id) != null)
                        {
                            transaction.Rollback();
                            return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Number already exist with another account", false);
                        }
                    }
                    DbContext.User user = db.Users.Find(users.Id);
                    user.FullName = users.Name;
                    user.Gender = users.Gender;
                    user.ModifyBy = users.Id;
                    user.ModifyDate = DateTime.Now;
                    user.PhoneNumber = users.PhoneNumber;
                    user.DateOfBirth = users.DateOfBirth;
                    
                    user.PhoneNumber = users.PhoneNumber;
                    user.ContactNumber = users.ContactNumber;
                    user.isDeactivated = false;
                    user.Country = users.Country;
                    user.City = users.City;


                    if (users.FullAddress != null)
                    {
                        if (user.Address != null)
                        {
                            DbContext.Address addressUpdate = db.Addresses.Find(users.Id);
                            addressUpdate.PermentAddress = users.FullAddress;
                            if (users.CityId != 0)
                            {
                                addressUpdate.CityId = users.CityId;
                            }
                        }
                        else
                        {
                            DbContext.Address address = new DbContext.Address();
                            address.Id = user.Id;
                            address.PermentAddress = users.FullAddress;
                            if (users.CityId != 0)
                            {
                                address.CityId = users.CityId;
                            }
                            db.Addresses.Add(address);
                        }
                    }
                    if (user.RoleId == 3)
                    {
                      
                        if (users.BuisnessName != null)
                        {
                            if (user.OperatorInfo != null)
                            {
                                DbContext.OperatorInfo operatorUpdate = db.OperatorInfoes.Find(users.Id);
                                operatorUpdate.BuisnessName = users.BuisnessName;
                            }
                            else
                            {
                                DbContext.OperatorInfo operatorInfo = new OperatorInfo();
                                operatorInfo.BuisnessName = users.BuisnessName;
                                operatorInfo.Id = user.Id;
                                db.OperatorInfoes.Add(operatorInfo);
                            }
                        }
                    }
                    db.SaveChanges();
                    transaction.Commit();
                    return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Users Updated", true);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false);
                }
            }
        }


        public object UpdateOptLiecence (DataEntites.Model.User.LicenceNo userlicence , long Uid)
        {

            try
            {
                DbContext.User user = db.Users.Find(Uid);

                if (user.LicenceNoes.Count > 0)
                {
                    var licence = db.LicenceNoes.FirstOrDefault(x => x.UId == user.Id);
                    licence.ModifyBy = user.Id;
                    licence.ModifyDate = DateTime.Now;
                    licence.ModifyBy = Uid;
                    licence.LicenceNo1 = userlicence.LicenceNumber;
                    licence.Status = false;

                    if (userlicence.LicenceImage != null && !string.IsNullOrEmpty(userlicence.LicenceImage))
                    {
                        licence.FileUrl = DocumentManager.SaveFile(userlicence.LicenceImage, "~/LicenceNo/" + user.Id + "/", ".jpg");
                    }
                }
                else
                {
                    DbContext.LicenceNo licenceNo = new DbContext.LicenceNo();
                    if (userlicence.LicenceImage != null && !string.IsNullOrEmpty(userlicence.LicenceImage))
                    {
                        licenceNo.FileUrl = DocumentManager.SaveFile(userlicence.LicenceImage, "~/LicenceNo/" + user.Id + "/", ".jpg");
                        userlicence.LicenceImage = licenceNo.FileUrl;
                    }
                    licenceNo.LicenceNo1 = userlicence.LicenceNumber != null ? userlicence.LicenceNumber : "";
                    licenceNo.ModifyBy = user.Id;
                    licenceNo.ModifyDate = DateTime.Now;
                    licenceNo.Remakrs = userlicence.Remakas != null ? userlicence.Remakas : "Pending Verfication";
                    licenceNo.Status = false;
                    licenceNo.UId = user.Id;
                    licenceNo.CreatrdDate = DateTime.Now;
                    db.LicenceNoes.Add(licenceNo);
                }

                db.SaveChanges();
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("You licence is updated and forwarded for verfication.", true);
            }
            catch(Exception ex)
            {

                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false);
            }




        }

        public void ReActivateUser(long id)
        {

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                   
                    DbContext.User user = db.Users.Find(id);
               
                    user.ModifyDate = DateTime.Now;
                    
                    user.isDeactivated = false;
                 


                  
                 
                    db.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                }
            }

        }

        private UserModel UserHelper(DbContext.User dbUser)
        {
            UserModel user = new UserModel();
            if (dbUser != null)
            {
                user.Id = dbUser.Id;
                user.Name = dbUser.FullName;
                user.CreatedDate = dbUser.CreatedDate;
                user.Email = dbUser.Email;
                user.Gender = dbUser?.Gender?.Trim();
                user.ModifyDate = dbUser.ModifyDate;
                user.RoleId = dbUser.RoleId;
                user.RoleName = dbUser.Role.Name;
                user.IsActive = dbUser.IsActive;
                user.IsUserTypeVarified = dbUser.IsUserTypeVarified;
                user.isDeactivated = dbUser.isDeactivated;
                user.Country = dbUser.Country;
                user.City = dbUser.City;

                if (dbUser.UsersSocialLinks.Count > 0)
                {
                    foreach (var item in dbUser.UsersSocialLinks)
                    {
                        UsersSocialLinksToShow operatorSocialLinks = new UsersSocialLinksToShow();
                        operatorSocialLinks.Url = item.Url;
                        operatorSocialLinks.TypeId = item.TypeId;
                        operatorSocialLinks.Type = item.SocailType.Name;
                        user.OperatorSocialLinks.Add(operatorSocialLinks);
                    }
                }
                if (dbUser.Address != null)
                {
                    if (dbUser.Address.CityId != null)
                    {
                        user.CityId = dbUser.Address.CityId != null ? dbUser.Address.CityId : 0;
                        user.CityName = dbUser.Address.City.Name;
                        user.ProvinceName = dbUser.Address.City.Province.Name;
                        user.CountryName = dbUser.Address.City.Province.Country.Name;
                    }

                    user.FullAddress = dbUser.Address.PermentAddress;
                }

                user.PhoneNumber = dbUser.PhoneNumber;
                user.ContactNumber = dbUser.ContactNumber;
                user.ProfileImage = dbUser.ProfileImagePath != null ? dbUser.ProfileImagePath.Contains("~") ? dbUser.ProfileImagePath.Replace("~", "") : dbUser.ProfileImagePath : null;
                user.FirebaseToken = dbUser.FirebaseToken;
                if (dbUser.DateOfBirth != null)
                {
                    user.DateOfBirth = (DateTime)dbUser.DateOfBirth;
                }
                else
                {
                    user.DateOfBirth = null;
                }
                user.IsVarified = dbUser.IsVarified;
                user.IsLicenceVarified = false;
                user.BuisnessName = dbUser.OperatorInfo != null ? dbUser.OperatorInfo.BuisnessName : "";
                var packagesInfor = db.SubscriptionPackages.FirstOrDefault(x => x.UId == user.Id && x.IsActive == true);
                if (packagesInfor != null)
                {
                    user.PackageId = packagesInfor.Id;
                    user.PackageName = packagesInfor.OperatorPackage != null ? packagesInfor.OperatorPackage.Name : null;
                 
                }


                if (dbUser.LicenceNoes != null && user.RoleId == 3)
                {
                    foreach (var item in dbUser.LicenceNoes)
                    {
                        user.Licence.CreatedDate = (DateTime)item.CreatrdDate;
                        user.Licence.Id = item.Id;
                        user.Licence.LicenceNumber = item.LicenceNo1;
                        user.Licence.LicenceImage = item.FileUrl != null ? item.FileUrl.Contains("~") ? item.FileUrl.Replace("~", "") : item.FileUrl : null;
                        user.Licence.Status = item.Status;
                        user.IsLicenceVarified = item.Status;
                        break;
                    }
                   
                }

            

                if(dbUser.isDeactivated != null && dbUser.isDeactivated == true)
                {
                    user.NeedToShowWelcomeBack = true;
              
                
                    ReActivateUser(user.Id);

                }
                else
                {
                    user.NeedToShowWelcomeBack = false;
                }

        
                return user;
            }
            return null;
        }

        public object GetTripOperatorInfoById(long Id)
        {
            TripOperatorInfo operatorInfo = new TripOperatorInfo();
            var tipOperatro = db.Users.Find(Id);
            if (tipOperatro != null)
            {
                operatorInfo.BuisnessName = tipOperatro.OperatorInfo != null ? tipOperatro.OperatorInfo.BuisnessName : "";
                operatorInfo.Address = tipOperatro.Address != null ? tipOperatro.Address.PermentAddress : "";
                operatorInfo.PhoneNumber = tipOperatro.ContactNumber;
                operatorInfo.MobileNumber = tipOperatro.PhoneNumber;
                operatorInfo.IsVarified = tipOperatro.IsVarified;
                operatorInfo.ProfileImage = tipOperatro.ProfileImagePath;
                operatorInfo.Email = tipOperatro.Email;
                operatorInfo.OperatorId = tipOperatro.Id;
                var OperatorId = new SqlParameter("OperatorId", SqlDbType.BigInt) { Value = Id };
                var tripReviewsList = SpRepository<DataEntites.Model.Trip.TripReviewModel.OperatorsReviews>.GetListWithStoreProcedure(@"exec Get_Operators_Review @OperatorId", OperatorId);
                operatorInfo.OperatorReviews = tripReviewsList;

                operatorInfo.TotalReviews = tripReviewsList.Count();
                var total = tripReviewsList.Sum(x => x.TripStar);
                if (tripReviewsList.Count > 0)
                {
                    operatorInfo.AvgReviews = (total / tripReviewsList.Count());
                }
                else
                {
                    operatorInfo.AvgReviews = 0;
                }
                var operatorCompleteTrips = db.GetAllCompletedTripByUserId(Id, 1).ToList();

                if (operatorCompleteTrips.Count > 0)
                {
                    operatorInfo.CompletedTrips = operatorCompleteTrips.Count();
                    foreach (var item in operatorCompleteTrips)
                    {
                        OperatorTrips operatorTrips = new OperatorTrips();
                        operatorTrips.Id = item.Id;
                        operatorTrips.Title = item.Title;
                        operatorInfo.OperatorRecentTrips.Add(operatorTrips);
                    }
                }
                return ObjectSerilization.GetSerilizedObject("Operator Info Found", true, operatorInfo);
            }
            return ObjectSerilization.GetSerilizedWithoutObject("No Operator Info Found", false);
        }

        public object GetComplaintByUserId(long Id,int pageNo,int pageSize)
        {
            var UserId = new SqlParameter("UserId", SqlDbType.BigInt) { Value = Id };
            var _pageNo = new SqlParameter("PageNumber", SqlDbType.Int) { Value = pageNo };
            var _pageSize = new SqlParameter("RowOfPage", SqlDbType.Int) { Value = pageSize };
            var ComplaintList = SpRepository<UsersComplaint>.GetListWithStoreProcedure(@"exec Get_Complaint_BY_UId 
                                @UserId,@PageNumber,@RowOfPage", UserId,_pageNo,_pageSize);
            if (ComplaintList.Count > 0)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Complaints Founds", true, ComplaintList, ComplaintList[0].PageSize, ComplaintList[0].PageNo, ComplaintList[0].TotalPage);
            }
            else
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Comlaint Found", false);
            }
        }

        public object UpdateToken(FirebaseToken firebaseToken, long UId)
        {
            try
            {
                var users = db.Users.Find(UId);
                users.FirebaseToken = firebaseToken.Token;
                users.ModifyDate = DateTime.Now;
                db.SaveChanges();
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Toke Updated", true);
            }
            catch (Exception ex)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false);
            }
        }
        public object GetAllOperators(int pageNo,int pageSize)
        {
            List<DataEntites.Model.Operators.AllOperatorModel> operators = new List<DataEntites.Model.Operators.AllOperatorModel>();
            var _pageNo = new SqlParameter("PageNumber", SqlDbType.Int) { Value = pageNo };
            var _pageSize = new SqlParameter("RowOfPage", SqlDbType.Int) { Value = pageSize };

            var result = SpRepository<DataEntites.Model.Admin.User.Users>.GetListWithStoreProcedure(@"exec GetAllOperators @PageNumber,@RowOfPage",_pageNo,_pageSize);
            if (result.Any())
            {
                foreach (var item in result)
                {
                    DataEntites.Model.Operators.AllOperatorModel opt = new DataEntites.Model.Operators.AllOperatorModel();
                    opt.BasicInfo = item;
                    ReviewsCount reviewsCount = new ReviewsCount();
                    var OperatorIdReviews = new SqlParameter("OperatorId", SqlDbType.BigInt) { Value = item.Id };
                    var OperatorIdtt = new SqlParameter("OperatorId", SqlDbType.BigInt) { Value = item.Id };

                    reviewsCount = SpRepository<ReviewsCount>.GetSingleObjectWithStoreProcedure(@"exec TotalReviewsAndAvgStarts @OperatorId", OperatorIdReviews);
                    opt.AvgStars = reviewsCount.AvgStars;
                    opt.TotalRevews = reviewsCount.TotalReviews;

                    var totaltrips = SpRepository<ReviewsCount>.GetSingleObjectWithStoreProcedure(@"exec TotalOperatorTrips @OperatorId", OperatorIdtt);
                    opt.TotalTrips = totaltrips.TotalReviews;



                    var opratorId = new SqlParameter("OperatorId", SqlDbType.BigInt) { Value = item.Id };
                    var _statusId = new SqlParameter("StatusId", SqlDbType.Int) { Value = 3 };

                    var operatorTrips = SpRepository<OperatorTripStatus>.GetListWithStoreProcedure(@"exec Get_Operators_Trips_BY_Operator_Id @OperatorId,@StatusId", opratorId, _statusId);

                    opt.ScheduleTrips = getTripByOptId(item.Id,3);

                    operators.Add(opt);

                }
                return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Users Founds", true, operators,
                    result[0].PageSize, result[0].PageNo,result[0].TotalPage);
            }
            else
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Users Founds", false);
            }
        }


        public List<TripModel> getTripByOptId(long optId,int TripStatusId)
        {

            var _TripStatusId = new SqlParameter("TripStatusId", SqlDbType.Int) { Value = TripStatusId };
           
            var _keyword = new SqlParameter("optId", SqlDbType.VarChar) { Value = optId };
            var tripList = SpRepository<TripBasicInfo>.GetListWithStoreProcedure(@"exec GetScheduleTripsByOptId @optId, @TripStatusId", _keyword, _TripStatusId);
            if (tripList.Any())
            {
                List<TripModel> tripModelsList = new List<TripModel>();
                foreach (var item in tripList)
                {
                    var fulldetail = false;

                    if(TripStatusId == 0)
                    { fulldetail = true;
                    }
                    tripModelsList.Add(getGenaricTrip(item, fulldetail));
                }
                return tripModelsList;
            }
            else
            {
                List<TripModel> tripModelsList = new List<TripModel>();
                return tripModelsList;
            }
        }


        private TripModel getGenaricTrip(TripBasicInfo tripBasicInfo, bool isDetials, int roleId = 0)
        {
            TripModel tripModel = new TripModel();
            if (tripBasicInfo != null)
            {
                tripModel.TripBasicInfo = tripBasicInfo;
                var OperatorId = new SqlParameter("OperatorId", SqlDbType.BigInt) { Value = tripBasicInfo.UId };
                var OperatorIdReviews = new SqlParameter("OperatorId", SqlDbType.BigInt) { Value = tripBasicInfo.UId };
                var tripId = new SqlParameter("TripId", SqlDbType.BigInt) { Value = tripBasicInfo.Id };
                List<OperatorsReviews> tripReviewsList = new List<OperatorsReviews>();
                ReviewsCount reviewsCount = new ReviewsCount();
                if (roleId > 0 && roleId == 3)
                {
                    tripReviewsList = SpRepository<DataEntites.Model.Trip.TripReviewModel.OperatorsReviews>.GetListWithStoreProcedure(@"exec Get_Operators_Review_By_TId @OperatorId,@TripId", OperatorId, tripId);
                }
                else
                {

                    reviewsCount = SpRepository<ReviewsCount>.GetSingleObjectWithStoreProcedure(@"exec TotalReviewsAndAvgStarts @OperatorId", OperatorIdReviews);
                    tripReviewsList = SpRepository<DataEntites.Model.Trip.TripReviewModel.OperatorsReviews>.GetListWithStoreProcedure(@"exec Get_Operators_Review @OperatorId", OperatorId);
                }
                if (tripReviewsList.Count > 0)
                {
                    tripModel.AvgStars = reviewsCount.AvgStars;
                    tripModel.TotalRevews = reviewsCount.TotalReviews;
                }
                #region trip Price
                //var tripId = new SqlParameter("TripId", SqlDbType.BigInt) { Value = tripBasicInfo.Id };
                //var tripPrices = SpRepository<DataEntites.Model.Trip.TripPrice.TripPrice>.GetSingleObjectWithStoreProcedure(@"exec Get_DiscountedTrip_Price @TripId", tripId);
                tripModel.TripStatus = tripBasicInfo.TripStatus;
                tripModel.IsBoost = tripBasicInfo.IsBoost;

                if (tripBasicInfo.DiscountPercentage != null && tripBasicInfo.DiscountPercentage > 0)
                {
                    tripModel.tripPrices.ActualAmount = tripBasicInfo.Amount;
                    tripModel.tripPrices.CurrentAmount = (double)(tripBasicInfo.Amount * (100.0 - tripBasicInfo.DiscountPercentage) / 100.0);
                    tripModel.tripPrices.DiscountPercentage = (double)tripBasicInfo.DiscountPercentage;
                    tripModel.tripPrices.HavePromo = tripBasicInfo.Is_Promo;
                    tripModel.tripPrices.PromoCode = tripBasicInfo.PromoCode;
                    tripModel.tripPrices.trip_id = (long)tripBasicInfo.Id;
                    tripModel.tripPrices.user_id = tripBasicInfo.UId;
                    tripModel.tripPrices.PromoCode = tripBasicInfo.PromoCode;
                }
                else
                {
                    tripModel.tripPrices.ActualAmount = tripBasicInfo.Amount;
                    tripModel.tripPrices.CurrentAmount = (double)tripBasicInfo.Amount;
                    tripModel.tripPrices.DiscountPercentage = 0;
                    tripModel.tripPrices.PromoCode = tripBasicInfo.PromoCode;
                    tripModel.tripPrices.HavePromo = tripBasicInfo.Is_Promo;
                    tripModel.tripPrices.trip_id = (long)tripBasicInfo.Id;
                    tripModel.tripPrices.user_id = tripBasicInfo.UId;
                    tripModel.tripPrices.PromoCode = tripBasicInfo.PromoCode;
                }
                #endregion
                #region trip Faclities

                var faclitiesList = db.Facilties.FirstOrDefault(x => x.TripId == tripBasicInfo.Id);
                if (faclitiesList != null)
                {
                    tripModel.Faclities.Id = faclitiesList.Id;
                    tripModel.Faclities.Transportation = faclitiesList.Transportation != null ? faclitiesList.Transportation : "";
                    tripModel.Faclities.Facalities = faclitiesList.FacalitiesList != null ? faclitiesList.FacalitiesList : "";
                    tripModel.Faclities.Accommodiation = faclitiesList.Accommodation != null ? faclitiesList.Accommodation : "";

                    tripModel.Faclities.HaveTransport = faclitiesList.HaveTransport;
                    tripModel.Faclities.HaveAccomodation = faclitiesList.HaveAccomodation;
                    tripModel.Faclities.HaveMeals = faclitiesList.HaveMeals;
                    tripModel.Faclities.HaveFirstAid = faclitiesList.HaveFirstAid;
                    tripModel.Faclities.Icon_Accomodation = faclitiesList.Icon_Accomodation;
                    tripModel.Faclities.Icon_Meals = faclitiesList.Icon_Meals;
                    tripModel.Faclities.Icon_FirstAid = faclitiesList.Icon_FirstAid;
                    tripModel.Faclities.Icon_Transport = faclitiesList.Icon_Transport;
                }

                #endregion trip Facilities

                if (isDetials)
                {
                    if (tripReviewsList.Count() > 0)
                    {
                        tripModel.TripReviews = tripReviewsList;
                    }
                    #region tripImage

                    var TImageList = db.TripImages.Where(x => x.TripId == tripBasicInfo.Id);
                    if (TImageList.Any())
                    {
                        foreach (var item in TImageList)
                        {
                            DataEntites.Model.Trip.TripImages tripImages = new DataEntites.Model.Trip.TripImages();
                            tripImages.Id = item.Id;
                            tripImages.IsActive = item.IsActive;
                            tripImages.CreatedDate = item.CreatedDate;
                            tripImages.ImageUrl = item.ImageUrl;
                            tripImages.ModifyDate = item.ModifyDate;
                            tripImages.ModifybyId = item.MofifyBy;
                            tripModel.TripImages.Add(tripImages);
                        }
                    }

                    #endregion tripImage


                    #region Iternary

                    var ItrenaryPlan = db.Iternaryplans.Where(x => x.TripId == tripBasicInfo.Id).ToList();

                    if (ItrenaryPlan.Count > 0)
                    {
                        foreach (var item in ItrenaryPlan)
                        {
                            DataEntites.Model.Trip.IternaryPlan plan = new IternaryPlan();
                            plan.Id = item.Id;
                            plan.DayName = item.DyaNumber;

                            if (item.DayPlans.Count > 0)
                            {
                                foreach (var dayv in item.DayPlans)
                                {
                                    DataEntites.Model.Trip.DayPlan dayPlan = new DataEntites.Model.Trip.DayPlan();
                                    dayPlan.DayId = dayv.DyId;
                                    dayPlan.TaskDetaisl = dayv.TaskDetails;
                                    dayPlan.Id = dayv.Id;
                                    plan.DayPlan.Add(dayPlan);
                                }
                            }
                            tripModel.IternaryPlan.Add(plan);
                        }
                    }

                    #endregion Itinerary


                }
            }
            return tripModel;
        }

        public object GetAllUsers()
        {
            var result = SpRepository<DataEntites.Model.Admin.User.Users>.GetListWithStoreProcedure(@"exec GetAllUsers");
            if (result.Any())
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Users Founds", true, result);
            }
            else
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Users Founds", false);
            }
        }

        public object GetAllUsersByIdForAdmin(long Id)
        {
            throw new NotImplementedException();
        }

        public bool UpdateUserType(long Id, int roleId)
        {
            var user = db.Users.Find(Id);
            if (user != null)
            {
                user.RoleId = roleId;
                user.IsUserTypeVarified = true;
                user.ModifyBy = Id;
                user.ModifyDate = DateTime.Now;
                if (user.RoleId == 3)
                {
                    DbContext.SubscriptionPackage subscription = new SubscriptionPackage();
                    subscription.CreatedDate = DateTime.Now;
                    subscription.ModifyDate = DateTime.Now;
                    subscription.UId = user.Id;
                    subscription.IsActive = true;
                    subscription.OId = 1;

                    db.SubscriptionPackages.Add(subscription);
                    db.SaveChanges();
                }
                db.SaveChanges();
                return true;
            }
            return false;
        }

        public DbContext.Role GetRoleById(int Id)
        {
            return db.Roles.Find(Id);
        }

        public object UpdateAdminUser(UserModel userModel)
        {
            if (userModel != null)
            {
                var user = db.Users.Find(userModel.Id);
                if (user != null)
                {
                    if (db.Users.FirstOrDefault(x => x.Email == userModel.Email && x.Id != userModel.Id) != null)
                    {
                        return new { Status = false, Data = "Email Already Exist" };
                    }
                    user.Email = userModel.Email;
                    user.Password = userModel.Password;
                    user.RoleId = userModel.RoleId;
                    user.Gender = userModel.Gender;
                    user.FullName = userModel.Name;
                    db.SaveChanges();
                    return ObjectSerilization.GetSerilizedWithoutObject("User Updated", true);
                }
                else
                {
                    return ObjectSerilization.GetSerilizedWithoutObject("No user found", true);
                }
            }
            else
            {
                return ObjectSerilization.GetSerilizedWithoutObject("Values requried", true);
            }
        }

        public object GetComplaint()
        {
            var ComplaintList = SpRepository<ComplaintModels>.GetListWithStoreProcedure(@"exec Get_ComplaintList");
            if (ComplaintList.Count > 0)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Complaints Founds", true, ComplaintList);
            }
            else
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Comlaint Found", false);
            }
        }

        public object UpdateComplaintStatus(long Id, bool status, string type, long userID)
        {
            var complaint = db.Complaints.Find(Id);
            if (type == "R") //Resolved Status
            {
                complaint.IsResolved = status;
                complaint.ModifyDate = DateTime.Now;
                complaint.ResolvedBy = userID;
                complaint.ModifyById = userID;
                db.SaveChanges();
                return ObjectSerilization.GetSerilizedWithoutObject("Complaint Status Updated", true);
            }
            else if (type == "V") // Visit Status
            {
                complaint.IsVisited = status;
                complaint.ModifyById = userID;
                db.SaveChanges();
                return ObjectSerilization.GetSerilizedWithoutObject("Complaint Status Updated", true);
            }
            return ObjectSerilization.GetSerilizedWithoutObject("Nothing to update", false);
        }

        public object UpdateTripReviewsAdmin(DataEntites.Model.Trip.TripReview tripReview)
        {
            var dbReviews = db.TripReviews.Find(tripReview.Id);
            dbReviews.IsAdminReviewd = true;
            dbReviews.Reviews = tripReview.Reviews;
            db.SaveChanges();
            return ObjectSerilization.GetSerilizedWithoutObject("Reviews Updated", true);
        }

        public object GetAllCounts()
        {
            var count = SpRepository<AllCounter>.GetSingleObjectWithStoreProcedure(@"exec GetAllCounts");
            if (count != null)
            {
                return ObjectSerilization.GetSerilizedObject("Count Found", true, count);
            }
            else
            {
                return ObjectSerilization.GetSerilizedWithoutObject("Count Found", false);
            }
        }

        public object UpdateStatus(long Id, bool Status)
        {
            try
            {
                var users = db.Users.Find(Id);
                users.IsActive = Status;
                db.SaveChanges();
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Users status updated", true);
            }
            catch (Exception ex)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("An Error ouces", false);
            }
        }

        public object UpdateLicenseStatus(long modifyBy, long Id, bool Status)
        {
            try
            {
                var licence = db.LicenceNoes.Where(x=>x.UId == Id).FirstOrDefault();
                if (licence != null)
                {
                    licence.Status = Status;
                    licence.ModifyBy = modifyBy;
                    licence.ValidBy = modifyBy;
                    db.SaveChanges();
                    return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Users license status updated", true);
                }
                else
                {
                    return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No licenece found for this operator", false);

                }
            }
            catch (Exception ex)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("An Error ouces", false);
            }
        }

        public object GetUsersPackageSubscription(bool status)
        {
            var st = new SqlParameter("Status", SqlDbType.Bit) { Value = status };
            var subPackagesList = SpRepository<OperatorSubPackage>.GetListWithStoreProcedure(@"exec GetUsersPackages @Status", st);
            return ObjectSerilization.GetSerilizedObject("Packages List", true, subPackagesList);

        }
        public object ChangePaymentStatus(long Id, bool status)
        {
            var exsitngSub = db.SubscriptionPackages.Find(Id);
            exsitngSub.IS_Payment = status;
            exsitngSub.ModifyDate = DateTime.Now;
            db.SaveChanges();
            var user = db.Users.Find(exsitngSub.UId);
            string EmailString = Utilties.Genral.EmailTemplates.profilePkgSubscriptionpayment(user.FullName, exsitngSub.OperatorPackage.Name, exsitngSub.OperatorPackage.Price.ToString(), exsitngSub.OperatorPackage.LimitaionTrips.ToString());
            EmailSender.SendEmail(user.Email, "Package subscription", EmailString);
            return ObjectSerilization.GetSerilizedWithoutObject("Payment Actived", true);
        }
        public object GetUsersBoostPackageSubscription(bool status)
        {
            var st = new SqlParameter("Status", SqlDbType.Bit) { Value = status };
            var subPackagesList = SpRepository<OperatorSubPackage>.GetListWithStoreProcedure(@"exec GetUsersBoostSubPackages @Status", st);
            return ObjectSerilization.GetSerilizedObject("Packages List", true, subPackagesList);

        }
        public object ChangeBoostPackagePaymentStatus(long Id, bool status)
        {
            var exsitngSub = db.BoostPackageSubscriptions.Find(Id);
            exsitngSub.IS_Payment = status;
            exsitngSub.ModifyDate = DateTime.Now;
            db.SaveChanges();
            var p_dis = 0;
            if (exsitngSub.IS_Promo == true)
            {
                p_dis = exsitngSub.BoostPackage.BoostPackagesPromo.Discount;
            }
            var user = db.Users.Find(exsitngSub.UId);
            string EmailString = Utilties.Genral.EmailTemplates.boostpkgsubrecipt(user.FullName, exsitngSub.BoostPackage.Title, exsitngSub.TotalAmmount.ToString(), exsitngSub.BoostPackage.Discount.ToString(), p_dis.ToString(), exsitngSub.CurrentAmmount.ToString(), exsitngSub.BoostPackage.NoTrips.ToString());
            EmailSender.SendEmail(user.Email, "Boost Packge Subscription", EmailString);
            return ObjectSerilization.GetSerilizedWithoutObject("Payment Actived", true);
        }

        
        public object GetAllOperatorsReviews(int pageNo, int pageSize)
        {
            List<DataEntites.Model.Operators.AllOperatorModel> operators = new List<DataEntites.Model.Operators.AllOperatorModel>();
            var _pageNo = new SqlParameter("PageNumber", SqlDbType.Int) { Value = pageNo };
            var _pageSize = new SqlParameter("RowOfPage", SqlDbType.Int) { Value = pageSize };

            var result = SpRepository<DataEntites.Model.Admin.User.Users>.GetListWithStoreProcedure(@"exec GetAllOperators @PageNumber,@RowOfPage", _pageNo, _pageSize);
            if (result.Any())
            {
                foreach (var item in result)
                {
                    DataEntites.Model.Operators.AllOperatorModel opt = new DataEntites.Model.Operators.AllOperatorModel();
                    opt.BasicInfo = item;
                    ReviewsCount reviewsCount = new ReviewsCount();
                    var OperatorIdReviews = new SqlParameter("OperatorId", SqlDbType.BigInt) { Value = item.Id };
                    var OperatorIdtt = new SqlParameter("OperatorId", SqlDbType.BigInt) { Value = item.Id };

                    reviewsCount = SpRepository<ReviewsCount>.GetSingleObjectWithStoreProcedure(@"exec TotalReviewsAndAvgStarts @OperatorId", OperatorIdReviews);
                    opt.AvgStars = reviewsCount.AvgStars;
                    opt.TotalRevews = reviewsCount.TotalReviews;

                    var totaltrips = SpRepository<ReviewsCount>.GetSingleObjectWithStoreProcedure(@"exec TotalOperatorTrips @OperatorId", OperatorIdtt);
                    opt.TotalTrips = totaltrips.TotalReviews;



                    var opratorId = new SqlParameter("OperatorId", SqlDbType.BigInt) { Value = item.Id };
                    var _statusId = new SqlParameter("StatusId", SqlDbType.Int) { Value = 0 };

                    var operatorTrips = SpRepository<OperatorTripStatus>.GetListWithStoreProcedure(@"exec Get_Operators_Trips_BY_Operator_Id @OperatorId,@StatusId", opratorId, _statusId);

                    opt.ScheduleTrips = getTripByOptId(item.Id,0);

                    operators.Add(opt);

                }
                return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Users Founds", true, operators,
                    result[0].PageSize, result[0].PageNo, result[0].TotalPage);
            }
            else
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Users Founds", false);
            }
        }

    }


    public class NonVarifiedUsers
    {
        public long UserId { get; set; }
        public bool IsUserTypeVarified { get; set; }
    }

}