﻿using DataEntites.Model.User;
using DataEntites.Model.User.Firebase;
using DataEntites.Model.User.OperatorInfo;

namespace Buisness.Core.User
{
    public interface IUser
    {
        object GetUserById(double Id);

        UserModel GetUserByEmail(string Email);

        UserModel GetUserByEmailAndPassword(string Email, string Password);

        object UserRegistraion(UserModel user);

        object UserUpdate(UserModel user);

        object UpdateOptLiecence(LicenceNo userlicence, long Uid);
        object UpdateUserProfileImage(string ImageUrl, long Id);

        object ChangePassword(string CurrentPassword, string NewPassword, long Id);

        object DeActiveAccount(long Id);

        object GetTripOperatorInfoById(long Id);

        object AddSocialLinks(UsersSocialLinks operatorSocialLinks);

        object GetComplaintByUserId(long Id,int pageNo,int pageSize);

        object UpdateToken(FirebaseToken firebaseToken, long UId);

        object GetAllUsers();
        object GetAllOperators(int pageNo,int pageSize);
        
        object GetAllOperatorsReviews(int pageNo, int pageSize);

        object GetAllUsersByIdForAdmin(long Id);

        bool UpdateUserType(long Id, int roleId);

        DbContext.Role GetRoleById(int Id);

        object UpdateAdminUser(UserModel userModel);

        object GetComplaint();

        object UpdateComplaintStatus(long Id, bool status, string type, long userID);

        object UpdateTripReviewsAdmin(DataEntites.Model.Trip.TripReview tripReview);

        object GetAllCounts();

        object UpdateStatus(long Id, bool Status);


        object AddTripToFavt(long user_id, long trip_id);
        object DelFavtTrip(long user_id, long trip_id);


        object UpdateLicenseStatus(long modifyBy, long Id, bool Status);

        #region Admin
        object GetUsersPackageSubscription(bool status);
        object ChangePaymentStatus(long Id, bool status);
        object GetUsersBoostPackageSubscription(bool status);
        object ChangeBoostPackagePaymentStatus(long Id, bool status);
        #endregion
    }
}