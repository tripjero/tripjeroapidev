﻿using Buisness.Core.DbContext;
using DataEntites.GeneraicRepositroy;
using DataEntites.Model.Admin.Trip;
using DataEntites.Model.AutoTask;
using DataEntites.Model.OperatorPackages;
using DataEntites.Model.Trip;
using DataEntites.Model.Trip.EmailModal;
using DataEntites.Model.Trip.TripReviewModel;
using DataEntites.Model.Trip.TripSearch;
using DataEntites.Model.TripBooking;
using System;

using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using TripJero.Notification.Services;
using Utilties.Genral;

namespace Buisness.Core.Trip
{
    public class TripService : ITrip
    {
        private delegate void AddView(long UId, long TId);
        private TripJeroEntities db = new TripJeroEntities();

        public object ActiveDActiveTip(int id, bool Status)
        {
            var trip = db.Trips.Find(id);
            if (trip != null)
            {
                trip.IsActive = Status;
                trip.ActiveBy = 1;
                db.SaveChanges();
                return new { Status = true, Message = "Trip status updated" };
            }
            else
            {
                return new { Status = false, Message = "Trip not found, status not updated" };
            }
        }

        public object ActiveDActiveTripReviewById(int id, bool Status)
        {
            var tripReview = db.TripReviews.Find(id);
            if (tripReview != null)
            {
                tripReview.isActive = Status;
                db.SaveChanges();
                return new { Status = true, Message = "Trip Review status updated" };
            }
            else
            {
                return new { Status = false, Message = "Trip Review not found, status not updated" };
            }
        }

        public object ActiveDeActiveTripImage(int Id, bool Status)
        {
            var tripImage = db.TripImages.Find(Id);
            if (tripImage != null)
            {
                tripImage.IsActive = Status;
                db.SaveChanges();
                return new { Status = true, Message = "Trip images status updated" };
            }
            else
            {
                return new { Status = false, Message = "Trip images not found, status not updated" };
            }
        }

        public object Addtrip(TripModel tripModel, long UserId, string callFrom = "")
        {
          
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    #region Boost Validation 
                    if (tripModel.IsBoost.HasValue && (bool)tripModel.IsBoost)
                    {
                        if (tripModel.Boost_Code == 0)
                        {
                            return ObjectSerilization.GetSerilizedWithoutObject("Boost Code Required", false);
                        }
                        //if (tripModel.Boost_Code.Split('-').Length < 1)
                        //{
                        //    return ObjectSerilization.GetSerilizedWithoutObject("Boost Code is not vaild", false);
                        //}
                       // var boostCode = Convert.ToInt32(tripModel.Boost_Code.Split('-')[1]);
                        var boostAvaible = db.BoostPackageSubscriptions.FirstOrDefault(x => x.Id == tripModel.Boost_Code && x.UId == UserId && x.IS_Active == true );
                        if (boostAvaible == null)
                        {
                            return ObjectSerilization.GetSerilizedWithoutObject("Boost Code is not vaild", false);
                        }
                        if (boostAvaible.RemningBoostTrips == 0 || boostAvaible.RemningBoostTrips < 0)
                        {
                            return ObjectSerilization.GetSerilizedWithoutObject("You cannot use this boost code any more.Boost limit Reached", false);
                        }
                        boostAvaible.RemningBoostTrips = boostAvaible.RemningBoostTrips - 1;
                       
                        if (boostAvaible.RemningBoostTrips == 0)
                        {
                            boostAvaible.IS_Active = false;
                        }
                        db.SaveChanges();
                    }
                    #endregion

                    #region PackgeSubscription Check Limitaion
                    var _userId = new SqlParameter("@UserId", SqlDbType.BigInt) { Value = UserId };
                    var isAllow = SpRepository<AllowToAddTrip>.GetSingleObjectWithStoreProcedure(@"exec Check_Limitaion_Of_Trip_Posting_BY_UserId @UserId", _userId);
                    if (!isAllow.IsAllow)
                    {
                        return ObjectSerilization.GetSerilizedWithoutObject("Your today limit of trip posting has been exceeded", false);
                    }
                    #endregion

                    DbContext.Trip trip = new DbContext.Trip();
                    trip.Title = tripModel.TripBasicInfo.Title;
                    trip.Details = tripModel.TripBasicInfo.Details;
                    trip.FromVistId = tripModel.TripBasicInfo.FromVistId;
                    trip.ToVistId = tripModel.TripBasicInfo.ToVistId;
                    trip.StartDateTime = tripModel.TripBasicInfo.StartDateTime;
                    trip.EndDateTime = tripModel.TripBasicInfo.EndDateTime;
                    trip.Duration = tripModel.TripBasicInfo.Duration;
                    trip.Amount = tripModel.tripPrices.ActualAmount;
                    trip.UId = UserId;
                    trip.CreatedDate = DateTime.Now;
                    trip.ModifyById = UserId;
                    trip.ModifyDate = DateTime.Now;
                    trip.FeaturesImage = tripModel.TripBasicInfo.FeaturesImage;
                    trip.IsActive = true;
                    trip.IsDelete = false;
                    trip.Is_Boost = tripModel.IsBoost.HasValue ? tripModel.IsBoost : false;
                    if (tripModel.TripBasicInfo.CategoriesId > 0)
                    {
                        trip.CategoriesId = tripModel.TripBasicInfo.CategoriesId;
                    }
                    else
                    {
                        return ObjectSerilization.GetSerilizedWithoutObject("Trip Categories Required", false);
                    }
                   
                    if (tripModel.tripPrices.DiscountPercentage > 0)
                    {
                        trip.DiscountPercentage = tripModel.tripPrices.DiscountPercentage;
                    }
                    
                    if (tripModel.tripPrices.HavePromo && !string.IsNullOrEmpty(tripModel.tripPrices.PromoCode))
                    {
                        var existingPromoCode = db.PromoTbls.FirstOrDefault(x=>x.Promo_Key == tripModel.tripPrices.PromoCode && x.UId == UserId);
                        if (existingPromoCode!=null)
                        {
                            if (existingPromoCode.EndDate > DateTime.Now)
                            {
                                trip.IS_PROMO_ACTIVE = true;
                                trip.PROMO_ID = existingPromoCode.Id;

                            }else
                            {
                                return ObjectSerilization.GetSerilizedWithoutObject("Selected Promo Code not valid any more", false);
                            }
                           
                        }else
                        {
                            return ObjectSerilization.GetSerilizedWithoutObject("Selected Promo Code not exist", false);
                        }
                       
                    }
                    if (tripModel.TripBasicInfo.BaseStringFeaturesImages != null && callFrom == "webiste")
                    {
                        trip.FeaturesImage = Utilties.Genral.DocumentManager.SaveFile(tripModel.TripBasicInfo.BaseStringFeaturesImages.Replace("data:image/jpeg;base64,",""), "~/Trip/TripImage/", ".jpg");
                    }
                    db.Trips.Add(trip);
                    db.SaveChanges();
                    if (tripModel.IternaryPlan != null && tripModel.IternaryPlan.Count > 0)
                    {
                        foreach (var item in tripModel.IternaryPlan)
                        {
                            if (item.DayPlan != null && item.DayPlan.Count > 0)
                            {
                            }
                            DbContext.Iternaryplan iternaryplan = new Iternaryplan();
                            iternaryplan.TripId = trip.Id;
                            iternaryplan.DyaNumber = item.DayName;
                            db.Iternaryplans.Add(iternaryplan);
                            db.SaveChanges();
                            if (item.DayPlan != null && item.DayPlan.Count > 0)
                            {
                                foreach (var day in item.DayPlan)
                                {
                                    DbContext.DayPlan dayPlan = new DbContext.DayPlan();
                                    dayPlan.DyId = iternaryplan.Id;
                                    dayPlan.TaskDetails = day.TaskDetaisl;
                                    db.DayPlans.Add(dayPlan);
                                }
                            }
                        }
                    }
                    if (tripModel.TripImages != null && tripModel.TripImages.Count > 0)
                    {
                        foreach (var item in tripModel.TripImages)
                        {
                            DbContext.TripImage tripImage = new TripImage();
                            tripImage.TripId = trip.Id;
                            tripImage.ImageUrl =  callFrom == "webiste" ? Utilties.Genral.DocumentManager.SaveFile(item.ImageUrl.Replace("data:image/jpeg;base64,", ""), "~/Trip/TripImage/" + tripImage.TripId + "/", ".jpg"): item.ImageUrl;
                            tripImage.ModifyDate = DateTime.Now;
                            tripImage.MofifyBy = UserId;
                            tripImage.IsActive = true;
                            tripImage.CreatedDate = DateTime.Now;
                            db.TripImages.Add(tripImage);
                        }
                    }
                    if (tripModel.Faclities != null)
                    {
                        DbContext.Facilty facilty = new Facilty();
                        facilty.Accommodation = tripModel.Faclities.Accommodiation;
                        facilty.Transportation = tripModel.Faclities.Transportation;
                        facilty.TripId = trip.Id;
                        facilty.FacalitiesList = tripModel.Faclities.Facalities;

                        facilty.HaveTransport = tripModel.Faclities.HaveTransport;
                        facilty.HaveAccomodation = tripModel.Faclities.HaveAccomodation;
                        facilty.HaveMeals = tripModel.Faclities.HaveMeals;
                        facilty.HaveFirstAid = tripModel.Faclities.HaveFirstAid;
                       
                        facilty.Icon_Accomodation = "";
                        facilty.Icon_Meals = "";
                        facilty.Icon_FirstAid = "";
                        facilty.Icon_Transport = "";
                        db.Facilties.Add(facilty);
                    }
                    DbContext.TripCurrentStatu tripCurrentStatu = new TripCurrentStatu();
                    tripCurrentStatu.CreatedDate = DateTime.Now;
                    tripCurrentStatu.ModifyDate = DateTime.Now;
                    tripCurrentStatu.TripId = trip.Id;
                    tripCurrentStatu.IsAcive = true;
                    tripCurrentStatu.TripStatusId = 3;
                    db.TripCurrentStatus.Add(tripCurrentStatu);
                    db.SaveChanges();
                    transaction.Commit();
                    var tripUser = db.Users.Find(UserId);
                    tripModel.TripBasicInfo.Id = trip.Id;
                    var fromPlacename = db.VisitPlaces.Find(trip.FromVistId);
                    var toplaceName = db.VisitPlaces.Find(trip.ToVistId);

                    tripModel.TripBasicInfo.FromPlaceName = fromPlacename.City.Name;
                    tripModel.TripBasicInfo.ToPlaceName = toplaceName.City.Name;


                    string EmailString = Utilties.Genral.EmailTemplates.TripPostEmail(tripModel,tripUser.FullName);
                    EmailSender.SendEmail(tripUser.Email, "Trip posted successfully", EmailString);


                    //string stringHtmlBody = NewTripEmailHtml(trip.Id.ToString(), tripUser.FullName, trip.Title, trip.StartDateTime.ToLongDateString(), trip.EndDateTime.ToLongDateString(), trip.Duration + " days");
                    //EmailSender.SendEmail(tripUser.Email, "New Trip Conformation", stringHtmlBody);
                    //return ObjectSerilization.GetSerilizedWithoutObject("Trip posted successfully", true , trip.Id);
                    return ObjectSerilization.GetSerilizedTripPostObject("Trip posted successfully", true, trip.Id);

                }
                catch (Exception ex)
                {   
                    transaction.Rollback();
                    return ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false);
                }
            }
        }

        public object UpdateTrip(TripModel tripModel)
        {
            var tripUpdate = db.Trips.Find(tripModel.TripBasicInfo.Id);
            if (tripUpdate != null)
            {
                //try
                //{
                //    tripUpdate.Title = tripModel.TripBasicInfo.Title;
                //    tripUpdate.Details = tripModel.TripBasicInfo.Details;
                //    tripUpdate.Duration = tripModel.TripBasicInfo.Duration;
                //    tripUpdate.CreatedDate = tripModel.TripBasicInfo.CreatedDate;
                //    tripUpdate.ModifyDate = DateTime.Now;
                //    tripUpdate.StartDateTime = tripModel.StartDateTime;
                //    tripUpdate.EndDateTime = tripModel.EndDateTime;
                //    tripUpdate.FeaturesImage = DocumentManager.SaveFile(tripModel.FeaturesImage, "~/Trip/TripFeatures/" + tripModel.UId + "/", ".jpg"); ;
                //    //tripUpdate.FromCityId = tripModel.FromCityId;
                //    //tripUpdate.ToCityId = tripModel.ToCityId;
                //    tripUpdate.Amount = tripModel.Amount;
                //    db.SaveChanges();
                //    return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Trip Updated", true, tripModel);
                //}
                //catch (Exception)
                //{
                //return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Trip Updated", false); ;
                // }
            }
            return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Trip found of given Id of" + tripModel.TripBasicInfo.Id, true);
        }

        public object AddTripImage(TripImages tripImages)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    DbContext.TripImage tripImage = new TripImage();
                    tripImage.IsActive = true;
                    tripImage.CreatedDate = DateTime.Now;
                    tripImage.ModifyDate = DateTime.Now;
                    tripImage.TripId = tripImages.TripId;
                    tripImage.ImageUrl = Utilties.Genral.DocumentManager.SaveFile(tripImages.ImageUrl, "~/Trip/TripImage/" + tripImage.TripId + "/", ".jpg");
                    db.TripImages.Add(tripImage);
                    db.SaveChanges();
                    transaction.Commit();
                    return new { Status = true, Message = "New trip image has been created ", TripImageId = tripImage.Id };
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return new { Status = true, Message = ex.Message };
                }
            }
        }

        public object DeleteTripImage(int Id)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var tripImage = db.TripImages.Find(Id);
                    Utilties.Genral.DocumentManager.DeleteFile(tripImage.ImageUrl);
                    db.TripImages.Remove(tripImage);
                    db.SaveChanges();
                    transaction.Commit();
                    return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Trip Image Deleted", true);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false);
                }
            }
        }

        public object DeleteTripReviewById(int id)
        {
            DbContext.TripReview tripReview = db.TripReviews.Find(id);
            if (tripReview != null)
            {
                db.TripReviews.Remove(tripReview);
                db.SaveChanges();
                return new { Status = true, Message = "Trip review deleted" };
            }
            else
            {
                return new { Status = false, Message = "Trip images not found, status not updated" };
            }
        }

        public object GetTripById(long Id, long ViewId , int userroleId, string callfrom = "")
        {
            var _id = new SqlParameter("SearchId", SqlDbType.BigInt) { Value = Id };
            var _type = new SqlParameter("Type", SqlDbType.VarChar) { Value = "T" }; // by tips Id
            var trip = SpRepository<TripBasicInfo>.GetSingleObjectWithStoreProcedure(@"exec Get_Trip_By_User_Id_OR_STAUS_ID @SearchId,@Type", _id, _type);
           
            if (trip != null)
            {
                if (ViewId > 0)
                {
                        AddView addViewEvent = new AddView(AddTripView);
                        addViewEvent.BeginInvoke(ViewId, Id, null, null);
                }
                if (callfrom=="mobile")
                {
                    var _ToId = new SqlParameter("ToId", SqlDbType.Int) { Value = trip.ToVistId }; 
                    var  _fromId = new SqlParameter("FromId", SqlDbType.Int) { Value = trip.FromVistId};
                    var tripsimilar = SpRepository<SimilarTripList>.GetListWithStoreProcedure(@"exec Get_Similar_Trips @ToId,@FromId", _ToId, _fromId);
                    SimilerTripModel similerTripModels = new SimilerTripModel();
                    similerTripModels.SingleTrip = getGenaricTrip(trip, true,userroleId,ViewId);
                    foreach (var item in tripsimilar)
                    {
                        similerTripModels.SimilartripsList.Add(item);
                    }
                    return ObjectSerilization.GetSerilizedObject("Trip Found With Similar Data", true, similerTripModels);
                }
                if (userroleId == 3)
                {
                    return ObjectSerilization.GetSerilizedObject("Trip Found", true, getGenaricTrip(trip, true,userroleId,0));
                }

                //var similarTrips = Get_Similar_Trips
                return ObjectSerilization.GetSerilizedObject("Trip Found", true, getGenaricTrip(trip, true,userroleId,0));
            }
            else
            {
                return ObjectSerilization.GetSerilizedWithoutObject("No Trip Found", false);
            }
        }

        public object GetTripByUserId(long Id)
        {
            var _id = new SqlParameter("UserId", SqlDbType.Int) { Value = Id };
            var tripList = SpRepository<TripBasicInfo>.GetListWithStoreProcedure(@"exec Get_Trip_By_User_Id @UserId", _id);
            if (tripList.Count > 0)
            {
                List<TripModel> tripModelsList = new List<TripModel>();
                foreach (var item in tripList)
                {
                    tripModelsList.Add(getGenaricTrip(item, false,3,0));
                }
              
                return ObjectSerilization.GetSerilizedObject("Trip Found", true, tripModelsList);
            }
            else
            {
                return ObjectSerilization.GetSerilizedWithoutObject("No Trip Found", false);
            }
        }
        public object GetAllTripByUserId(long Id)
        {
            var _id = new SqlParameter("UserId", SqlDbType.Int) { Value = Id };
            var tripList = SpRepository<TripBasicInfo>.GetListWithStoreProcedure(@"exec Get_All_Trip_By_User @UserId", _id);
            if (tripList.Count > 0)
            {
                List<TripModel> tripModelsList = new List<TripModel>();
                TotalTripsModal totalTripsModalsList = new TotalTripsModal();
                foreach (var item in tripList)
                {
                    tripModelsList.Add(getGenaricTrip(item, false,3,0));
                }
                if (tripModelsList.Any())
                {
                    var groupedTrips = tripModelsList.GroupBy(x => x.TripStatus);
                    totalTripsModalsList.CompleteTrips = groupedTrips.FirstOrDefault(x => x.Key == "Complete") !=null ? groupedTrips.FirstOrDefault(x => x.Key == "Complete").ToList(): null;
                    totalTripsModalsList.InProgressTrips = groupedTrips.FirstOrDefault(x => x.Key == "In Progress") != null ? groupedTrips.FirstOrDefault(x => x.Key == "In Progress").ToList():null;
                    totalTripsModalsList.Schedule = groupedTrips.FirstOrDefault(x => x.Key == "Schedule") != null ? groupedTrips.FirstOrDefault(x => x.Key == "Schedule").ToList():null;
                    totalTripsModalsList.Hold = groupedTrips.FirstOrDefault(x => x.Key == "Hold") != null ? groupedTrips.FirstOrDefault(x => x.Key == "Hold").ToList():null;
                    totalTripsModalsList.Cancel = groupedTrips.FirstOrDefault(x => x.Key == "Cancel") != null ? groupedTrips.FirstOrDefault(x => x.Key == "Cancel").ToList():null;
                    
                }
                var _idForReviews = new SqlParameter("UserId", SqlDbType.Int) { Value = Id };
                var totalRevirwsTrips = SpRepository<TripBasicInfo>.GetListWithStoreProcedure(@"exec Get_Trip_By_User_Id_With_Reviews @UserId", _idForReviews);
                if (totalRevirwsTrips.Any())
                {
                    foreach (var item in totalRevirwsTrips)
                    {
                        totalTripsModalsList.TopReviews.Add(getGenaricTrip(item, false,3,0));
                    }
                }
                return ObjectSerilization.GetSerilizedObject("Trip Found", true, totalTripsModalsList);
            }
            else
            {
                return ObjectSerilization.GetSerilizedWithoutObject("No Trip Found", false);
            }
        }

        public object GetTripImagesByTripId(long Id)
        {
            var tripImage = db.Get_TripImage_By_TripId(Id).ToList();
            if (tripImage.Count > 0)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Trip Images Found", true, tripImage);
            }
            else
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Image found", false);
            }
        }

        public object GetTripReviewByTripId(long Id)
        {
            var TId = new SqlParameter("TripId", SqlDbType.BigInt) { Value = Id };
            var tripReviewsList = SpRepository<OperatorTripReviews>.GetListWithStoreProcedure(@"exec Get_Review_By_Trip_Id @TripId", TId);
            if (tripReviewsList.Count > 0)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Reviews Founds", true, tripReviewsList);
            }
            else
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Reviews Founds", false);
            }
        }

        public object UpdateTripImage(TripImages tripImages)
        {
            throw new NotImplementedException();
        }

        public object GetScheduleTripViews(long Id)
        {
            

            List<TripViewsModel> views = new List<TripViewsModel>();


            var _id = new SqlParameter("UserId", SqlDbType.Int) { Value = Id };
            var tripList = SpRepository<TripBasicInfo>.GetListWithStoreProcedure(@"exec Get_Trip_By_User_Id @UserId", _id);
            if (tripList.Count > 0)
            {
                List<TripModel> tripModelsList = new List<TripModel>();
                foreach (var item in tripList)
                {
                    TripViewsModel v = new TripViewsModel();
                    v.TId = item.Id ?? 0;
                    v.TripTitle = item.Title;
                    v.TripStatus = item.TripStatus;
                    var vvvs =  db.TripViews.Where(x => x.TId == item.Id).ToList();

                    foreach (var ch in vvvs)
                    {
                        v.UId = ch.UId;
                        v.TravName = ch.User.FullName;
                        v.Dtm = ch.CreatedDate ?? DateTime.Now;
                        views.Add(v);
                    }
                }

                return ObjectSerilization.GetSerilizedObject("Trip Found", true, views);
            }
            else
            {

                return ObjectSerilization.GetSerilizedObject("No Trip Found", true, views);

            }         

        }
        public object UpdateTripReview(DataEntites.Model.Trip.TripReview tripReview)
        {
            var tripReviewUpdate = db.TripReviews.Find(tripReview.Id);
            if (tripReviewUpdate != null)
            {
                tripReviewUpdate.Reviews = tripReview.Reviews;
                db.SaveChanges();
                return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Trip Reviews Updated", true, tripReview);
            }
            else
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Reviews found of give Id", false);
            }
        }

        public object UpdateTripComment(DataEntites.Model.Trip.TripReview tripReview)
        {
            var tripReviewUpdate = db.TripReviews.Find(tripReview.Id);
            if (tripReviewUpdate != null)
            {
                tripReviewUpdate.Status = tripReview.Status;
                tripReviewUpdate.Comment = tripReview.Comment;
                db.SaveChanges();
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Trip Review Updated", true);
            }
            else
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Reviews found of give Id", false);
            }
        }

        
        
        #region Trip

        private TripModel getGenaricTrip(TripBasicInfo tripBasicInfo, bool isDetials,int roleId,long logginuserId)
        {
            TripModel tripModel = new TripModel();
            if (tripBasicInfo != null)
            {
                tripModel.TripBasicInfo = tripBasicInfo;
                var OperatorId = new SqlParameter("OperatorId", SqlDbType.BigInt) { Value = tripBasicInfo.UId };
                var OperatorIdReviews = new SqlParameter("OperatorId", SqlDbType.BigInt) { Value = tripBasicInfo.UId };
                var tripId = new SqlParameter("TripId", SqlDbType.BigInt) { Value = tripBasicInfo.Id };
                List<OperatorsReviews> tripReviewsList = new List<OperatorsReviews>();
                ReviewsCount reviewsCount = new ReviewsCount();
                if (roleId > 0 && roleId == 3 )
                {
                    tripReviewsList =  SpRepository<DataEntites.Model.Trip.TripReviewModel.OperatorsReviews>.GetListWithStoreProcedure(@"exec Get_Operators_Review_By_TId @OperatorId,@TripId", OperatorId,tripId);
                }
                else
                {


                    reviewsCount = SpRepository<ReviewsCount>.GetSingleObjectWithStoreProcedure(@"exec TotalReviewsAndAvgStarts @OperatorId", OperatorIdReviews);
                    tripReviewsList = SpRepository<DataEntites.Model.Trip.TripReviewModel.OperatorsReviews>.GetListWithStoreProcedure(@"exec Get_Operators_Review @OperatorId", OperatorId);
                }
                if(logginuserId != 0)
                {
                    if (db.FavtTrips.Where(x => x.user_id == logginuserId && x.trip_id == tripBasicInfo.Id).FirstOrDefault() != null)
                    {
                        tripModel.IsMyFavt = true;
                    }
                    else
                    {
                        tripModel.IsMyFavt = false;
                    }
                }
                else
                {
                    tripModel.IsMyFavt = false;
                }
                if (tripReviewsList.Count > 0)
                {
                    tripModel.AvgStars = reviewsCount.AvgStars;
                    tripModel.TotalRevews = reviewsCount.TotalReviews;
                }
                #region trip Price
                //var tripId = new SqlParameter("TripId", SqlDbType.BigInt) { Value = tripBasicInfo.Id };
                //var tripPrices = SpRepository<DataEntites.Model.Trip.TripPrice.TripPrice>.GetSingleObjectWithStoreProcedure(@"exec Get_DiscountedTrip_Price @TripId", tripId);
                tripModel.TripStatus = tripBasicInfo.TripStatus;
                tripModel.IsBoost = tripBasicInfo.IsBoost;
                
                if (tripBasicInfo.DiscountPercentage != null && tripBasicInfo.DiscountPercentage > 0)
                {
                    tripModel.tripPrices.ActualAmount = tripBasicInfo.Amount;
                    tripModel.tripPrices.CurrentAmount =  (double)(tripBasicInfo.Amount * (100.0 - tripBasicInfo.DiscountPercentage) / 100.0);
                    tripModel.tripPrices.DiscountPercentage = (double)tripBasicInfo.DiscountPercentage;
                    tripModel.tripPrices.HavePromo = tripBasicInfo.Is_Promo;
                    tripModel.tripPrices.PromoCode = tripBasicInfo.PromoCode;
                    tripModel.tripPrices.trip_id = (long)tripBasicInfo.Id;
                    tripModel.tripPrices.user_id = tripBasicInfo.UId;
                    tripModel.tripPrices.PromoCode = tripBasicInfo.PromoCode;
                }
                else
                {
                    tripModel.tripPrices.ActualAmount = tripBasicInfo.Amount;
                    tripModel.tripPrices.CurrentAmount = (double)tripBasicInfo.Amount;
                    tripModel.tripPrices.DiscountPercentage = 0;
                    tripModel.tripPrices.PromoCode = tripBasicInfo.PromoCode;
                    tripModel.tripPrices.HavePromo = tripBasicInfo.Is_Promo;
                    tripModel.tripPrices.trip_id =(long)tripBasicInfo.Id;
                    tripModel.tripPrices.user_id = tripBasicInfo.UId;
                    tripModel.tripPrices.PromoCode = tripBasicInfo.PromoCode;
                }
                #endregion
                #region trip Faclities

                var faclitiesList = db.Facilties.FirstOrDefault(x => x.TripId == tripBasicInfo.Id);
                if (faclitiesList != null)
                {
                    tripModel.Faclities.Id = faclitiesList.Id;
                    tripModel.Faclities.Transportation = faclitiesList.Transportation != null ? faclitiesList.Transportation : "";
                    tripModel.Faclities.Facalities = faclitiesList.FacalitiesList != null ? faclitiesList.FacalitiesList : "";
                    tripModel.Faclities.Accommodiation = faclitiesList.Accommodation != null ? faclitiesList.Accommodation : "";

                    tripModel.Faclities.HaveTransport = faclitiesList.HaveTransport;
                    tripModel.Faclities.HaveAccomodation = faclitiesList.HaveAccomodation;
                    tripModel.Faclities.HaveMeals = faclitiesList.HaveMeals;
                    tripModel.Faclities.HaveFirstAid = faclitiesList.HaveFirstAid;
                    tripModel.Faclities.Icon_Accomodation = faclitiesList.Icon_Accomodation;
                    tripModel.Faclities.Icon_Meals = faclitiesList.Icon_Meals;
                    tripModel.Faclities.Icon_FirstAid = faclitiesList.Icon_FirstAid;
                    tripModel.Faclities.Icon_Transport = faclitiesList.Icon_Transport;
                }

                #endregion trip Facilities

                if (isDetials)
                {
                    if (tripReviewsList.Count() > 0)
                    {
                        tripModel.TripReviews = tripReviewsList;
                    }
                    #region tripImage

                    var TImageList = db.TripImages.Where(x => x.TripId == tripBasicInfo.Id);
                    if (TImageList.Any())
                    {
                        foreach (var item in TImageList)
                        {
                            DataEntites.Model.Trip.TripImages tripImages = new DataEntites.Model.Trip.TripImages();
                            tripImages.Id = item.Id;
                            tripImages.IsActive = item.IsActive;
                            tripImages.CreatedDate = item.CreatedDate;
                            tripImages.ImageUrl = item.ImageUrl;
                            tripImages.ModifyDate = item.ModifyDate;
                            tripImages.ModifybyId = item.MofifyBy;
                            tripModel.TripImages.Add(tripImages);
                        }
                    }

                    #endregion tripImage

             
                    #region Iternary

                    var ItrenaryPlan = db.Iternaryplans.Where(x => x.TripId == tripBasicInfo.Id).ToList();

                    if (ItrenaryPlan.Count > 0)
                    {
                        foreach (var item in ItrenaryPlan)
                        {
                            DataEntites.Model.Trip.IternaryPlan plan = new IternaryPlan();
                            plan.Id = item.Id;
                            plan.DayName = item.DyaNumber;

                            if (item.DayPlans.Count > 0)
                            {
                                foreach (var dayv in item.DayPlans)
                                {
                                    DataEntites.Model.Trip.DayPlan dayPlan = new DataEntites.Model.Trip.DayPlan();
                                    dayPlan.DayId = dayv.DyId;
                                    dayPlan.TaskDetaisl = dayv.TaskDetails;
                                    dayPlan.Id = dayv.Id;
                                    plan.DayPlan.Add(dayPlan);
                                }
                            }
                            tripModel.IternaryPlan.Add(plan);
                        }
                    }

                    #endregion Itinerary

                   
                }
            }
            return tripModel;
        }

        public object AddReview(DataEntites.Model.Trip.TripReview tripReview)
        {
            try
            {

                var rev = db.TripReviews.Where(x=>x.TId == tripReview.TId && x.UId == tripReview.UId).FirstOrDefault();
                if (rev != null)
                {
                    return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("You already rated this trip.", true);

                }
                else
                {
                    DbContext.TripReview tripR = new DbContext.TripReview();
                    tripR.isActive = false;
                    tripR.TId = tripReview.TId;
                    tripR.UId = tripReview.UId;
                    tripR.OperatorId = tripReview.OperatorId;
                    tripR.CreatedDate = DateTime.Now;
                    tripR.Reviews = tripReview.Reviews;
                    tripR.Status = true;
                    tripR.Stars = tripReview.Stars;
                    db.TripReviews.Add(tripR);
                    db.SaveChanges();
                    var user = db.Users.Find(tripReview.OperatorId);
                    if (user != null)
                    {
                        var trip = db.Trips.Find(tripReview.TId);
                        if (user.FirebaseToken != null && !string.IsNullOrEmpty(user.FirebaseToken))
                        {

                            NotificationSender.SendNotification(user.FirebaseToken, "You have a review of your trip " + trip.Title, "Trip Review");

                        }
                        var t = Task.Run(() => NotificationLog(user.Id, "You have a review of your trip " + trip.Title, "Trip Review"));
                        t.Wait();
                    }
                    return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Review Added", true);
                }
            }
            catch (Exception ex)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false);
            }
        }

        public object TempTripImageUpload(string imageString, string from, long Id)
        {
            var fileUrl = "";
            if (from == "web")
            {
                fileUrl = DocumentManager.SaveFile(imageString.Split(',')[1], "~/Trip/TripImage/" + Id + "/", ".jpg");

            }
            else
            {
                fileUrl = Utilties.Genral.DocumentManager.SaveFile(imageString, "~/Trip/TripImage/" + Id + "/", ".jpg");

            }
            return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(fileUrl.Replace("~", ""), true);
        }

        #region Add Temp Image List
        public object TempTripImageUpload(List<TripTempImagesList> imageString, long Id)
        {
            if (imageString.Any())
            {
                List<string> tripTempImagesLists = new List<string>();
                foreach (var item in imageString)
                {
                    var fileUrl = DocumentManager.SaveFile(item.ImageString.Split(',')[1], "~/Trip/TripImage/" + Id + "/", ".jpg");
                    tripTempImagesLists.Add(fileUrl.Replace("~", ""));
                }
                return ObjectSerilization.GetSerilizedObject("Temp Image resolved", true, tripTempImagesLists);
            }
            return ObjectSerilization.GetSerilizedWithoutObject("No image found in request", false);
        }
        #endregion Add Temp Image List

        public object DeleteTempTripImage(string imageString)
        {
            var fileUrl = Utilties.Genral.DocumentManager.DeleteFile(imageString);
            if (fileUrl)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Image deleted to server", true);
            }
            else
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Image not deleted to server or may be not exit in directory", false);
            }
        }
        public object GetAllStatusTrips(long Id, int pageNo, int pageSize)
        {
            var _status = new SqlParameter("StatusId", SqlDbType.Int) { Value = 0 };
            var _pageNo = new SqlParameter("PageNumber", SqlDbType.Int) { Value = pageNo };
            var _pageSize = new SqlParameter("RowOfPage", SqlDbType.Int) { Value = pageSize };
            var tripList = SpRepository<TripBasicInfo>.GetListWithStoreProcedure(@"exec Get_Trip_BY_StatusId @StatusId,@PageNumber,@RowOfPage", _status, _pageNo, _pageSize);
            if (tripList.Count > 0)
            {
                var boostTrips = tripList.Where(x => x.IsBoost == true);
                var noramlTrips = tripList.Where(x => x.IsBoost == false).ToList();
                List<TripModel> tripModelsList = new List<TripModel>();


                foreach (var item in noramlTrips)
                {
                    tripModelsList.Add(getGenaricTrip(item, false, 4, Id));
                }


                if (noramlTrips.Count() > 0)
                {
                    if (boostTrips.Any())
                    {
                        int indexToInsert = 1;
                        tripModelsList.Insert(indexToInsert, getGenaricTrip(boostTrips.First(), false, 4, Id));
                        foreach (var item in boostTrips)
                        {
                            indexToInsert += 3;
                            if (noramlTrips.Count() >= (indexToInsert - 1))
                            {
                                tripModelsList.Insert(indexToInsert, getGenaricTrip(item, false, 4, Id));
                            }
                            else
                            {
                                tripModelsList.Add(getGenaricTrip(item, false, 4, Id));
                            }
                        }
                    }
                }
                else
                {
                    if (boostTrips.Any())
                    {

                        foreach (var item in boostTrips)
                        {
                            tripModelsList.Add(getGenaricTrip(item, false, 4, Id));
                        }
                    }
                }
                return ObjectSerilization.GetSerilizedObject("Trips Found", true, tripModelsList, tripList[0].PageSize, tripList[0].PageNo, tripList[0].TotalPage);
            }
            else
            {
                return ObjectSerilization.GetSerilizedWithoutObject("No Trip Found", false);
            }
        }
        public object GetAllTrips(long loggInId, int pageNo, int pageSize)
        {
            
            var _status = new SqlParameter("StatusId", SqlDbType.Int) { Value = 3 };
            var _pageNo = new SqlParameter("PageNumber", SqlDbType.Int) { Value = pageNo };
            var _pageSize = new SqlParameter("RowOfPage", SqlDbType.Int) { Value = pageSize };
            var tripList = SpRepository<TripBasicInfo>.GetListWithStoreProcedure(@"exec Get_Trip_BY_StatusId @StatusId,@PageNumber,@RowOfPage", _status,_pageNo,_pageSize);
            if (tripList.Count > 0)
            {
                var boostTrips = tripList.Where(x => x.IsBoost == true);
                var noramlTrips = tripList.Where(x => x.IsBoost == false).ToList();
                List<TripModel> tripModelsList = new List<TripModel>();


                foreach (var item in noramlTrips)
                {
                    tripModelsList.Add(getGenaricTrip(item, false, 4, loggInId));
                }


                if (noramlTrips.Count() > 0)
                {
                    if (boostTrips.Any())
                    {
                        int indexToInsert = 1;
                        tripModelsList.Insert(indexToInsert, getGenaricTrip(boostTrips.First(), false, 4, loggInId));
                        foreach (var item in boostTrips)
                        {
                            indexToInsert += 3;
                            if (noramlTrips.Count() >= (indexToInsert - 1))
                            {
                                tripModelsList.Insert(indexToInsert, getGenaricTrip(item, false, 4, loggInId));
                            }
                            else
                            {
                                tripModelsList.Add(getGenaricTrip(item, false, 4, loggInId));
                            }
                        }
                    }
                }
                else
                {
                    if (boostTrips.Any())
                    {

                        foreach (var item in boostTrips)
                        {
                            tripModelsList.Add(getGenaricTrip(item, false, 4, loggInId));
                        }
                    }
                }
                return ObjectSerilization.GetSerilizedObject("Trips Found", true, tripModelsList, tripList[0].PageSize, tripList[0].PageNo, tripList[0].TotalPage);
            }
            else
            {
                return ObjectSerilization.GetSerilizedWithoutObject("No Trip Found", false);
            }
        }

        public object GetAllBoostTrips(long loggedInId)
        {
            var _status = new SqlParameter("StatusId", SqlDbType.Int) { Value = 3 };
            var tripList = SpRepository<TripBasicInfo>.GetListWithStoreProcedure(@"exec Get_BOOST_Trip_BY_StatusId @StatusId", _status);
            if (tripList.Count > 0)
            {
                List<TripModel> tripModelsList = new List<TripModel>();
                foreach (var item in tripList)
                {
                    tripModelsList.Add(getGenaricTrip(item, false,4,loggedInId));
                }
                return ObjectSerilization.GetSerilizedObject("Trips Found", true, tripModelsList);
            }
            else
            {
                return ObjectSerilization.GetSerilizedWithoutObject("No Trip Found", false);
            }
        }



        public object GetFavtTrips(long user_id, int pageNo, int pageSize)
        {
            var _TrevlerId = new SqlParameter("TrevlerId", SqlDbType.BigInt) { Value = user_id };
            var _pageNo = new SqlParameter("PageNumber", SqlDbType.Int) { Value = pageNo };
            var _pageSize = new SqlParameter("RowOfPage", SqlDbType.Int) { Value = pageSize };
            var tripList = SpRepository<TripBasicInfo>.GetListWithStoreProcedure(@"exec GetFavtTrips @TrevlerId,@PageNumber,@RowOfPage", _TrevlerId,_pageNo,_pageSize);
            if (tripList.Count > 0)
            {
                List<TripModel> tripModelsList = new List<TripModel>();
                foreach (var item in tripList)
                {
                    tripModelsList.Add(getGenaricTrip(item, false,4,0));
                }
                return ObjectSerilization.GetSerilizedObject("Trips Found", true, tripModelsList,
                    tripList[0].PageSize,
                    tripList[0].PageNo, tripList[0].TotalPage);
            }
            else
            {
                return ObjectSerilization.GetSerilizedWithoutObject("No Trip Found", false);
            }
        }

        public object UpdateTripStatus(long opratorId, TripStatus tripStatus, string callFrom = "")
        {
            try
            {
                var tripId = new SqlParameter("TripId", SqlDbType.Int) { Value = tripStatus.TripId };
                var tripcurrentStatus = SpRepository<CurrentTripStatus>.GetSingleObjectWithStoreProcedure(@"exec GetTripCurrentStatusValue @TripId", tripId);
                //var lastStatusList = db.TripCurrentStatus.FirstOrDefault(x => x.TripId == tripStatus.TripId && x.IsAcive == true);
                if (string.IsNullOrEmpty(callFrom))
                {
                    if ((tripcurrentStatus.StartDateTime - DateTime.Now).TotalDays <= 2)
                    {
                        return ObjectSerilization.GetSerilizedWithoutObject("You cannot update status of this trip", false);
                    }
                    if (tripStatus.StatusId == 1 && tripStatus.StatusId == 2)
                    {
                        return ObjectSerilization.GetSerilizedWithoutObject("You cannot update status of this trip", false);
                    }
                }

             
                TripCurrentStatu tripCurrent = new TripCurrentStatu();
                tripCurrent.CreatedDate = DateTime.Now;
                tripCurrent.ModifyDate = DateTime.Now;
                tripCurrent.TripId = tripStatus.TripId;
                tripCurrent.TripStatusId = tripStatus.StatusId;
                tripCurrent.IsAcive = true;
                db.TripCurrentStatus.Add(tripCurrent);
             
                using (var transection = db.Database.BeginTransaction())
                {
                    try
                    {
                        var update = db.UpdateAllTripCurrentStatusToFalse(tripStatus.TripId);
                        db.SaveChanges();
                        transection.Commit();
                    }
                    catch (Exception)
                    {
                        transection.Rollback();
                    }
                }

             
                var bookedTripUsers = db.TripBookings.Where(x => x.TId == tripStatus.TripId).ToList();
                if (tripStatus.StatusId == 5)
                {
                    foreach (var item in bookedTripUsers)
                    {
                        if (item.User.FirebaseToken != null && !string.IsNullOrEmpty(item.User.FirebaseToken))
                        {
                            var notficationList = db.NotificationTrayTbls.FirstOrDefault(x => x.Title == "TripCancelled");
                            if (notficationList != null)
                            {
                                NotificationSender.SendNotification(item.User.FirebaseToken, notficationList.NotificationMessage, notficationList.Title);
                            }
                            var t = Task.Run(() => NotificationLog(item.User.Id, notficationList.NotificationMessage, notficationList.Title));
                            t.Wait();
                        }
                        string emailBody = SendTripCancelledEmailHtml(item.User.FullName, item.Trip.User.FullName, item.Trip.Title, item.Id.ToString(), "Cancelled", item.Trip.StartDateTime.ToLongDateString(), item.Trip.Duration + " days", item.CreatedDate.ToLongDateString());
                        EmailSender.SendEmail(item.User.Email, "Trip Cancelled", emailBody);
                    }
                }

                return ObjectSerilization.GetSerilizedWithoutObject("Status Updated", true);
            }
            catch (Exception ex)
            {
                return ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false);
            }
        }

        public object TripCommpersion(TripComparison tripComparison)
        {
            var FromId = new SqlParameter("FromId", SqlDbType.BigInt) { Value = tripComparison.FromId };
            var ToId = new SqlParameter("ToId", SqlDbType.BigInt) { Value = tripComparison.ToId };
            var tripFaclities = SpRepository<TripComparison>.GetListWithStoreProcedure(@"exec Get_TrimpCompression_From_To @FromId,@ToId ", FromId, ToId);
            if (tripFaclities.Count > 0)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Trip Founds", true, tripFaclities);
            }
            else
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Trip Founds", false);
            }
        }

        public object TripFiltraion(TripFilter tripFilter,long loggedInId)
        {
            var FromId = new SqlParameter("FromId", SqlDbType.Int) { Value = tripFilter.FromId };
            var ToId = new SqlParameter("ToId", SqlDbType.Int) { Value = tripFilter.ToId };
            var StartDate = new SqlParameter("StartDate", SqlDbType.Date) { Value = tripFilter.StartDate };

            if (tripFilter.StartDate < DateTime.Now)
            {
                StartDate = new SqlParameter("StartDate", SqlDbType.Date) { Value = DateTime.Now };

            }
            //var Accommodation = new SqlParameter("Accommodation", SqlDbType.VarChar) { Value = tripFilter.Accommodation };
            //var Transportation = new SqlParameter("Transportation", SqlDbType.VarChar) { Value = tripFilter.Transportation };
            var Title = new SqlParameter("Title", SqlDbType.VarChar) { Value = tripFilter.Title != null ? tripFilter.Title : "" };
            var MinAmount = new SqlParameter("MinAmount", SqlDbType.Float) { Value = tripFilter.MinAmount };
            var MaxAmount = new SqlParameter("MaxAmount", SqlDbType.Float) { Value = tripFilter.MaxAmount };

        
            var tripList = SpRepository<TripBasicInfo>.GetListWithStoreProcedure(@"exec Get_Filterd_Trip @FromId,@ToId,@StartDate,@MaxAmount,@MinAmount,@Title", FromId, ToId, StartDate, MaxAmount, MinAmount, Title);
            if (tripList.Count > 0)
            {

               
                    List<TripModel> tripModelsList = new List<TripModel>();
                    foreach (var item in tripList)
                    {
                        tripModelsList.Add(getGenaricTrip(item, false,4, loggedInId));
                    }
                    return ObjectSerilization.GetSerilizedObject("Trip Found", true, tripModelsList);
            
            }
            return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Trip Founds", false);
        }


        public object WebTripFiltraion(TripFilter tripFilter, long loggedInId,String type)
        {

           
            var FromId = new SqlParameter("FromId", SqlDbType.Int) { Value = tripFilter.FromId };
            var ToId = new SqlParameter("ToId", SqlDbType.Int) { Value = tripFilter.ToId };
            var StartDate = new SqlParameter("StartDate", SqlDbType.Date) { Value = tripFilter.StartDate };
            var catId = new SqlParameter("CatId", SqlDbType.Int) { Value = tripFilter.cat_Id };

            if (tripFilter.StartDate < DateTime.Now)
            {
                StartDate = new SqlParameter("StartDate", SqlDbType.Date) { Value = DateTime.Now };

            }
            //var Accommodation = new SqlParameter("Accommodation", SqlDbType.VarChar) { Value = tripFilter.Accommodation };
            //var Transportation = new SqlParameter("Transportation", SqlDbType.VarChar) { Value = tripFilter.Transportation };
            var Title = new SqlParameter("Title", SqlDbType.VarChar) { Value = tripFilter.Title != null ? tripFilter.Title : "" };
           // var MinAmount = new SqlParameter("MinAmount", SqlDbType.Float) { Value = tripFilter.MinAmount };
          //  var MaxAmount = new SqlParameter("MaxAmount", SqlDbType.Float) { Value = tripFilter.MaxAmount };



          


            var Type = new SqlParameter("Type", SqlDbType.VarChar) { Value = type };




            var tripList = SpRepository<TripBasicInfo>.GetListWithStoreProcedure(@"exec WebFilter @FromId,@ToId,@CatId,@Title,@Type", FromId, ToId,  catId, Title, Type);
            if (tripList.Count > 0)
            {


                List<TripModel> tripModelsList = new List<TripModel>();
                foreach (var item in tripList)
                {
                    tripModelsList.Add(getGenaricTrip(item, false, 4, loggedInId));
                }
                return ObjectSerilization.GetSerilizedObject("Trip Found", true, tripModelsList);

            }
            return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Trip Founds", false);
        }

        #endregion Trip

        private string SendTripCancelledEmailHtml(string travllerName, string operatorName, string tripTitle, string bookingId, string status, string startDate, string durations, string bookedDate)
        {
            string emailHTML = "<h4> Dear " + travllerName + "  ,</h4>" +
                                "<p> You Booking Reference <b> #" + bookingId + "</b> trip is <b> Cancelled</b> by " + operatorName + " please contact tour operator for details.Our contact channels are open for disputes and complains.</p>" +
                                "<p>As per <span style='color:blue'>Trip</span><span style='color:orange'>Jero</span> policy you are obliged to inform or notify Tour Operator in any kind of changes to Booked Trip</p>"
                                + "<div align='center'>"
                                  + "<h2>Booking Reference #</h2><h1>" + bookingId + "</h1>"
                                + "</div>"
                                + "<div><h2>Booking Details</h2><ol>"
                                    + "<li>Tour Operator:" + operatorName + "</li>"
                                    + "<li>Trip Name:" + tripTitle + "</li>"
                                    + "<li>Status: Cancelled</li>"
                                    + "<li>Start Date:" + startDate + "</li>"
                                    + "<li>Durations:" + durations + "</li>"
                                    + "<li>Booked Date:" + bookedDate + "</li>"
                                  + "</ol></div><div>"
                                  + "<b>Disclaimer:</b> <span style='color:blue'>Trip</span><span style='color:orange'>Jero</span>, does not handles or responsible for financial losses of either party, TripJero is facility provider to query trips and operators.Our contact channels are open for disputes and complains."
                                  + "</div><br><br>"
                                + @"<div align='center'>
                                © <span style='color:blue'>Trip</span><span style='color:orange'>Jero</span>, 2020. All rights reserved.
                                TripJero.com | Islamabad, Pakistan
                                </div>";
            return emailHTML;
        }

        private string NewTripEmailHtml(string tripId, string operatorName, string tripTitle, string startDate, string endDate, string durations)
        {
            string emailHTML = "<h4> Dear " + operatorName + ",</h4>" +
                                "<div align='center'><p>"
         + "Your Add / Tip has been published and now it is visible to all travelers connected on TripJero. As per our policy you are obliged to inform or notify traveler in any kind of changes to Trip.</p><p>"
          + "Trip Publish #" + tripId + "</p></div>"
                                + "<div align='center'>"
                                  + "<h2>Trip Publish #</h2><h1>" + tripId + "</h1>"
                                + "</div>"
                                + "<div><h2>Trip Details</h2><ol>"
                                    + "<li>Tour Operator:" + operatorName + "</li>"
                                    + "<li>Trip Name:" + tripTitle + "</li>"
                                    + "<li>Status: Active</li>"

                                    + "<li>Start Date:" + startDate + "</li>"
                                    + "<li>End: Date:" + endDate + "</li>"
                                    + "<li>Duration:" + durations + "</li>"
                                    + "<li>Published Date:" + DateTime.Now + "</li>"
                                  + "</ol></div><div>"
                                  + "<b>Disclaimer:</b> <span style='color:blue'>Trip</span><span style='color:orange'>Jero</span>, does not handles or responsible for financial losses of either party, TripJero is facility provider to query trips and operators.Our contact channels are open for disputes and complains."
                                  + "</div><br><br>"
                                + @"<div align='center'>
                                © <span style='color:blue'>Trip</span><span style='color:orange'>Jero</span>, 2020. All rights reserved.
                                TripJero.com | Islamabad, Pakistan
                                </div>";
            return emailHTML;
        }

        #region Admin Section

        public object GetTripStatusCount()
        {
            var tripList = SpRepository<TripStatusCount>.GetListWithStoreProcedure(@"exec GetTripStatusCounts");
            if (tripList.Count > 0)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Trip Count Founds", true, tripList);
            }
            else
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Trip Founds", false);
            }
        }

        public object GetAllTripForAdmin()
        {
            var tripList = SpRepository<DataEntites.Model.Admin.Trip.Trip>.GetListWithStoreProcedure(@"exec GetTripForAdmin");
            if (tripList.Count > 0)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Trip Count Founds", true, tripList);
            }
            else
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Trip Founds", false);
            }
        }

        public object UpdateBookingStatus(DbContext.Trip trip)
        {
            string message = "";
            var tripUpdate = db.Trips.Find(trip.Id);
            if (tripUpdate != null)
            {
                tripUpdate.NO_MORE_BOOKING = trip.NO_MORE_BOOKING;
                db.SaveChanges();
                message = trip.NO_MORE_BOOKING == true ? "No  more booking activated" : "No  more booking de activated";
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(message, true);
            }
            return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Trip Found", false);
        }

        #endregion Admin Section

        public void AddTripView(long UId, long TId)
        {
            try
            {
                var viewData = db.TripViews.FirstOrDefault(x => x.UId == UId && x.TId == TId);
                if (viewData == null)
                {
                    TripView tripView = new TripView();
                    tripView.TId = TId;
                    tripView.UId = UId;
                    tripView.CreatedDate = DateTime.Now;
                    db.TripViews.Add(tripView);
                    db.SaveChanges();
                }
            }
            catch (Exception)
            {

                
            }
          
        }
        public async Task NotificationLog(long Uid, string Title, string Body)
        {
            NotificationHistory notificationHistory = new NotificationHistory();
            notificationHistory.UId = Uid;
            notificationHistory.Title = Title;
            notificationHistory.Body = Body;
            notificationHistory.CreatedDate = DateTime.Now;
            db.NotificationHistories.Add(notificationHistory);
            await db.SaveChangesAsync();
        }

        public object GetOperatorTrips(long Id)
        {
            var _id = new SqlParameter("UserId", SqlDbType.Int) { Value = Id };
            var tripList = SpRepository<TripBasicInfo>.GetListWithStoreProcedure(@"exec Get_Trip_By_User_Id @UserId", _id);
            if (tripList.Count > 0)
            {
                List<TripModel> tripModelsList = new List<TripModel>();
                foreach (var item in tripList)
                {
                    tripModelsList.Add(getGenaricTrip(item, false,3,0));
                }

                return ObjectSerilization.GetSerilizedObject("Trip Found", true, tripModelsList);
            }
            else
            {
                return ObjectSerilization.GetSerilizedWithoutObject("No Trip Found", false);
            }
        }

        public object AddTripAsDefult(TripDefult tripDefult)
        {
            if (tripDefult!=null)
            {
                DefultTripModal defultTripModal = new DefultTripModal();
                if (tripDefult.Id > 0)
                {

                    var exsitingDefultTrip = db.TripDefults.Find(tripDefult.Id);
                    if (exsitingDefultTrip!=null)
                    {
                        exsitingDefultTrip.TripString = tripDefult.TripString;
                        defultTripModal.Id = exsitingDefultTrip.Id;
                        exsitingDefultTrip.ModifyDate = DateTime.Now;
                        exsitingDefultTrip.IS_FacilitiesDone = exsitingDefultTrip.IS_FacilitiesDone==false ? tripDefult.IS_FacilitiesDone : true;
                        exsitingDefultTrip.IS_ItnaryPlaneDone = exsitingDefultTrip.IS_ItnaryPlaneDone == false ? tripDefult.IS_ItnaryPlaneDone : true;
                        exsitingDefultTrip.IS_TripImagesDone = exsitingDefultTrip.IS_TripImagesDone == false ? tripDefult.IS_TripImagesDone : true;
                        db.SaveChanges();
                        return ObjectSerilization.GetSerilizedObject("Trip Save as defult sucessfully", true, defultTripModal);
                    }
                }
                tripDefult.CreatedDate = DateTime.Now;
                tripDefult.TripString = tripDefult.TripString;
                tripDefult.ModifyDate = DateTime.Now;
                tripDefult.IS_FacilitiesDone = false;
                tripDefult.IS_ItnaryPlaneDone = false;
                tripDefult.IS_TripImagesDone = false;
                db.TripDefults.Add(tripDefult);
                db.SaveChanges();
                defultTripModal.Id = tripDefult.Id;
                return ObjectSerilization.GetSerilizedObject("Trip Save as defult sucessfully",true, defultTripModal);
            }
            return ObjectSerilization.GetSerilizedWithoutObject("Defult trip data not supplied", false);
        }

        public object SearchTrip(string keyword,long loggedInId)
        {
            
            var _keyword = new SqlParameter("KeyWord", SqlDbType.VarChar) { Value = keyword };
            var tripList = SpRepository<TripBasicInfo>.GetListWithStoreProcedure(@"exec SearcTrips @KeyWord", _keyword);
            if (tripList.Any())
            {
                List<TripModel> tripModelsList = new List<TripModel>();
                foreach (var item in tripList)
                {
                    tripModelsList.Add(getGenaricTrip(item,false,4, loggedInId));
                }
                return ObjectSerilization.GetSerilizedObject("Trip Found",true, tripModelsList);
            } else
            {
                return ObjectSerilization.GetSerilizedWithoutObject("No Trip Found", false);
            }
        }



        public object getTripByPlaceId(long place_id,long loogedInId,int pageNo,int pageSize)
        {

            var _keyword = new SqlParameter("placeId", SqlDbType.VarChar) { Value = place_id };
            var _pageNo = new SqlParameter("PageNumber", SqlDbType.Int) { Value = pageNo };
            var _pageSize = new SqlParameter("RowOfPage", SqlDbType.Int) { Value = pageSize };
            var tripList = SpRepository<TripBasicInfo>.GetListWithStoreProcedure(@"exec GetTripsByPlaceId @placeId,@PageNumber,@RowOfPage", _keyword,_pageNo,_pageSize);
            if (tripList.Any())
            {
                List<TripModel> tripModelsList = new List<TripModel>();
                foreach (var item in tripList)
                {
                    tripModelsList.Add(getGenaricTrip(item, false,4,loogedInId));
                }
                return ObjectSerilization.GetSerilizedObject("Trip Found", true, tripModelsList, tripList[0].PageSize, tripList[0].PageNo, tripList[0].TotalPage);
            }
            else
            {
                return ObjectSerilization.GetSerilizedWithoutObject("No Trip Found", false);
            }
        }


        public object getTripByOptId(long optId,long loggedInId)
        {

            var _keyword = new SqlParameter("optId", SqlDbType.VarChar) { Value = optId };
            var tripList = SpRepository<TripBasicInfo>.GetListWithStoreProcedure(@"exec GetScheduleTripsByOptId @optId", _keyword);
            if (tripList.Any())
            {
                List<TripModel> tripModelsList = new List<TripModel>();
                foreach (var item in tripList)
                {
                    tripModelsList.Add(getGenaricTrip(item, false,4,loggedInId));
                }
                return ObjectSerilization.GetSerilizedObject("Trip Found", true, tripModelsList);
            }
            else
            {
                return ObjectSerilization.GetSerilizedWithoutObject("No Trip Found", false);
            }
        }



        public object SendSingleTripReport(long id, string Email)
        {
            var tripId = new SqlParameter("TripId", SqlDbType.Int) { Value = id };
            var tripcurrentStatus = SpRepository<CompleteTripModal>.GetSingleObjectWithStoreProcedure(@"exec Get_Complete_Trip_Report @TripId", tripId);
            if (tripcurrentStatus != null)
            {
                string EmailString = Utilties.Genral.EmailTemplates.CompleteTripEmailString(tripcurrentStatus);
                EmailSender.SendEmail(Email, "Complete trip Details", EmailString);
                return ObjectSerilization.GetSerilizedWithoutObject("Report Sent to your registred email", true);
            }
            return ObjectSerilization.GetSerilizedWithoutObject("No Report available for requried trip ", false);
        }

        public object GetAllDraftTripsByUsers(long Uid)
        {

            var _keyword = new SqlParameter("OptId", SqlDbType.VarChar) { Value = Uid };
            var dfrattrip = SpRepository<DraftModel>.GetListWithStoreProcedure(@"exec GETDraftTrips @OptId", _keyword);
           // var dfrattrip = db.TripDefults.Where(x => x.UId == Uid).ToList();
            if (dfrattrip.Any())
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Draft Trips Founds", true, dfrattrip);
            }
            return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Draft Trips Founds", false);
        }

        public object GetAllDraftTripsByUsers(long Uid, long DId)
        {
            var dfrattrip = db.TripDefults.FirstOrDefault(x => x.UId == Uid && x.Id == DId);
            if (dfrattrip != null)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Draft Trips Founds", true, dfrattrip);
            }
            return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Draft Trips Founds", false);
        }

        public object GetTripCountByUser(long UId)
        {
            var _id = new SqlParameter("UserId", SqlDbType.BigInt) { Value = UId };
            var tripList = SpRepository<TripCountByUserModel>.GetSingleObjectWithStoreProcedure(@"exec GetAllUsersTripCounts @UserId", _id);
            if (tripList != null)
            {
                return ObjectSerilization.GetSerilizedObject("Trip Count Found", true, tripList);
            }
            return ObjectSerilization.GetSerilizedWithoutObject("No Trip Found For Cout", false);
        }
        public object GetOperatorGraphData(long UId)
        {

            var currentYY =  DateTime.Now.ToString("yyyy");
            List<MonthList> monthList = new List<MonthList>();

            for (int i = 0; i < 12; i++)
            {
                MonthList ml = new MonthList();
               ml.month = i+1;
                ml.year = int.Parse(currentYY);
                monthList.Add(ml);
            }

            var _keyword = new SqlParameter("optId", SqlDbType.VarChar) { Value = UId };
            var tripList = SpRepository<OperatorGraphModel>.GetListWithStoreProcedure(@"exec GetAllTripsByOptIDWithBookings @optId", _keyword);
            if (tripList.Any())
            {
                for(int v = 0;v<monthList.Count;v++)

                {
                    var month = monthList[v];

                    var totalTrips = 0;
                    var totalbookings = 0;
                    foreach (var item in tripList)
                    {
                        if (item.CreatedDate.Month == month.month && item.CreatedDate.Year == month.year)
                        {
                            totalTrips += 1;
                            totalbookings += getTripBookings(item);
                        }

                    }

                    MonthlyTripData mm = new MonthlyTripData();
                    mm.TotalTripPosted = totalTrips;
                    mm.TotalBookings = totalbookings;
                    monthList[v].monthlyTripData = mm;
                }
            
                return ObjectSerilization.GetSerilizedObject("Trip Found", true, monthList);
            }
            else
            {
                return ObjectSerilization.GetSerilizedObject("Trip Found", true, monthList);
            }
        }

        public int getTripBookings(OperatorGraphModel req)
        {

            var _keyword = new SqlParameter("tripId", SqlDbType.VarChar) { Value = req.Id };
            var tripList = SpRepository<OperatorGraphModel>.GetSingleObjectWithStoreProcedure(@"exec GETBookingCountsByTripId  @tripId", _keyword);
            if (tripList != null)
            {
               return tripList.TotalBookings;
            }
            else
            {
               return 0;
            }

            return 0;

        }
        public object DeletDraftTrip(long Id)
        {
            var tripToDelete = db.TripDefults.Find(Id);
            if (tripToDelete != null)
            {
                db.TripDefults.Remove(tripToDelete);
                db.SaveChanges();
                return ObjectSerilization.GetSerilizedWithoutObject("Trip Deleted", true);
            }
            return ObjectSerilization.GetSerilizedWithoutObject("No Trip found to delete", false);
        }

        public object GetTripByUserIdAndStatusId(long Id,int statusId, int pageNo, int pageSize)
        {
            var _operatorId = new SqlParameter("UserId", SqlDbType.BigInt) { Value = Id };
            var _statusId = new SqlParameter("TripStatusId", SqlDbType.BigInt) { Value = statusId };
            var _pageNo = new SqlParameter("PageNumber", SqlDbType.Int) { Value = pageNo };
            var _pageSize = new SqlParameter("RowOfPage", SqlDbType.Int) { Value = pageSize };
            var tripReviewsList = SpRepository<TripsByStatus>.GetListWithStoreProcedure(@"exec GetAllCompletedTripByUserId @UserId,@TripStatusId,@PageNumber,@RowOfPage", 
                _operatorId,_statusId,_pageNo,_pageSize);
            if (tripReviewsList.Count >0)
            {
                return ObjectSerilization.GetSerilizedObject("Trip Info Found", true, tripReviewsList, tripReviewsList[0].PageSize, tripReviewsList[0].PageNo, tripReviewsList[0].TotalPage);
            }
            return ObjectSerilization.GetSerilizedWithoutObject("No Trip Info Found", false);

        }

        public object GetTripByIdForShare(long Id)
        {
            var result = db.Trips.Find(Id);
            if (result!=null)
            {
                var imagesList = db.TripImages.FirstOrDefault(x => x.TripId == result.Id);
                var returnResult = new
                {
                    Id = result.Id,
                    Title = result.Title,
                    Description = result.Details,
                    ImageUrl = imagesList != null ? imagesList.ImageUrl : ""
                };
                return ObjectSerilization.GetSerilizedObject("Trip Info Found", true, returnResult);
            }
            return ObjectSerilization.GetSerilizedWithoutObject("No Trip Info Found", false);
        }

      
    }


  
    public class DefultTripModal
    {
        public int Id { get; set; }
    }
}