﻿using DataEntites.Model.Trip;
using DataEntites.Model.Trip.TripSearch;
using DataEntites.Model.Utilities;
using System.Collections.Generic;
using System.Net.Http.Headers;

namespace Buisness.Core.Trip
{
    public interface ITrip
    {
        object Addtrip(TripModel tripModel, long Id,string callFrom="");

        object GetScheduleTripViews(long Id);
        object UpdateTrip(TripModel tripModel);

        object GetTripByUserId(long Id);

        object GetTripById(long Id, long ViewId, int userroleId,string callfrom = "");

        object GetAllTrips(long Id, int pageNo , int pageSize);
        
        object GetAllStatusTrips(long Id, int pageNo, int pageSize);

        object AddTripImage(TripImages tripImages);

        object GetTripImagesByTripId(long Id);

        object AddReview(TripReview tripReview);

        object DeleteTripImage(int Id);

        object ActiveDeActiveTripImage(int Id, bool Status);

        object GetTripReviewByTripId(long Id);

        object DeleteTripReviewById(int id);

        object ActiveDActiveTripReviewById(int id, bool Status);

        object TempTripImageUpload(string imageString,string from, long Id);
        object TempTripImageUpload(List<TripTempImagesList> imageString, long Id);


        object ActiveDActiveTip(int id, bool Status);

        object UpdateTripReview(TripReview tripReview);

        object UpdateTripImage(TripImages tripImages);

        object DeleteTempTripImage(string imageString);

        object UpdateTripStatus(long opratorId, TripStatus tripStatus, string callFrom = "");

        object UpdateTripComment(DataEntites.Model.Trip.TripReview tripReview);

        object TripCommpersion(TripComparison tripComparison);

        object TripFiltraion(TripFilter tripFilter,long Id);

        object WebTripFiltraion(TripFilter tripFilter, long Id,string type);
        object UpdateBookingStatus(DbContext.Trip trip);

        object GetAllBoostTrips(long Id);

        object GetFavtTrips(long user_id, int pageNo, int pageSize);
        object GetAllTripByUserId(long Id);
        object GetOperatorTrips(long Id);

        object GetOperatorGraphData(long Id);
        object AddTripAsDefult(DbContext.TripDefult tripDefult);

        object SearchTrip(string keyword,long Id);

        object getTripByPlaceId(long place_id,long Id, int pageNo, int pageSize);

        object getTripByOptId(long optId,long Id);
        object SendSingleTripReport(long id, string Email);
        object GetTripCountByUser(long UId);
        object DeletDraftTrip(long Id);
        #region Website
        object GetAllDraftTripsByUsers(long Uid);
        object GetAllDraftTripsByUsers(long Uid,long DId);
        #endregion

        #region Admin Section

        object GetTripStatusCount();

        object GetAllTripForAdmin();
        object GetTripByUserIdAndStatusId(long Id, int statusId, int pageNo, int pageSize);

        #endregion Admin Section
        #region Share Content
        object GetTripByIdForShare(long Id);
        #endregion
    }
}