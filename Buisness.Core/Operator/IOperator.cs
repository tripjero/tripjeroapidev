﻿namespace Buisness.Core.Operator
{
    public interface IOperator
    {
        object UpdateTripStatus(long operatorId);

        object GetOperatorTripsWithStatus(long operatorId, int statusId);

        object GetOperatorPackageInfo(long operatorId);
    }
}