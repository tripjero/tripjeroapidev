﻿using Buisness.Core.DbContext;
using DataEntites.GeneraicRepositroy;
using DataEntites.Model;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Buisness.Core.Operator
{
    public class OperatorService : IOperator
    {
        private TripJeroEntities db = new TripJeroEntities();

        public object GetOperatorTripsWithStatus(long operatorId, int statusId)
        {
            var opratorId = new SqlParameter("OperatorId", SqlDbType.BigInt) { Value = operatorId };
            var _statusId = new SqlParameter("StatusId", SqlDbType.Int) { Value = statusId };
            var operatorTrips = SpRepository<OperatorTripStatus>.GetListWithStoreProcedure(@"exec Get_Operators_Trips_BY_Operator_Id @OperatorId,@StatusId", opratorId, _statusId);
            if (operatorTrips.Count > 0)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Trip Founds", true, operatorTrips);
            }
            else
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Trip Not Founds", false);
            }
        }

        public object UpdateTripStatus(long operatorId)
        {
            throw new NotImplementedException();
        }

        public object GetOperatorPackageInfo(long operatorId)
        {
            var opratorId = new SqlParameter("OperatorId", SqlDbType.BigInt) { Value = operatorId };
            var operatorsubPackage = SpRepository<DataEntites.Model.Operators.OperatorPackages>.GetListWithStoreProcedure(@"exec Get_Operator_Packages_Info_By_UId @OperatorId", opratorId);
            if (operatorsubPackage.Count > 0)
            {
                DataEntites.Model.Operators.OperatorPackagesInfo operatorPackagesInfo = new DataEntites.Model.Operators.OperatorPackagesInfo();
                operatorPackagesInfo.PackgeName = operatorsubPackage[0].PackageName;
                operatorPackagesInfo.LimitedTrip = operatorsubPackage[0].TripLimits;
                operatorPackagesInfo.PackageDetaisl.Add(new { Details = operatorsubPackage[0].TripLimitation });
                foreach (var item in operatorsubPackage)
                {
                    operatorPackagesInfo.PackageDetaisl.Add(new { Details = item.RulsContent });
                }
                return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Packages Founds", true, operatorPackagesInfo);
            }
            else
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Packages Founds", false);
            }
        }
    }
}