﻿using Buisness.Core.DbContext;
using DataEntites.GeneraicRepositroy;
using DataEntites.Model.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using TripJero.Notification.FirbeaseNotification;
using TripJero.Notification.Services;
using Utilties.Genral;

namespace Buisness.Core.UtitliesSection
{
    public class UtilitiesService : IUtilities
    {
        private DbContext.TripJeroEntities db = new DbContext.TripJeroEntities();

        public object GetTripStatus()
        {
            var tripStatus = db.TripStatus.ToList();
            List<TripStatusDropDown> tripStatusDropDownList = new List<TripStatusDropDown>();
            foreach (var item in tripStatus)
            {
                TripStatusDropDown tripStatusDropDown = new TripStatusDropDown();
                tripStatusDropDown.Id = item.Id;
                tripStatusDropDown.StatusName = item.Name;
                tripStatusDropDownList.Add(tripStatusDropDown);
            }
            return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Trip Status Found", true, tripStatusDropDownList);
        }

        public object GetVisitPlaces()
        {
            List<DataEntites.Model.VistPlace.VistPlaces> PlacesList = new List<DataEntites.Model.VistPlace.VistPlaces>();
            var vistPlacesList = db.VisitPlaces.ToList();
            if (vistPlacesList.Count > 0)
            {
                foreach (var item in vistPlacesList)
                {
                    DataEntites.Model.VistPlace.VistPlaces vistPlaces = new DataEntites.Model.VistPlace.VistPlaces();
                    vistPlaces.Id = item.Id;
                    vistPlaces.trips = db.Trips.Where(x => x.ToVistId == item.Id).Count();
                    vistPlaces.VistPlace = item.Place + ", " + item.City.Name + " , " + item.City.Province.Country.Name;
                    vistPlaces.CityId = item.CityId;
                    vistPlaces.ProviceId = item.City.ProvincesId;
                    vistPlaces.PartialVisitPlace = item.Place;
                    vistPlaces.ImgPath = item.img_path;
                    PlacesList.Add(vistPlaces);
                }
                return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Vist Placess found", true, PlacesList.OrderByDescending(o => o.trips).ToList());
            }
            return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Vist Place Found", false);
        }


        public object GetVistPlacesWithCount()
        {
            List<DataEntites.Model.VistPlace.VistPlaces> PlacesList = new List<DataEntites.Model.VistPlace.VistPlaces>();
            var vistPlacesList = db.VisitPlaces.ToList();
            if (vistPlacesList.Count > 0)
            {
                foreach (var item in vistPlacesList)
                {
                    DataEntites.Model.VistPlace.VistPlaces vistPlaces = new DataEntites.Model.VistPlace.VistPlaces();
                    vistPlaces.Id = item.Id;
                    vistPlaces.trips = db.Trips.Where(x => x.ToVistId == item.Id).Count();
                    vistPlaces.VistPlace = item.Place + ", " + item.City.Name + " , " + item.City.Province.Country.Name;
                    vistPlaces.CityId = item.CityId;
                    vistPlaces.ProviceId = item.City.ProvincesId;
                    vistPlaces.PartialVisitPlace = item.Place;
                    vistPlaces.ImgPath = item.img_path;
                    PlacesList.Add(vistPlaces);
                }
                return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Vist Placess found", true, PlacesList);
            }
            return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Vist Place Found", false);

        }
        public object AddNewVisitPlace(VisitplaceModal visitplaceModal)
        {
            if (visitplaceModal.Id > 0)
            {
                var visitPlaceUpdate = db.VisitPlaces.Find(visitplaceModal.Id); ;
                visitPlaceUpdate.Place = visitplaceModal.VisitPlace;
                visitPlaceUpdate.CityId = visitplaceModal.CityId;
                visitPlaceUpdate.img_path = visitplaceModal.ImgPath;
                db.SaveChanges();
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Visit Places Updated", true);
            }
            else
            {
                var visitPlace = new DbContext.VisitPlace();
                visitPlace.Place = visitplaceModal.VisitPlace;
                visitPlace.CityId = visitplaceModal.CityId;
                visitPlace.img_path = visitplaceModal.ImgPath;

                db.VisitPlaces.Add(visitPlace);
                db.SaveChanges();
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Visit Places Updated", true);
            }
        }

        public object UpdateCity(City city)
        {
            if (city.Id > 0)
            {
                var visitPlaceUpdate = db.Cities.Find(city.Id); ;
                visitPlaceUpdate.Name = city.Name;
                visitPlaceUpdate.ProvincesId = city.ProvincesId;
                db.SaveChanges();
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("City Updated", true);
            }
            else
            {
                var cities = new DbContext.City();
                cities.Name = city.Name;
                cities.ProvincesId = city.ProvincesId;
                db.Cities.Add(cities);
                db.SaveChanges();
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("City Updated", true);
            }
        }

        public object GetTripCategories()
        {
            var categories = db.TripCategories.Where(x=>x.IS_Delted != true).ToList();
            List<TripCategoresModel> tripCategores = new List<TripCategoresModel>();
            if (categories!=null)
            {
                foreach (var item in categories)
                {
                    var ctagory = new TripCategoresModel();
                    ctagory.Id = item.Id;
                    ctagory.Name = item.Name;
                    tripCategores.Add(ctagory);
                }
            }
            return ObjectSerilization.GetSerilizedObject("Catagories Found",true,tripCategores);
        }

        public object GetNotificationHistory(long UId, int pageNo, int pageSize)
        {
            var _id = new SqlParameter("UserId", SqlDbType.Int) { Value = UId };
            var _pageNo = new SqlParameter("PageNumber", SqlDbType.Int) { Value = pageNo };
            var _pageSize = new SqlParameter("RowOfPage", SqlDbType.Int) { Value = pageSize };
            var list = SpRepository<NotificationHistoryDTO>.GetListWithStoreProcedure(@"exec Get_Notifications_By_UID @UserId,@PageNumber,@RowOfPage", _id,_pageNo,_pageSize);
            if (list.Count > 0)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Notification Founds", true, list, list[0].PageSize, pageNo, list[0].TotalPage);
            }
            else
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Notification Found", false);
            }
            
         
        }

        public object DeletNotificationById(long NId)
        {
            var notification = db.NotificationHistories.Find(NId);
            if (notification!=null)
            {
                db.NotificationHistories.Remove(notification);
                db.SaveChanges();
                return ObjectSerilization.GetSerilizedWithoutObject("Notification Deleted", true);
            }
            return ObjectSerilization.GetSerilizedWithoutObject("No Notification Deleted", false);
        }

        public object DeleteAllNotification(long UId)
        {
            var notification = db.NotificationHistories.Where(x=>x.UId==UId);
            if (notification != null)
            {

                db.NotificationHistories.RemoveRange(notification);

                db.SaveChanges();
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("All Notification Deleted", true);
            }
            return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Notification Deleted", false);
        }

        public object AddTripCategories(TripCategory tripCategory)
        {
            if (tripCategory!=null)
            {
                tripCategory.IS_Delted = false;
                db.TripCategories.Add(tripCategory);
                db.SaveChanges();
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Categories has been Added",true);
            }
            return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Categories has been Added", false);
        }

        public object UpdateTripCategory(TripCategory tripCategory)
        {
            if (tripCategory!=null)
            {
                var existingCategory = db.TripCategories.Find(tripCategory.Id);
                existingCategory.Name = tripCategory.Name;
                db.SaveChanges();
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Categories has been updatetd", true);
            }
            return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Some thing wrong please try again", false);
        }

        public object DeleteCategory(int Id)
        {
            var existingCatg = db.TripCategories.Find(Id);
            existingCatg.IS_Delted = true;
            db.SaveChanges();
            return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Categories has been deleted", true);
        }


        public object AllTravPushNotification(NotificationHistory noti)
        {

            var users = db.Users.Where(x => x.RoleId == noti.Id).ToList();


            try
            {
                foreach (var item in users)
                {
                    if (item.FirebaseToken != null && item.FirebaseToken != "")
                    {
                        NotificationSender.SendNotification(item.FirebaseToken, noti.Body, noti.Title);
                    }
                }

                return ObjectSerilization.GetSerilizedWithoutObject("Notification sent successfuly", true);

            }
            catch (Exception ex)
            {
                return ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false);

            }

        }
        public object GetTravCounters()
        {

           var  TravCount = SpRepository<TravCounterModel>.GetSingleObjectWithStoreProcedure(@"exec TravellerCounter");
            if (TravCount != null)
            {
                return ObjectSerilization.GetSerilizedObject("Counter", true, TravCount);
            }
            else
            {
                return ObjectSerilization.GetSerilizedWithoutObject("Not Found", false);
            }
        }


        public object getCurrency()
        {

            String JsonResult = GetRates().Result;
           // CurrencyResponse CurrencyDetail = Newtonsoft.Json.JsonConvert.DeserializeObject<CurrencyResponse>(JsonResult);
            Dictionary<string, double> Rates = new Dictionary<string, double>();
            //Rates.Add("EURO", (1 / CurrencyDetail.rates["PKR"]));
            //Rates.Add("USD", (CurrencyDetail.rates["USD"] / CurrencyDetail.rates["PKR"]));
            //Rates.Add("PKR", 1);

            Rates.Add("EURO", 0.0053);
            Rates.Add("USD", 0.0063);
            Rates.Add("PKR", 1);


            return ObjectSerilization.GetSerilizedObject("Rates", true,Rates);
        }



        public static async Task<String> GetRates()
        {

           var URL = "http://data.fixer.io/api/latest?access_key=4ed93f93df1100c270e6b96c539e7e43&symbols=PKR,USD,EU";
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);

            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));


            //if (AddAutherizationHeader)
            //{
            //    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
            //}

            try
            {
                HttpResponseMessage response = client.GetAsync("").Result;
                response.EnsureSuccessStatusCode();
                String result = await response.Content.ReadAsStringAsync();
                client.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }



        }

        public object getFcmTokenByEmail(string email)
        {
            var user = db.Users.Where(x => x.Email == email).FirstOrDefault();
            if (user != null)
            {
                return ObjectSerilization.GetSerilizedObject("Token found", true, user.FirebaseToken);
              
             
            }
            else
            {
                return ObjectSerilization.GetSerilizedWithoutObject("Not Found", false);
            }
        }
    }
}