﻿using Buisness.Core.DbContext;
using DataEntites.Model.Utilities;

namespace Buisness.Core.UtitliesSection
{
    public interface IUtilities
    {
        object GetVisitPlaces();

        object GetVistPlacesWithCount();
        object GetTripStatus();

        object AddNewVisitPlace(VisitplaceModal visitplaceModal);

        object AllTravPushNotification(NotificationHistory noti);
        object GetTravCounters();
        object UpdateCity(City city);
        object GetNotificationHistory(long UId,int pageNo,int pageSize);
        object DeletNotificationById(long NId);
        object DeleteAllNotification(long UId);

        object getFcmTokenByEmail(string email);

        object getCurrency();
        #region Admin Section
        object GetTripCategories();
        object AddTripCategories(TripCategory tripCategory);
        object UpdateTripCategory(TripCategory tripCategory);
        object DeleteCategory(int Id);
        #endregion


    }
}