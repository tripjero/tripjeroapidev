﻿using Buisness.Core.DbContext;
using System;
using System.Linq;

namespace Buisness.Core.OwnRepostry
{
    public class UserMasterRepository : IUserMaster, IDisposable
    {
        private DbContext.TripJeroEntities context = new TripJeroEntities();

        public void Dispose()
        {
            context.Dispose();
        }

        public DbContext.User ValidateUser(string Email, string Password)
        {
            return context.Users.FirstOrDefault(user =>
            user.Email.Equals(Email, StringComparison.OrdinalIgnoreCase)
            && user.Password == Password);
        }
    }
}