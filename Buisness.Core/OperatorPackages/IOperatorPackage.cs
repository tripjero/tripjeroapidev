﻿using DataEntites.Model.BoostTrip;
using DataEntites.Model.OperatorPackages;

namespace Buisness.Core.OperatorPackages
{
    public interface IOperatorPackage
    {
        object AddNewPackage(OperatorPackagesModel operatorPackage);

        object UpdatePackage(OperatorPackagesModel operatorPackage);

        object GetAllPackages();
        object PackageSubscription(long pid, long uid);

        object GetBoostPackageByUser(long uid, int pageNo, int pageSize);
        object GetOperatorPackageById(long Id);


        #region Admin
        object AddBoostPackage(DbContext.BoostPackage boostPackage, long UserId);
        object UpdateBoostPackage(DbContext.BoostPackage boostPackage, long UserId);
        object DeleteBoostPackage(long Id, long UserId);
        object UpdateBoostStatus(bool status, long Id, long UserId);
        object AddBoostPromo(DbContext.BoostPackagesPromo boostPackagesPromo, long UserId);
        object DeletePromoPackage(long Id, long UserId);
        object UpdateBoostPromo(DbContext.BoostPackagesPromo boostPackagesPromo, long UserId);
        object DeleteOperatorPackage(long Id, long UserId);
        #endregion
    }
}