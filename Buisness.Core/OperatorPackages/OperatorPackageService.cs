﻿using Buisness.Core.DbContext;
using DataEntites.GeneraicRepositroy;
using DataEntites.Model.BoostTrip;
using DataEntites.Model.OperatorPackages;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using TripJero.Notification.Services;
using Utilties.Genral;

namespace Buisness.Core.OperatorPackages
{
    public class OperatorPackageService : IOperatorPackage
    {
        private TripJeroEntities db = new TripJeroEntities();

        public object GetAllPackages()
        {
            List<OperatorPackagesModel> operatorPackageList = new List<OperatorPackagesModel>();
            var dbPackages = db.OperatorPackages.Where(x=>x.Is_Delete != true).ToList();
            if (dbPackages != null)
            {
                foreach (var item in dbPackages)
                {
                    OperatorPackagesModel opt = new OperatorPackagesModel();
                    opt.Id = item.Id;
                    opt.Name = item.Name;
                    opt.LimitaionTrips = item.LimitaionTrips;
                    opt.Price = item.Price;
                    opt.IsActive = item.IsActive;
                    if (item.Is_Discount.HasValue)
                    {
                        if ((bool)item.Is_Discount)
                        {
                            opt.CurrentPrice = DiscountCalculator.Discount(Convert.ToInt32(item.Discount), Convert.ToDouble(item.Price));
                        }
                    }
                    else
                    {
                        opt.CurrentPrice = item.Price!=null ? (int)item.Price: 0;
                    }
                    opt.Discount = item.Discount;
                    opt.CreatedDate = item.CreatedDate;
                    opt.IS_Discount = item.Is_Discount;
                    opt.Details = item.Details;
                    operatorPackageList.Add(opt);
                }
            }
            return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Packages", true, operatorPackageList);
        }

        public object PackageSubscription(long pid, long uid)
        {
            var SubPackage = db.SubscriptionPackages.FirstOrDefault(s => s.UId == uid && s.OId == pid);
            if (SubPackage!=null)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Package Already in your subscription list",false);
            }
            try
            {
                var existingPacakge = db.SubscriptionPackages.FirstOrDefault(x => x.UId == uid && x.IsActive == true);
                if (existingPacakge != null)
                {
                    existingPacakge.IsActive = false;
                    existingPacakge.IS_Payment = false;
                    db.SaveChanges();
                }
                SubscriptionPackage subscription = new SubscriptionPackage();
                subscription.CreatedDate = DateTime.Now;
                subscription.IS_Payment = false;
                subscription.ModifyDate = DateTime.Now;
                subscription.IsActive = true;
                subscription.OId = pid;
                subscription.UId = uid;
                subscription.IS_Payment = false;
                db.SubscriptionPackages.Add(subscription);
                db.SaveChanges();
                var user = db.Users.Find(uid);
                string EmailString = Utilties.Genral.EmailTemplates.profilePkgSubscription(user.FullName, subscription.OperatorPackage.Name, subscription.OperatorPackage.Price.ToString(), subscription.OperatorPackage.LimitaionTrips.ToString());
                EmailSender.SendEmail(user.Email, "Package subscription", EmailString);

                return ObjectSerilization.GetSerilizedWithoutObject("Your subscription request send to Admin for review, Package will be active when you payment is done.", true);
            }
            catch (Exception ex)
            {

                return ObjectSerilization.GetSerilizedWithoutObject("Some thing went wrong please try again latter", false);
            }
          
          
        }

        public object UpdatePackage(OperatorPackagesModel operatorPackage)
        {
            try
            {
                OperatorPackage package = db.OperatorPackages.Find(operatorPackage.Id);
                if (package != null)
                {
                    package.IsActive = false;
                    package.LimitaionTrips = operatorPackage.LimitaionTrips;
                    package.ModifyById = operatorPackage.ModifyById;
                    package.ModifyDate = DateTime.Now;
                    package.Name = operatorPackage.Name;
                    package.CreatedDate = DateTime.Now;
                    db.OperatorPackages.Add(package);
                    db.SaveChanges();
                    return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("New Packages created", true);
                }
                else
                {
                    return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Package Avaiable for given Id", false);
                }
            }
            catch (Exception ex)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false);
            }
        }
        public object GetBoostPackageByUser(long uid,int pageNo,int pageSize)
        {
            var userId = new SqlParameter("UserId", SqlDbType.BigInt) { Value = uid };
            var _pageNo = new SqlParameter("PageNumber", SqlDbType.Int) { Value = pageNo };
            var _pageSize = new SqlParameter("RowOfPage", SqlDbType.Int) { Value = pageSize };
            var boostPakagesList = SpRepository<BoostPackagesUserModal>.GetListWithStoreProcedure(@"exec GetBoostPackageSubscriptonById @UserId,@PageNumber,@RowOfPage", userId,_pageNo,_pageSize);
            if (boostPakagesList.Count > 0)
            {
                return ObjectSerilization.GetSerilizedObject("Boost Packages Found", true, boostPakagesList, boostPakagesList[0].PageSize, boostPakagesList[0].PageNo, boostPakagesList[0].TotalPage);
            }
            else
            {
                return ObjectSerilization.GetSerilizedWithoutObject("Boost Packages Not Found", false);
            }
        }

        public object GetOperatorPackageById(long Id)
        {
            OperatorPackagesModel opt = new OperatorPackagesModel();
            var dbPackages = db.OperatorPackages.Find(Id);
            if (dbPackages != null)
            {


                opt.Id = dbPackages.Id;
                opt.Name = dbPackages.Name;
                opt.LimitaionTrips = dbPackages.LimitaionTrips;
                opt.Price = dbPackages.Price;
                opt.IsActive = dbPackages.IsActive;
                if (dbPackages.Is_Discount.HasValue)
                {
                    if ((bool)dbPackages.Is_Discount)
                    {
                        opt.CurrentPrice = DiscountCalculator.Discount(Convert.ToInt32(dbPackages.Discount), Convert.ToDouble(dbPackages.Price));
                    }
                    else
                    {
                        opt.CurrentPrice = dbPackages.Price != null ? (double)dbPackages.Price : 0;
                    }
                }
                else
                {
                    opt.CurrentPrice = dbPackages.Price!=null ? (double)dbPackages.Price: 0;
                }
                opt.Discount = dbPackages.Discount;
                opt.CreatedDate = dbPackages.CreatedDate;
                opt.IS_Discount = dbPackages.Is_Discount;
                opt.Details = dbPackages.Details;
                return Utilties.Genral.ObjectSerilization.GetSerilizedObject("Packages", true, opt);
            }
            else
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("No Package Found", false);
            }
            
        }


        #region Admin
        public object AddBoostPackage(BoostPackage boostPackage, long UserId)
        {
            if (boostPackage!=null)
            {
                boostPackage.CreatedDate = DateTime.Now;
                boostPackage.ModifyDate = DateTime.Now;
                boostPackage.ModifyBy = UserId;
                boostPackage.IsActive = true;
                boostPackage.IsDeleted = false;
                db.BoostPackages.Add(boostPackage);
                db.SaveChanges();
                return ObjectSerilization.GetSerilizedWithoutObject("Boost Package Added",true);
            }
            return ObjectSerilization.GetSerilizedWithoutObject("Data should be supplied", false);

        }

        public object UpdateBoostPackage(BoostPackage boostPackage, long UserId)
        {
            if (boostPackage != null)
            {
                var updatedData = db.BoostPackages.Find(boostPackage.Id);
                if (updatedData!=null)
                {

                    updatedData.CreatedDate = DateTime.Now;
                    updatedData.ModifyDate = DateTime.Now;
                    updatedData.ModifyBy = UserId;
                    updatedData.IsActive = true;
                    updatedData.IsDeleted = false;
                    updatedData.Is_Promo = boostPackage.Is_Promo;
                    updatedData.NoTrips = boostPackage.NoTrips;
                    updatedData.Is_Promo = boostPackage.Is_Promo;
                    updatedData.Price = boostPackage.Price;
                    updatedData.PromoId = boostPackage.PromoId;
                    updatedData.Title = boostPackage.Title;
                    updatedData.Discount = boostPackage.Discount;
                    db.SaveChanges();
                    return ObjectSerilization.GetSerilizedWithoutObject("Boost Package Updated", true);
                }
                return ObjectSerilization.GetSerilizedWithoutObject("Id not Valid", false);

            }
            return ObjectSerilization.GetSerilizedWithoutObject("Data should be supplied", false);
        }

        public object DeleteBoostPackage(long Id, long UserId)
        {
            var updatedData = db.BoostPackages.Find(Id);
            if (updatedData!=null)
            {
                updatedData.IsDeleted = true;
                updatedData.ModifyBy = UserId;
                db.SaveChanges();
                return ObjectSerilization.GetSerilizedWithoutObject("Package Deleted", true);
            }
            return ObjectSerilization.GetSerilizedWithoutObject("Values no avaiable", false);
        }

        public object AddBoostPromo(BoostPackagesPromo boostPackagesPromo, long UserId)
        {
            if (boostPackagesPromo.EndDate < DateTime.Now )
            {
                return ObjectSerilization.GetSerilizedWithoutObject("End date must be greater then current date",false);
            }
            if (db.BoostPackagesPromoes.FirstOrDefault(x=>x.PromoCode==boostPackagesPromo.PromoCode && x.Is_Delete == false)!=null)
            {
                return ObjectSerilization.GetSerilizedWithoutObject("Promo Code Already Exsit", false);
            }
            boostPackagesPromo.IsActive = true;
            boostPackagesPromo.CreatedBy = UserId;
            boostPackagesPromo.ModifyBy = UserId;
            boostPackagesPromo.CreatedDate = DateTime.Now;
            boostPackagesPromo.ModifyDate = DateTime.Now;
            db.BoostPackagesPromoes.Add(boostPackagesPromo);
            db.SaveChanges();
            return ObjectSerilization.GetSerilizedWithoutObject("End date must be greater then current date", false);
        }

        public object DeletePromoPackage(long Id, long UserId)
        {
            var existingPromo = db.BoostPackagesPromoes.Find(Id);
            existingPromo.Is_Delete = true;
            db.SaveChanges();
            return ObjectSerilization.GetSerilizedWithoutObject("Promo Code Deleted", true);
        }

        public object UpdateBoostPromo(BoostPackagesPromo boostPackagesPromo, long UserId)
        {
            if (boostPackagesPromo.EndDate < DateTime.Now)
            {
                return ObjectSerilization.GetSerilizedWithoutObject("End date must be greater then current date", false);
            }
            if (db.BoostPackagesPromoes.FirstOrDefault(x => x.PromoCode == boostPackagesPromo.PromoCode 
                                                        && x.Is_Delete == false
                                                        && x.Id !=boostPackagesPromo.Id) != null)
            {
                return ObjectSerilization.GetSerilizedWithoutObject("Promo Code Already Exsit", false);
            }
            var exsitingPromo = db.BoostPackagesPromoes.Find(boostPackagesPromo.Id);
            boostPackagesPromo.IsActive = true;
            boostPackagesPromo.CreatedBy = UserId;
            boostPackagesPromo.ModifyBy = UserId;
            boostPackagesPromo.CreatedDate = DateTime.Now;
            boostPackagesPromo.ModifyDate = DateTime.Now;
            boostPackagesPromo.StartDate = boostPackagesPromo.StartDate;
            boostPackagesPromo.EndDate = boostPackagesPromo.EndDate;
            boostPackagesPromo.PromoCode = boostPackagesPromo.PromoCode;
            boostPackagesPromo.Discount = boostPackagesPromo.Discount;
            db.SaveChanges();
            return ObjectSerilization.GetSerilizedWithoutObject("Promo code updated", true);
        }

        public object UpdateBoostStatus(bool status, long Id, long UserId)
        {
            var existingPackage = db.BoostPackages.Find(Id);
            existingPackage.IsActive = status;
            existingPackage.ModifyBy = UserId;
            existingPackage.ModifyDate = DateTime.Now;
            db.SaveChanges();
            return ObjectSerilization.GetSerilizedWithoutObject("Boost Pacakge Status Updated", false);
        }
        public object AddNewPackage(OperatorPackagesModel operatorPackage)
        {
            try
            {
                if (operatorPackage.Id > 0)
                {
                    var operatorPackageExisting = db.OperatorPackages.Find(operatorPackage.Id);
                    if (operatorPackageExisting != null)
                    {
                        operatorPackageExisting.IsActive = operatorPackage.IsActive;
                        if (operatorPackage.Discount > 0)
                        {
                            operatorPackageExisting.Is_Discount = true;
                            operatorPackageExisting.Discount = operatorPackage.Discount;
                        }
                        else
                        {
                            operatorPackageExisting.Is_Discount = false;
                            operatorPackageExisting.Discount = operatorPackage.Discount;
                        }
                        operatorPackageExisting.LimitaionTrips = operatorPackage.LimitaionTrips;
                        operatorPackageExisting.Name = operatorPackage.Name;
                        operatorPackageExisting.Price = operatorPackage.Price;
                        operatorPackageExisting.Details = operatorPackage.Details;
                        operatorPackageExisting.ModifyDate = DateTime.Now;
                        operatorPackageExisting.ModifyById = operatorPackage.ModifyById;
                        db.SaveChanges();
                        return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Package upated", true);
                    }
                }
                else
                {
                    OperatorPackage package = new OperatorPackage();
                    package.IsActive = true;
                    package.LimitaionTrips = operatorPackage.LimitaionTrips;
                    package.ModifyById = operatorPackage.ModifyById;
                    package.ModifyDate = DateTime.Now;
                    package.Name = operatorPackage.Name;
                    package.CreatedDate = DateTime.Now;
                    package.Price = operatorPackage.Price;
                    if (operatorPackage.Discount > 0)
                    {
                        package.Is_Discount = true;
                        package.Discount = operatorPackage.Discount;
                    }
                    else
                    {
                        package.Is_Discount = false;
                        package.Discount = operatorPackage.Discount;
                    }
                    db.OperatorPackages.Add(package);
                    db.SaveChanges();
                    return ObjectSerilization.GetSerilizedWithoutObject("New Packages created", true);
                }
                return ObjectSerilization.GetSerilizedWithoutObject("Some thing went wrong", false);

            }
            catch (Exception ex)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false);
            }
        }

        public object DeleteOperatorPackage(long Id, long UserId)
        {
            var tripOperator = db.OperatorPackages.Find(Id);
            if (tripOperator!=null)
            {
                tripOperator.Is_Delete = true;
                tripOperator.ModifyById = UserId;
                db.SaveChanges();
                return ObjectSerilization.GetSerilizedWithoutObject("Package Deleted", true);
            }
            return ObjectSerilization.GetSerilizedWithoutObject("No Package Deleted", false);
        }


        #endregion

    }
}