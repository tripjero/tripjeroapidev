//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Buisness.Core.DbContext
{
    using System;
    
    public partial class GetBoostPromo_Result
    {
        public long BoostPcakgeId { get; set; }
        public Nullable<long> PromoId { get; set; }
        public int NoTrips { get; set; }
        public double Price { get; set; }
        public string Title { get; set; }
        public int Discount { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
    }
}
