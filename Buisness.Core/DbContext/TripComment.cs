//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Buisness.Core.DbContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class TripComment
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TripComment()
        {
            this.TripCommentsReplies = new HashSet<TripCommentsReply>();
        }
    
        public long Id { get; set; }
        public long TId { get; set; }
        public long UId { get; set; }
        public string Comments { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifyDate { get; set; }
        public Nullable<bool> IsAdminReviwed { get; set; }
        public string ReviewComments { get; set; }
        public Nullable<bool> IsSpam { get; set; }
    
        public virtual Trip Trip { get; set; }
        public virtual User User { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TripCommentsReply> TripCommentsReplies { get; set; }
    }
}
