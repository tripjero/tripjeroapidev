﻿using System.Data.Entity;

namespace DataEntites.GeneraicRepositroy
{
    public class DbContextSP : DbContext
    {
        public DbContextSP() : base("TripJeroEntities")
        {
            Database.SetInitializer<DbContextSP>(null);
        }

        public DbContextSP(string connectionString) : base(connectionString)
        {
            Database.SetInitializer<DbContextSP>(null);
        }
    }
}