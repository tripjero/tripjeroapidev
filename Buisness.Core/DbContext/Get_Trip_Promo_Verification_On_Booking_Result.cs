//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Buisness.Core.DbContext
{
    using System;
    
    public partial class Get_Trip_Promo_Verification_On_Booking_Result
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public long TripId { get; set; }
        public string Promo_Key { get; set; }
        public string PromoCode { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
        public double DiscountPercentage { get; set; }
    }
}
