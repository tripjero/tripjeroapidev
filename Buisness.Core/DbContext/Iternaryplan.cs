//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Buisness.Core.DbContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class Iternaryplan
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Iternaryplan()
        {
            this.DayPlans = new HashSet<DayPlan>();
        }
    
        public long Id { get; set; }
        public string DyaNumber { get; set; }
        public long TripId { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DayPlan> DayPlans { get; set; }
        public virtual Trip Trip { get; set; }
    }
}
