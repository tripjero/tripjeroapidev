//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Buisness.Core.DbContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class ExternalLogin
    {
        public long Id { get; set; }
        public string Provider { get; set; }
        public string ProviderId { get; set; }
        public string Token { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifyDate { get; set; }
        public long UId { get; set; }
    }
}
