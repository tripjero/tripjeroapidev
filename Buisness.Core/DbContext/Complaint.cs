//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Buisness.Core.DbContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class Complaint
    {
        public long Id { get; set; }
        public string Subject { get; set; }
        public string ComplaintDetails { get; set; }
        public bool IsActive { get; set; }
        public bool IsResolved { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public long UId { get; set; }
        public Nullable<long> ModifyById { get; set; }
        public System.DateTime ModifyDate { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<bool> IsVisited { get; set; }
        public Nullable<long> ResolvedBy { get; set; }
    
        public virtual User User { get; set; }
    }
}
