﻿using Buisness.Core.DbContext;
using DataEntites.GeneraicRepositroy;
using DataEntites.Model.Promo;
using DataEntites.Model.Trip;
using DataEntites.Model.TripBooking;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using TripJero.Notification.Services;
using Utilties.Genral;

namespace Buisness.Core.TripBooking
{
    public class TripBookingService : ITripBooking
    {
        private TripJeroEntities db = new TripJeroEntities();

        public object AddComplaint(Complaint complaint)
        {
            try
            {
                if (complaint.Subject == null || string.IsNullOrEmpty(complaint.Subject.Trim()))
                {
                    return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Complaint Subject is required", false);
                }
                if (complaint.ComplaintDetails == null || string.IsNullOrEmpty(complaint.ComplaintDetails.Trim()))
                {
                    return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Complaint Details is required", false);
                }
                complaint.IsActive = true;
                complaint.IsResolved = false;
                complaint.ModifyDate = DateTime.Now;
                complaint.ModifyById = complaint.UId;
                complaint.IsDeleted = false;
                complaint.CreatedDate = DateTime.Now;
                db.Complaints.Add(complaint);
                db.SaveChanges();
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Complaint Added", true);
            }
            catch (Exception ex)
            {
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false);
            }
        }

        public object BookTrip(DataEntites.Model.TripBooking.TripBooking tripBooking, string usersToken)
        {
            try
            {
                DbContext.TripBooking booking = new DbContext.TripBooking();

                if (db.TripBookings.FirstOrDefault(x => x.TId == tripBooking.TId && x.UId == tripBooking.UId && x.IsCanclled != true) != null)
                {
                    return ObjectSerilization.GetSerilizedWithoutObject("You have already Booked this Trip! To Rebook please cancel your booking of Trip from Booking Page and re-book this trip again", false);
                }
                if (db.Trips.Find(tripBooking.TId).NO_MORE_BOOKING == true)
                {
                    return ObjectSerilization.GetSerilizedWithoutObject("This tirp no more avaible for booking , Please select other one", false);
                }

                if (db.Trips.Find(tripBooking.TId).StartDateTime.Date <= DateTime.Now.Date)
                {
                    return ObjectSerilization.GetSerilizedWithoutObject("This tirp cannot accept more booking ", false);
                }
                var tripDetails = db.Trips.Find(tripBooking.TId);
                if (tripDetails==null)
                {
                    return ObjectSerilization.GetSerilizedWithoutObject("Trip not found", false);
                }
               
                var totalPrice = (tripBooking.NoPersons * tripDetails.Amount);

                if (tripDetails.DiscountPercentage != null)
                {
                    if (tripDetails.DiscountPercentage > 0)
                    {
                        var discPer = tripDetails.DiscountPercentage;
                        var tempamount = totalPrice - (double)(totalPrice * (discPer) / 100.0);
                        totalPrice =  tempamount;

                    }

                }


                if (tripBooking.Is_Promo)
                {
                    var pKey = new SqlParameter("PromoCode", SqlDbType.VarChar) { Value = tripBooking.PromoCode };
                    var tripId = new SqlParameter("TripId", SqlDbType.VarChar) { Value = tripBooking.TId };
                    var prmoList = SpRepository<PromoModel>.GetSingleObjectWithStoreProcedure(@"exec Get_Trip_Promo_Verification_On_Booking @PromoCode,@TripId", pKey,tripId);
                    if (prmoList == null)
                    {
                        return ObjectSerilization.GetSerilizedWithoutObject("Promo code no valid please try an other", false);
                    }
                    booking.Is_PromoApplyed = true;

                    var discPer = prmoList.DiscountPercentage;
                    var tempamount =  (double)((tripBooking.NoPersons * tripDetails.Amount) * (discPer) / 100.0);
                    totalPrice = totalPrice - tempamount;

                    //totalPrice = Utilties.Genral.DiscountCalculator.Discount(Convert.ToInt32(prmoList.DiscountPercentage), Convert.ToDouble((tripBooking.NoPersons * tripDetails.Amount)));
                }
                booking.TotalPrice = totalPrice;
                booking.UId = tripBooking.UId;
                booking.NoPersons = tripBooking.NoPersons;
                booking.TId = tripBooking.TId;
                booking.ModifyDate = DateTime.Now;
                booking.CreatedDate = DateTime.Now;
                booking.ModifyById = tripBooking.ModifyById;
                booking.IsActive = true;
                booking.IsDeleted = false;
                booking.PaymentStatus = Constant.PaymentStatus.UnPaid.ToString();
                booking.Name = tripBooking.Name;
                booking.PhoneNumber = tripBooking.PhoneNumber;
                booking.Email = tripBooking.Email;
                booking.EmergencyContact = tripBooking.EmergencyContact;
                booking.IsBookForMySelf = tripBooking.IsBookForMySelf;
                booking.Childrens = tripBooking.Childrens;


                db.TripBookings.Add(booking);
                db.SaveChanges();
                //if (!string.IsNullOrEmpty(usersToken))
                //{
                    var trip = db.Trips.Find(booking.TId);
                    
                    var user = db.Users.Find(booking.UId);
                    var notificationTrayList = db.NotificationTrayTbls.FirstOrDefault(x => x.Title == "BookTripNotification");
                    //if (notificationTrayList != null)
                    //{
                        if (trip.User.FirebaseToken != null)
                        {
                            NotificationSender.SendNotification(trip.User.FirebaseToken, "You have new Booking of Trip " + trip.Title, "Trip Booking");
                        }
                      
                
                var t = Task.Run(() => NotificationLog(trip.User.Id, "You have new Booking of Trip " + trip.Title, "Trip Booking"));
                        t.Wait();
                        if (user.Email != null)
                        {
                            string body = SendBookedEmailHtml(user.FullName, trip.User.FullName, trip.Title, booking.Id.ToString(), "Conformed", trip.StartDateTime.ToLongDateString(), trip.Duration + " days", DateTime.Now.ToLongDateString());
                            EmailSender.SendEmail(user.Email, "Trip Booking Conformation", body);
                        }
                        if (trip.User.Email != null)
                        {
                            var paymentSTatus = Constant.PaymentStatus.Paid.ToString();
                            if (booking.IsActive)
                            {
                                paymentSTatus = Constant.PaymentStatus.Paid.ToString();
                            }
                            else
                            {
                                paymentSTatus = Constant.PaymentStatus.UnPaid.ToString();
                            }
                            string EmailString = Utilties.Genral.EmailTemplates.BookingEmailToOpt(trip.User.FullName, trip.Title, booking.Id.ToString(),
                                user.FullName, user.PhoneNumber, trip.StartDateTime.ToShortDateString(), trip.EndDateTime.ToShortDateString(), trip.Amount.ToString(), trip.DiscountPercentage.ToString(),
                                booking.TotalPrice.ToString(), booking.NoPersons.ToString(), booking.Is_PromoApplyed.ToString(), booking.CreatedDate.ToShortDateString(), paymentSTatus);
                            EmailSender.SendEmail(trip.User.Email, "New Trip Booking", EmailString);
                        }
                    //}
                //}
                var bookingRefreance = new { BookingId = booking.Id };
                return ObjectSerilization.GetSerilizedObject("Thank you for booking. Your trip has been booked.", true, bookingRefreance);
            }
            catch (Exception ex)
            {
                return ObjectSerilization.GetSerilizedWithoutObject(ex.Message, false);
            }
        }

        public object GetScheduledTripsByUserId(long Id, int pageNo, int pageSize)
        {
            var emptyList = new List<TravBookings>();
            var _id = new SqlParameter("TrevlerId", SqlDbType.BigInt) { Value = Id };
            var _pageNo = new SqlParameter("PageNumber", SqlDbType.Int) { Value = pageNo };
            var _pageSize = new SqlParameter("RowOfPage", SqlDbType.Int) { Value = pageSize };

            var bookedTrip = SpRepository<TravBookings>.GetListWithStoreProcedure(@"exec Get_Travlers_Booked_Trip @TrevlerId,@PageNumber,@RowOfPage", _id,_pageNo,_pageSize);

        
            if (bookedTrip.Count > 0)
            {
                return ObjectSerilization.GetSerilizedObject("Booked Trip Found", true, bookedTrip,
                    bookedTrip[0].PageSize, bookedTrip[0].PageNo, bookedTrip[0].TotalPage);
            }
            else
            {

                return ObjectSerilization.GetSerilizedObject("No Trip Found", true, emptyList);

            }


 
        }

        public object UpdateTrip(DataEntites.Model.TripBooking.TripBooking tripBooking)
        {
            throw new NotImplementedException();
        }

        public string SendBookedEmailHtml(string travllerName, string operatorName, string tripTitle, string bookingId, string status, string startDate, string durations, string bookedDate)
        {
            string emailHTML = "<h4> Dear " + travllerName + "  ,</h4>" +
                                "<p> Thank you for booking with TripJero and your trip has been confirmed by (" + operatorName + ").</p><p>Change or Cancel the booking subjected to Tour Operator policy please contact Tour Operator for details. </p>" +
                                "<p>As per <span style='color:blue'>Trip</span><span style='color:orange'>Jero</span> policy you are obliged to inform or notify Tour Operator in any kind of changes to Booked Trip</p>"
                                + "<div align='center'>"
                                  + "<h2>Booking Reference #</h2><h1>" + bookingId + "</h1>"
                                + "</div>"
                                + "<div><h2>Booking Details</h2><ol>"
                                    + "<li>Tour Operator:" + operatorName + "</li>"
                                    + "<li>Trip Name:" + tripTitle + "</li>"
                                    + "<li>Status: Conformed</li>"
                                    + "<li>Start Date:" + startDate + "</li>"
                                    + "<li>Durations:" + durations + "</li>"
                                    + "<li>Booked Date:" + bookedDate + "</li>"
                                  + "</ol></div><div>"
                                  + "<b>Disclaimer:</b> <span style='color:blue'>Trip</span><span style='color:orange'>Jero</span>, does not handles or responsible for financial losses of either party, TripJero is facility provider to query trips and operators.Our contact channels are open for disputes and complains."
                                  + "</div><br><br>"
                                + @"<div align='center'>
                                © <span style='color:blue'>Trip</span><span style='color:orange'>Jero</span>, 2020. All rights reserved.
                                TripJero.com | Islamabad, Pakistan
                                </div>";
            return emailHTML;
        }

        public object CanclledTrip(long Id, long BId)
        {
            if (Id > 0 && BId > 0)
            {
                var _booking = db.TripBookings.FirstOrDefault(x => x.Id == BId && x.UId == Id);
                if (_booking != null)
                {
                    _booking.IsCanclled = true;
                    _booking.ModifyById = Id;
                    _booking.ModifyDate = DateTime.Now;
                    db.SaveChanges();
                    return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Trip has been canclled", true);
                }
                return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Current Booking is not available", false);
            }
            return Utilties.Genral.ObjectSerilization.GetSerilizedWithoutObject("Id requried", false);
        }
        public async Task NotificationLog(long Uid, string Title, string Body)
        {
            NotificationHistory notificationHistory = new NotificationHistory();
            notificationHistory.UId = Uid;
            notificationHistory.Title = Title;
            notificationHistory.Body = Body;
            notificationHistory.CreatedDate = DateTime.Now;
            db.NotificationHistories.Add(notificationHistory);
            await db.SaveChangesAsync();
        }

        public object GetOperatorScheduleBookingTrips(long Id,int pageNo,int pageSize)
        {
            var emptyList = new List<Get_Travlers_Booked_Trip_Result>();

            var _id = new SqlParameter("UserId", SqlDbType.Int) { Value = Id };
            var _pageNo = new SqlParameter("PageNumber", SqlDbType.Int) { Value = pageNo };
            var _pageSize = new SqlParameter("RowOfPage", SqlDbType.Int) { Value = pageSize };
            var tripList = SpRepository<TripBasicInfo>.GetListWithStoreProcedure(@"exec Get_Trip_By_User_Id @UserId,@PageNumber,@RowOfPage", _id,_pageNo,_pageSize);
            if (tripList.Count > 0)
            {
                List<TripModel> tripModelsList = new List<TripModel>();
                foreach (var item in tripList)
                {
                    tripModelsList.Add(getBookingOfThisTrip(item));
                }

                return ObjectSerilization.GetSerilizedObject("Trip Found", true, tripModelsList, tripList[0].PageSize,tripList[0].PageNo,tripList[0].TotalPage);
            }
            else
            {
                
                    return ObjectSerilization.GetSerilizedObject("No Trip Found", true, emptyList);
                
            }

        }

        public object GetOperatorOldBookingTrips(long Id, int pageNo, int pageSize)
        {
            var emptyList = new List<Get_Travlers_Booked_Trip_Result>();

            var _id = new SqlParameter("UserId", SqlDbType.Int) { Value = Id };
            var _pageNo = new SqlParameter("PageNumber", SqlDbType.Int) { Value = pageNo };
            var _pageSize = new SqlParameter("RowOfPage", SqlDbType.Int) { Value = pageSize };
            var tripList = SpRepository<TripBasicInfo>.GetListWithStoreProcedure(@"exec GetOperatorOldTrips @UserId,@PageNumber,@RowOfPage", _id, _pageNo, _pageSize);
            if (tripList.Count > 0)
            {
                List<TripModel> tripModelsList = new List<TripModel>();
                foreach (var item in tripList)
                {
                    tripModelsList.Add(getBookingOfThisTrip(item));
                }

                return ObjectSerilization.GetSerilizedObject("Trip Found", true, tripModelsList, tripList[0].PageSize, tripList[0].PageNo, tripList[0].TotalPage);
            }
            else
            {
                return ObjectSerilization.GetSerilizedObject("No Trip Found", true, emptyList);
            }
        }

        public object UpdateBookingStatus(OperatorBookings req)
        {
            var booking = db.TripBookings.Find(req.Id);
            if (booking != null)
            {
                booking.PaymentStatus = req.PaymentStatus;
                db.SaveChanges();
                var user = db.Users.Find(booking.UId);
                string EmailString = Utilties.Genral.EmailTemplates.PaymentStatusToTrav(user.FullName,booking.Id.ToString(),booking.PaymentStatus);
                EmailSender.SendEmail(user.Email, "Trip Booking Payment Confirmation.", EmailString);
                return ObjectSerilization.GetSerilizedWithoutObject("Payment status updated sucessfully", true);

            }
            else
            {
                return ObjectSerilization.GetSerilizedWithoutObject("Order not found", false);
            }
        }




        private TripModel getBookingOfThisTrip(TripBasicInfo tripBasicInfo)
        {
            TripModel tripModel = new TripModel();
            if (tripBasicInfo != null)
            {
                tripModel.TripBasicInfo = tripBasicInfo;
                var OperatorId = new SqlParameter("OperatorId", SqlDbType.BigInt) { Value = tripBasicInfo.UId };
                var OperatorIdReviews = new SqlParameter("OperatorId", SqlDbType.BigInt) { Value = tripBasicInfo.UId };
                var tripId = new SqlParameter("TripId", SqlDbType.BigInt) { Value = tripBasicInfo.Id };
       
                #region trip Price
                //var tripId = new SqlParameter("TripId", SqlDbType.BigInt) { Value = tripBasicInfo.Id };
                //var tripPrices = SpRepository<DataEntites.Model.Trip.TripPrice.TripPrice>.GetSingleObjectWithStoreProcedure(@"exec Get_DiscountedTrip_Price @TripId", tripId);
                tripModel.TripStatus = tripBasicInfo.TripStatus;
                tripModel.IsBoost = tripBasicInfo.IsBoost;

                if (tripBasicInfo.DiscountPercentage != null && tripBasicInfo.DiscountPercentage > 0)
                {
                    tripModel.tripPrices.ActualAmount = tripBasicInfo.Amount;
                    tripModel.tripPrices.CurrentAmount = (double)(tripBasicInfo.Amount * (100.0 - tripBasicInfo.DiscountPercentage) / 100.0);
                    tripModel.tripPrices.DiscountPercentage = (double)tripBasicInfo.DiscountPercentage;
                    tripModel.tripPrices.HavePromo = tripBasicInfo.Is_Promo;
                    tripModel.tripPrices.PromoCode = tripBasicInfo.PromoCode;
                    tripModel.tripPrices.trip_id = (long)tripBasicInfo.Id;
                    tripModel.tripPrices.user_id = tripBasicInfo.UId;
                    tripModel.tripPrices.PromoCode = tripBasicInfo.PromoCode;
                }
                else
                {
                    tripModel.tripPrices.ActualAmount = tripBasicInfo.Amount;
                    tripModel.tripPrices.CurrentAmount = (double)tripBasicInfo.Amount;
                    tripModel.tripPrices.DiscountPercentage = 0;
                    tripModel.tripPrices.PromoCode = tripBasicInfo.PromoCode;
                    tripModel.tripPrices.HavePromo = tripBasicInfo.Is_Promo;
                    tripModel.tripPrices.trip_id = (long)tripBasicInfo.Id;
                    tripModel.tripPrices.user_id = tripBasicInfo.UId;
                    tripModel.tripPrices.PromoCode = tripBasicInfo.PromoCode;
                }
                #endregion

            
                var TripId = new SqlParameter("TripId", SqlDbType.Int) { Value = tripBasicInfo.Id };
                var tripList = SpRepository<OperatorBookings>.GetListWithStoreProcedure(@"exec GetTripBookingsByTripID @TripId", TripId);
                tripModel.Bookings = tripList;
            }
            return tripModel;
        }

    }
}