﻿using Buisness.Core.DbContext;
using DataEntites.Model.TripBooking;

namespace Buisness.Core.TripBooking
{
    public interface ITripBooking
    {
        object BookTrip(DataEntites.Model.TripBooking.TripBooking tripBooking, string usersToken);

        object UpdateTrip(DataEntites.Model.TripBooking.TripBooking tripBooking);

        object GetScheduledTripsByUserId(long Id,int pageNo,int pageSize);

        object AddComplaint(Complaint complaint);

        object CanclledTrip(long Id, long BId);

        object GetOperatorScheduleBookingTrips(long Id,int pageNo,int pageSize);

        object GetOperatorOldBookingTrips(long Id, int pageNo, int pageSize);

        object UpdateBookingStatus(OperatorBookings req);
    }
}