﻿using TripJero.Notification.FirbeaseNotification;

namespace TripJero.Notification.Services
{
    public class EmailSender
    {
        public static void SendEmail(string senderEmail, string subj, string body)
        {
            EmailService.AsynEmailSender sendEmail = new EmailService.AsynEmailSender(EmailService.SendEmail);
            sendEmail.BeginInvoke(senderEmail, subj, body, null, null);
        }
    }
}