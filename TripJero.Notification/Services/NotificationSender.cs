﻿using TripJero.Notification.FirbeaseNotification;

namespace TripJero.Notification.Services
{
    public class NotificationSender
    {
        public static void SendNotification(string token, string message, string title)
        {
            NotificationService.AsyncSendNotification sendNotification = new NotificationService.AsyncSendNotification(NotificationService.sendNotificaton);
            sendNotification.BeginInvoke(token, message, title, null, null);
        }
    }
}