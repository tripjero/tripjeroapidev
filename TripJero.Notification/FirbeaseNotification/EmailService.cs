
using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Text;
using System;
using System.Configuration;

namespace TripJero.Notification.FirbeaseNotification
{
    public class EmailService
    {
        public delegate void AsynEmailSender(string senderEmail, string subj, string body);

        public static void SendEmail(string senderEmail, string subj, string body)
        {
            string starttime = DateTime.Now.ToLongTimeString();
            var bodyHTML = "";
            bodyHTML += "<body>";
            bodyHTML += body;
            bodyHTML += "</body>";
            System.Net.Mail.SmtpClient smp = new System.Net.Mail.SmtpClient();
            //MailMessage msg = new MailMessage("noreply@tripjero.com", senderEmail);
            //if (CC != null)
            //{
            //    foreach (var item in CC)
            //    {
            //        msg.CC.Add(item);
            //    }
            //}

            //if (BCC != null)
            //{
            //    foreach (var item in BCC)
            //    {
            //        msg.Bcc.Add(item);
            //    }
            //}


            //msg.Subject = subj;
            //msg.BodyEncoding = System.Text.Encoding.UTF8;
            //msg.Body = bodyHTML;
            //msg.IsBodyHtml = true;
            //client.EnableSsl = true;
            //msg.Priority = MailPriority.Normal;
            //client.Send(msg);
            //string Endtime = DateTime.Now.ToLongTimeString();
            ////Logtimer(starttime, Endtime, "Email");
            //return true;
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("Trip Jero Info", ConfigurationManager.AppSettings.Get("SMTPUserName").ToString()));
            message.To.Add(new MailboxAddress("", senderEmail));
            message.Subject = subj;

            message.Body = new TextPart(TextFormat.Html)
            {
                Text = bodyHTML
            };

            using (var client = new SmtpClient())
            {
                try
                {
                    client.Connect(ConfigurationManager.AppSettings.Get("SMTPHost").ToString(),
                                  int.Parse(ConfigurationManager.AppSettings.Get("SMTPPort")), true);
                    client.Authenticate(ConfigurationManager.AppSettings.Get("SMTPUserName").ToString(),
                                        ConfigurationManager.AppSettings.Get("SMTPPassword").ToString());
                    client.Send(message);
                    client.Disconnect(true);
                }
                catch (Exception ex)
                {

                    client.Disconnect(true);
                }

            }


        }

    }
}